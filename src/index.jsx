import React from 'react';
import { render } from 'react-dom';
import { SWRConfig } from 'swr';
import { dataFetcher } from 'Api';
import { App } from './app';

render((
  <SWRConfig
    value={{
      fetcher: dataFetcher,
      revalidateOnFocus: false,
      revalidateOnMount: false,
      // shouldRetryOnError: false,
    }}
  >
    <App />
  </SWRConfig>
), document.getElementById('root'));
