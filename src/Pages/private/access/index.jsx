import React, { useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import pt from 'prop-types';
import defaultDpImage from 'Assets/person.svg';
import defaultDpImage_Female from 'Assets/female@2x.png';
import defaultDpImage_Male from 'Assets/male@2x.png';
import newRequestImage from 'Assets/plus@2x.png';
import { Page } from 'Components';
import { AppContext } from 'Context';
import { Pathname } from 'Routes';
import styles from './index.module.css';

const ProfileLink = ({ className, dpURL, gender, isAccessRequestButton, link, name }) => (
  <Link className={cn({ [styles.profileLink]: true, [className]: className })} to={link}>
    <div className={styles.profileImage}>
      {isAccessRequestButton
        ? <img className={styles.dp} src={newRequestImage} alt="Request Access" />
        : (
          <img
            className={styles.dp}
            src={
              dpURL || (gender === 'female'
                ? defaultDpImage_Female
                : gender === 'male'
                  ? defaultDpImage_Male
                  : defaultDpImage
              )
            }
            alt={name}
          />
        )}
    </div>
    <div className={styles.profileName}>{name}</div>
  </Link>
);

ProfileLink.defaultProps = {
  className: undefined,
  dpURL: undefined,
  gender: undefined,
  isAccessRequestButton: false,
  name: undefined
};

ProfileLink.propTypes = {
  className: pt.string,
  dpURL: pt.string,
  gender: pt.string,
  isAccessRequestButton: pt.bool,
  link: pt.string.isRequired,
  name: pt.string
};

export const Access = () => {
  const { userConnections, getUserConnections } = useContext(AppContext);

  useEffect(() => {
    const userConnectionFetch = setInterval(() => {
      getUserConnections();
    }, 30000);

    return () => {
      clearInterval(userConnectionFetch);
    };
  }, []);

  return (
    <Page
      className={styles.page}
      containerClassName={styles.pageContainer}
      contentClassName={styles.pageContent}
      noBottomBar
      noDefaultBg
      noFixedSidebar
      transparentCompactHeader
    >
      <div className={styles.access}>
        <div className={styles.message}>
          <>YOU CAN</>
          <span className={styles.messageSpace}>&nbsp;</span>
          <br className={styles.messageBreak} />
          <>ACCESS</>
        </div>

        <div
          className={cn({
            [styles.profileLinksContainer]: true,
            [styles[`has${((userConnections?.length + 1) >= 4 ? 4 : (userConnections?.length + 1))}Item`]]: true,
          })}
        >
          {userConnections?.map(({ email, firstname, id }, idx) => (
            <ProfileLink key={idx} link={Pathname.getFriendProfilePath(id)} name={firstname || email} />
          ))}
          <ProfileLink
            className={styles.requestProfileAccess}
            gender="male"
            isAccessRequestButton
            link={Pathname.requestAccess}
            name="Request New Access"
          />
        </div>

        <Link to={Pathname.getProfilePath()} className={styles.myContentButton}>View My Content</Link>
      </div>
    </Page>
  );
};
