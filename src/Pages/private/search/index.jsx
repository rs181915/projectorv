import React, { useContext, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { CircularProgress as MuiCircularProgress, withStyles } from '@material-ui/core';
import { dataFetcher, endpoints } from 'Api';
import { ProfilePage } from 'Components';
import { AppContext } from 'Context';
import { Pathname } from 'Routes';
import styles from './index.module.css';

const CircularProgress = withStyles({
  circle: {
    color: '#fff !important',
  },
})(MuiCircularProgress);

const ImageGroupTitle = ({ title }) => (<h3 className={styles.imageGroupTitle}>{title}</h3>);

const ImageGroup = ({ searchTerm, videos, isGrid }) => (
  <div className={styles.imageGroup}>
    {searchTerm && <ImageGroupTitle title={`Search results for "${searchTerm}"`} />}
    <div className={styles.imageGroupImagesWrapper}>
      <div className={cn({ [styles.imageGroupImagesContainer]: true, [styles.isGrid]: isGrid })}>
        <div className={styles.imageGroupImagesContent}>
          {videos?.map(({ id, thumbnails, title: videoTitle }, idx) => (
            <Link className={styles.imageGroupImageLink} key={idx} to={Pathname.getVideoPath(id)}>
              <img src={thumbnails} className={styles.imageGroupImage} />
              <div>
                <h4>{videoTitle}</h4>
              </div>
            </Link>
          ))}
          <div className={cn({ [styles.imageGroupImageLink]: true, [styles.dummy]: true })} />
        </div>
      </div>
    </div>
  </div>
);

export const SearchPage = () => {
  const { setAppSnackbar, userDetails } = useContext(AppContext);
  const { profileID } = useParams();

  const [profileName, setProfileName] = useState(undefined);
  const [searchTerm, setSearchTerm] = useState('');
  const [loading, setLoading] = useState(true);
  const [videos, setVideos] = useState([]);
  const [searchResults, setSearchResults] = useState([]);

  useEffect(() => {
    if (userDetails?.firstname) {
      setProfileName(userDetails?.firstname?.substring(0, 10));
    } else if (userDetails?.email) {
      setProfileName(userDetails?.email?.split('@')[0]?.substring(0, 10));
    }
  }, [userDetails]);

  useEffect(() => {
    if (searchTerm) {
      const results = [];

      videos.forEach((video) => {
        if (video?.title?.includes(searchTerm)) {
          results.push(video);
        }
      });

      setSearchResults(results);
    } else {
      setSearchResults(videos);
    }
  }, [searchTerm, videos]);

  const { mutate: getMyVideos } = useSWR([endpoints.getMyVideosList, profileID], {
    fetcher: (url) => dataFetcher(url),
    onSuccess: ({ success, data }) => {
      if (success) {
        setVideos(data);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }

      setLoading(false);
    },
    onError: () => {
      setLoading(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  useEffect(() => { getMyVideos(); }, []);

  return (
    <ProfilePage
      containerClassName={styles.pageContainer}
      mainClassName={cn({ [styles.mainContent]: true, [styles.loading]: loading })}
      userName={profileName}
      userProfileLink={Pathname.getFriendProfilePath(profileID)}
      noHeader
      noDefaultBg
      isProfileHeader
    >
      {loading
        ? <CircularProgress />
        : (
          <>
            <input
              className={styles.searchInput}
              type="search"
              placeholder="Search"
              onChange={(e) => { setSearchTerm(e.target.value); }}
            />
            <br />
            <br />
            {loading
              ? (
                <div className={styles.categoriesLoader}>
                  <CircularProgress />
                </div>
              )
              : (
                <ImageGroup
                  isGrid
                  videos={searchResults}
                  searchTerm={searchTerm}
                />
              )}
          </>
        )}
    </ProfilePage>
  );
};
