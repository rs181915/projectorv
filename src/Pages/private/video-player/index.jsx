/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/no-find-dom-node */
import React, { useContext, useEffect, useRef, useState } from 'react';
import { findDOMNode } from 'react-dom';
import { useNavigate, useParams } from 'react-router-dom';
import useSWR from 'swr';
import { CircularProgress as MuiCircularProgress, withStyles } from '@material-ui/core';
import screenfull from 'screenfull';
import { dataFetcher, endpoints } from 'Api';
import { AppContext } from 'Context';
import PlayIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import CloseIcon from '@material-ui/icons/Cancel';
import MuteIcon from '@material-ui/icons/VolumeOff';
import UnMuteIcon from '@material-ui/icons/VolumeUp';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import skipBehindIcon from 'Assets/skip-behind-15-seconds.png';
import skipAheadIcon from 'Assets/skip-ahead-15-seconds.png';
import ReactPlayer from 'react-player';
import styles from './index.module.css';
import 'video-react/dist/video-react.css';

const CircularProgress = withStyles({
  circle: {
    color: '#fff !important'
  }
})(MuiCircularProgress);

export const VideoPlayer = () => {
  const { setAppSnackbar } = useContext(AppContext);
  const isFistRender = useRef(true);
  const navigate = useNavigate();
  const { videoID } = useParams();
  const videoRef = useRef(null);

  const [videoDetails, setVideoDetails] = useState({});
  const [loading, setLoading] = useState(true);

  const [isPlaying, setPlaying] = useState(false);
  const [isPIPOn, setPIP] = useState(false);
  const [isMuted, setMute] = useState(false);
  const [lastRecordedPlaytime, setLastRecordedPlaytime] = useState(null);
  const [controllerIsVisible, setControllerVisibility] = useState(true);
  const [isFullscreen, setFullscreen] = useState(false);

  const { mutate: getVideoDetails } = useSWR([endpoints.getVideosDetail, videoID], {
    fetcher: (url, id) => dataFetcher(url, { id }),
    onSuccess: ({ success, data }) => {
      if (success) {
        setVideoDetails(data);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }

      setLoading(false);
    },
    onError: () => {
      setLoading(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { mutate: recordPlayTime } = useSWR([endpoints.addToPausedList, videoID, lastRecordedPlaytime], {
    fetcher: (url, id, lrpt) => dataFetcher(url, {
      video_id: id,
      action: 'paused',
      paused_at: lrpt,
    }),
    onSuccess: () => { },
    onError: () => { },
  });

  const { mutate: recordVideoEnd } = useSWR([endpoints.addToPausedList, videoID], {
    fetcher: (url, id) => dataFetcher(url, {
      video_id: id,
      action: 'finished',
    }),
    onSuccess: () => { },
    onError: () => { },
  });

  const { mutate: recordVideoStart } = useSWR([endpoints.addToPausedList, videoID, 'start'], {
    fetcher: (url, id) => dataFetcher(url, {
      video_id: id,
      action: 'started',
    }),
    onSuccess: () => { },
    onError: () => { },
  });

  const skipBehind = () => {
    const seekTime = videoRef.current.getCurrentTime() - 15;

    videoRef.current.seekTo(seekTime <= 0 ? 0 : seekTime);
  };

  const skipAhead = () => {
    const seekTime = videoRef.current.getCurrentTime() + 15;

    videoRef.current.seekTo((seekTime > videoRef.current.getDuration())
      ? (videoRef.current.getDuration() - 1)
      : seekTime);
  };

  let controllerFadeTimeout = setTimeout(() => {
    if (isPlaying) {
      setControllerVisibility(false);
    } else {
      setControllerVisibility(true);
    }
  }, 6000);

  const showControls = () => {
    clearTimeout(controllerFadeTimeout);
    setControllerVisibility(true);
    controllerFadeTimeout = null;
    controllerFadeTimeout = setTimeout(() => {
      if (isPlaying) {
        setControllerVisibility(false);
      } else {
        setControllerVisibility(true);
      }
    }, 6000);
  };

  useEffect(() => {
    setInterval(() => {
      if (isPlaying) {
        if (isFistRender.current) {
          isFistRender.current = false;
          recordVideoStart();
        } else {
          setLastRecordedPlaytime(videoRef.current.getCurrentTime());
          recordPlayTime();
        }
      }
    }, 60000);
  }, []);

  useEffect(() => {
    if (isPlaying) {
      clearTimeout(controllerFadeTimeout);
      controllerFadeTimeout = null;
      controllerFadeTimeout = setInterval(() => {
        if (isPlaying) {
          setControllerVisibility(false);
        } else {
          setControllerVisibility(true);
        }
      }, 6000);
    } else {
      setControllerVisibility(true);
    }
  }, [isPlaying]);

  useEffect(() => {
    getVideoDetails();
  }, []);

  return (
    <div className={styles.playerContainer}>
      {loading
        ? <CircularProgress />
        : (
          <>
            <ReactPlayer
              className={styles.player}
              controls={false}
              height="100vh"
              muted={isMuted}
              // onBuffer={() => console.log('onBuffer')}
              onDisablePIP={() => { setPIP(false); }}
              // onDuration={this.handleDuration}
              onEnablePIP={() => { setPIP(true); }}
              onEnded={() => {
                setLastRecordedPlaytime(videoRef.current.getCurrentTime());
                setPlaying(false);
                recordVideoEnd();
              }}
              // onError={(e) => console.log('onError', e)}
              onPause={() => {
                setLastRecordedPlaytime(videoRef.current.getCurrentTime());
                recordPlayTime();
              }}
              // onPlay={this.handlePlay}
              // onProgress={this.handleProgress}
              // onReady={() => console.log('onReady')}
              // onSeek={(e) => console.log('onSeek', e)}
              onStart={() => {
                setPlaying(true);
                recordVideoStart();
              }}
              pip={isPIPOn}
              playing={isPlaying}
              ref={videoRef}
              url={videoDetails?.video_file}
              style={{
                backgroundColor: '#000000',
                left: '0',
                position: 'fixed',
                top: '0',
              }}
              width="100vw"
            />
            <div
              style={{ opacity: controllerIsVisible ? 1 : 0 }}
              className={styles.overlay}
              onClick={showControls}
              onMouseMove={showControls}
            >
              <div className={styles.header}>
                <button onClick={() => { navigate(-1); }} style={{ transform: 'scale(0.75)' }}>
                  <CloseIcon />
                </button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div className={styles.title}>{videoDetails?.title}</div>
              </div>
              <div className={styles.playPause} />
              <div className={styles.footer}>
                <button onClick={() => { setMute(!isMuted); }} style={{ marginLeft: 'auto' }}>
                  {isMuted ? <UnMuteIcon /> : <MuteIcon />}
                </button>
                <div className={styles.playbackControls}>
                  <button onClick={skipBehind}>
                    <img src={skipBehindIcon} />
                  </button>
                  <button onClick={() => { setPlaying(!isPlaying); }}>
                    {isPlaying ? <PauseIcon /> : <PlayIcon />}
                  </button>
                  <button onClick={skipAhead}>
                    <img src={skipAheadIcon} />
                  </button>
                </div>
                <button
                  onClick={() => {
                    if (!isFullscreen) {
                      screenfull.request(findDOMNode(videoRef.current.player));
                      setFullscreen(true);
                    } else {
                      screenfull.exit(findDOMNode(videoRef.current.player));
                      setFullscreen(false);
                    }
                  }}
                >
                  {isFullscreen ? <FullscreenExitIcon /> : <FullscreenIcon />}
                </button>
              </div>
            </div>
          </>
        )}
    </div>
  );
};
