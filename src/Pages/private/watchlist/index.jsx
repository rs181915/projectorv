import React, { useContext, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { CircularProgress as MuiCircularProgress, withStyles } from '@material-ui/core';
import { dataFetcher, endpoints } from 'Api';
import { ProfilePage } from 'Components';
import { AppContext } from 'Context';
import { Pathname } from 'Routes';
import styles from './index.module.css';

const CircularProgress = withStyles({
  circle: {
    color: '#fff !important',
  },
})(MuiCircularProgress);

const ImageGroupTitle = ({ title }) => (<h3 className={styles.imageGroupTitle}>{title}</h3>);

const ImageGroup = ({ title, videos, isGrid }) => (
  <div className={styles.imageGroup}>
    <ImageGroupTitle title={title} />
    <div className={styles.imageGroupImagesWrapper}>
      <div className={cn({ [styles.imageGroupImagesContainer]: true, [styles.isGrid]: isGrid })}>
        <div className={styles.imageGroupImagesContent}>
          {videos?.map(({ id, thumbnails, title: videoTitle }, idx) => (
            <Link className={styles.imageGroupImageLink} key={idx} to={Pathname.getVideoPath(id)}>
              <img src={thumbnails} className={styles.imageGroupImage} />
              <div>
                <h4>{videoTitle}</h4>
              </div>
            </Link>
          ))}
          <div className={cn({ [styles.imageGroupImageLink]: true, [styles.dummy]: true })} />
        </div>
      </div>
    </div>
  </div>
);

export const Watchlist = () => {
  const { setAppSnackbar, userDetails } = useContext(AppContext);
  const { profileID } = useParams();

  const [profileName, setProfileName] = useState(undefined);
  const [loading, setLoading] = useState(true);
  const [videosGroup, setVideosGroup] = useState([]);

  useEffect(() => {
    if (userDetails?.firstname) {
      setProfileName(userDetails?.firstname?.substring(0, 10));
    } else if (userDetails?.email) {
      setProfileName(userDetails?.email?.split('@')[0]?.substring(0, 10));
    }
  }, [userDetails]);

  const { mutate: getMyVideos } = useSWR([endpoints.getMyWatchList, profileID], {
    fetcher: (url) => dataFetcher(url),
    onSuccess: ({ success, videos }) => {
      if (success) {
        setVideosGroup([{ title: ' ', id: '-', videos }]);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }

      setLoading(false);
    },
    onError: () => {
      setLoading(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  useEffect(() => { getMyVideos(); }, []);

  return (
    <ProfilePage
      containerClassName={styles.pageContainer}
      mainClassName={cn({ [styles.mainContent]: true, [styles.loading]: loading })}
      userName={profileName}
      userProfileLink={Pathname.getFriendProfilePath(profileID)}
      noHeader
      noDefaultBg
      isProfileHeader
    >
      {loading
        ? <CircularProgress />
        : (
          <>
            <div className={styles.heading}>
              <div className={styles.headingBGFade} />
              <div className={styles.headingText}>
                <>Your Watchlist</>
                {(videosGroup?.length === 0) && ' is \'empty\''}
              </div>
            </div>

            <br />
            <br />

            {loading
              ? (
                <div className={styles.categoriesLoader}>
                  <CircularProgress />
                </div>
              )
              : (!!videosGroup?.length && (
                videosGroup?.map((videoGroup) => (
                  !!videoGroup?.videos?.length && (
                    <ImageGroup
                      isGrid
                      videos={videoGroup?.videos}
                      key={videoGroup?.id}
                      title={videoGroup?.title}
                    />
                  )
                ))
              ))}
          </>
        )}
    </ProfilePage>
  );
};
