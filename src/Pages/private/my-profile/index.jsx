import React, { useContext, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { CircularProgress as MuiCircularProgress, withStyles } from '@material-ui/core';
import { dataFetcher, endpoints } from 'Api';
import { ProfilePage } from 'Components';
import { AppContext } from 'Context';
import { Pathname } from 'Routes';
import styles from './index.module.css';

const CircularProgress = withStyles({
  circle: {
    color: '#fff !important'
  }
})(MuiCircularProgress);

const ImageGroupTitle = ({ title }) => (<h3 className={styles.imageGroupTitle}>{title}</h3>);

const ImageGroup = ({ title, videos, isGrid }) => (
  <div className={styles.imageGroup}>
    <ImageGroupTitle title={title} />
    <div className={styles.imageGroupImagesWrapper}>
      <div className={cn({ [styles.imageGroupImagesContainer]: true, [styles.isGrid]: isGrid })}>
        <div className={styles.imageGroupImagesContent}>
          {videos?.map(({ id, thumbnails, title: videoTitle }, idx) => (
            <Link className={styles.imageGroupImageLink} key={idx} to={Pathname.getVideoPath(id)}>
              <img src={thumbnails} className={styles.imageGroupImage} />
              <div>
                <h4>{videoTitle}</h4>
              </div>
            </Link>
          ))}
          <div className={cn({ [styles.imageGroupImageLink]: true, [styles.dummy]: true })} />
        </div>
      </div>
    </div>
  </div>
);

export const MyProfile = () => {
  const { setAppSnackbar, userDetails } = useContext(AppContext);
  const { profileID } = useParams();

  const [selectedCategoryID, setSelectedCategoryID] = useState(undefined);
  const [loadingCategories, setLoadingCategories] = useState(false);
  const [profileName, setProfileName] = useState(undefined);
  const [loading, setLoading] = useState(true);
  const [videosGroup, setVideosGroup] = useState([]);
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);

  useEffect(() => {
    if (userDetails?.firstname) {
      setProfileName(userDetails?.firstname?.substring(0, 10));
    } else if (userDetails?.email) {
      setProfileName(userDetails?.email?.split('@')[0]?.substring(0, 10));
    }
  }, [userDetails]);

  const { mutate: getMyVideos } = useSWR([endpoints.getMyVideosList, profileID], {
    fetcher: (url) => dataFetcher(url),
    onSuccess: ({ success, data }) => {
      if (success) {
        setVideosGroup([{ title: 'My Videos', id: '-', videos: data }]);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }

      setLoading(false);
    },
    onError: () => {
      setLoading(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { mutate: getCategoryDetails } = useSWR([endpoints.getMyCategory, selectedCategoryID], {
    fetcher: (url, parent_id) => dataFetcher(url, { parent_id }),
    onSuccess: ({ success, data }) => {
      if (success) {
        const tempSubCategories = data;

        if (videosGroup[0]) {
          videosGroup[0]?.videos?.forEach((video) => {
            const index = tempSubCategories?.findIndex(({ id }) => (id === video.subcategory_id));

            if (index >= 0) {
              if (tempSubCategories[index].videos) {
                tempSubCategories[index]?.videos?.push(video);
              } else {
                tempSubCategories[index].videos = [video];
              }
            }
          });

          setSubCategories(tempSubCategories);
        } else {
          setSubCategories([]);
        }
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
      setLoadingCategories(false);
    },
    onError: () => {
      setLoadingCategories(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  useEffect(() => {
    if (selectedCategoryID) {
      setLoadingCategories(true);
      getCategoryDetails();
    }
  }, [selectedCategoryID]);

  const { mutate: getCategories } = useSWR(endpoints.getMyCategory, {
    fetcher: (url) => dataFetcher(url),
    onSuccess: ({ success, data }) => {
      if (success) {
        setCategories(data);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
      setLoadingCategories(false);
    },
    onError: () => {
      setLoadingCategories(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  useEffect(() => { getCategories(); getMyVideos(); }, []);

  return (
    <ProfilePage
      containerClassName={styles.pageContainer}
      mainClassName={cn({ [styles.mainContent]: true, [styles.loading]: loading })}
      userName={profileName}
      isProfileHeader
      userProfileLink={Pathname.getProfilePath()}
    >
      {loading
        ? <CircularProgress />
        : (
          <>
            <div className={styles.heading}>
              <div className={styles.headingBGFade} />
              <div className={styles.headingText}>
                <>Your video collection</>
                {(videosGroup?.length === 0) && ' is \'empty\''}
              </div>
            </div>

            <br />
            <br />

            {(!loading && !!categories?.length) && (
              <div className={styles.imageGroup}>
                <div className={styles.imageGroupImagesWrapper}>
                  <div className={styles.imageGroupImagesContainer}>
                    <div className={styles.imageGroupImagesContent}>
                      {categories?.map(({ id, title, video_count }, idx) => (
                        !!video_count && (
                          <div
                            className={cn({
                              [styles.imageGroupImageLink]: true,
                              [styles.categoryButton]: true,
                              [styles.active]: selectedCategoryID === id,
                            })}
                            key={idx}
                            tabIndex="0"
                            role="button"
                            onKeyPress={undefined}
                            onClick={() => {
                              if (selectedCategoryID === id) {
                                setSelectedCategoryID(undefined);
                              } else {
                                setSelectedCategoryID(id);
                              }
                            }}
                          >
                            {title}
                          </div>
                        )))}
                      <div
                        className={cn({
                          [styles.categoryButton]: true,
                          [styles.dummy]: true,
                        })}
                      />
                    </div>
                  </div>
                </div>
              </div>
            )}

            {selectedCategoryID
              ? loadingCategories
                ? (
                  <div className={styles.categoriesLoader}>
                    <CircularProgress />
                  </div>
                )
                : (
                  <>
                    {!!subCategories?.length && (
                      subCategories?.map((subCategory) => (
                        !!subCategory?.videos?.length && (
                          <ImageGroup
                            isGrid
                            videos={subCategory?.videos}
                            key={subCategory?.id}
                            title={subCategory?.title}
                          />
                        )
                      ))
                    )}
                  </>
                )
              : (
                <>
                  {!!videosGroup?.length && (
                    videosGroup?.map((videoGroup) => (
                      !!videoGroup?.videos?.length && (
                        <ImageGroup
                          isGrid
                          videos={videoGroup?.videos}
                          key={videoGroup?.id}
                          title={videoGroup?.title}
                        />
                      )
                    ))
                  )}
                </>
              )}
          </>
        )}
    </ProfilePage>
  );
};
