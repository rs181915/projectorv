import React, { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { dataFetcher, endpoints } from 'Api';
import { Collapse, Fade, Zoom } from '@material-ui/core';
import { Button, Page, Snackbar, Text } from 'Components';
import { Pathname } from 'Routes';
import { ReactComponent as UploadIcon } from 'Assets/video-upload.svg';
import styles from './index.module.css';

const transitionTimeout = 300;

const SectionTitle = ({ title, action: Action }) => (
  <div className={styles.sectionTitle}>
    <Text.SectionTitle>{title}</Text.SectionTitle>
    {Action && <Action />}
  </div>
);

const UserRequest = ({ email, id, firstname, lastname, setSnackbar }) => {
  const [action, setAction] = useState(undefined);
  const [isVisible, setVisibility] = useState(true);

  const { isValidating: handlingAction, mutate: handleAction } = useSWR([endpoints.updateViewRequest, id, action], {
    fetcher: (url, user, status) => dataFetcher(url, { user, status }),
    onSuccess: ({ success }) => {
      if (success) {
        setVisibility(false);
        setSnackbar({
          isVisible: true,
          message: `${firstname ? `${firstname}'s r` : 'R'}equest ${action === 1 ? 'accepted' : 'declined'}`
        });
      } else {
        setSnackbar({ isVisible: true, message: 'Oops! Something went wrong', type: 'error' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, message: 'Oops! Something went wrong', type: 'error' });
    },
  });

  useEffect(() => { if (action) { handleAction(); } }, [handleAction, action]);

  return (
    <Collapse in={isVisible} timeout={transitionTimeout}>
      <Zoom in={isVisible} timeout={transitionTimeout}>
        <div className={styles.userRequest}>
          <div className={styles.userRequestText}>
            <div className={styles.userRequestName}>{`${firstname} ${lastname}`}</div>
            <div className={styles.userRequestEmail}>{email}</div>
          </div>
          <div className={styles.userRequestActionsContainer}>
            <button
              className={styles.userRequestActionButton}
              onClick={() => { setAction(1); }}
            >
              {(handlingAction && (action === 1)) ? 'Accepting...' : 'Accept'}
            </button>
            <button
              className={cn({ [styles.userRequestActionButton]: true, [styles.delete]: true })}
              onClick={() => { setAction(2); }}
            >
              {(handlingAction && (action === 2)) ? 'Declining...' : 'Decline'}
            </button>
          </div>
        </div>
      </Zoom>
    </Collapse>
  );
};

export const Dashboard = () => {
  const location = useLocation();
  const navigate = useNavigate();

  const [accessRequests, setAccessRequests] = useState([]);
  const [videos, setVideos] = useState([]);

  const [snackbar, setSnackbar] = useState({ isVisible: false, message: '' });

  const { isValidating: loadingAccessRequests, mutate: fetchAccessRequests } = useSWR([endpoints.getAllViewRequest, 0], {
    fetcher: (url, status) => dataFetcher(url, { status }),
    onSuccess: ({ success, data }) => {
      if (success && data) {
        setAccessRequests(data?.filter(({ status }) => (status === '0')));
      } else {
        setAccessRequests([]);
      }
    },
    onError: () => {
      setAccessRequests([]);
    },
  });

  const { isValidating: loadingVideos, mutate: fetchVideos } = useSWR(endpoints.getMyVideosList, {
    onSuccess: ({ success, data }) => {
      if (success && data) {
        setVideos(data);
      } else {
        setVideos([]);
      }
    },
    onError: () => {
      setVideos([]);
    },
  });

  useEffect(() => {
    fetchVideos();
    fetchAccessRequests();
  }, []);

  return (
    <Page contentClassName={styles.pageContent}>
      <div className={styles.dashboard}>
        <Text.PageTitle>Projector Dashboard</Text.PageTitle>
        <div className={styles.sectionsContainer}>
          <section className={cn({ [styles.section]: true, [styles.videoUploadSection]: true })}>
            <div className={styles.videoUploadSectionContent}>
              <UploadIcon className={styles.uploadIcon} />
              <Button
                className={styles.uploadButton}
                onClick={() => { navigate(`${location.pathname}${Pathname.uploadVideo}`); }}
              >
                upload video
              </Button>
            </div>
          </section>
          <section className={cn({ [styles.section]: true, [styles.videoAnalyticsSection]: true })}>
            <SectionTitle title="Video Analytics" />
            <table className={styles.videoAnalyticsList}>
              {loadingVideos
                ? (
                  <tbody>
                    <tr>
                      <td colSpan="2">Loading ...</td>
                    </tr>
                  </tbody>
                )
                : (videos?.length
                  ? (
                    <>
                      <thead>
                        <tr>
                          <td>Video</td>
                          <td>Views</td>
                        </tr>
                      </thead>
                      <tbody>
                        {videos?.slice(0, 5)?.map(({ id, title, views }) => (
                          <tr key={id}>
                            <td>{title}</td>
                            <td>{views}</td>
                          </tr>
                        ))}
                      </tbody>
                    </>
                  )
                  : (
                    <tbody>
                      <tr>
                        <td colSpan="2">Upload videos to see the views here</td>
                      </tr>
                    </tbody>
                  )
                )}
            </table>
          </section>
          <section className={cn({ [styles.section]: true, [styles.storageSection]: true })}>
            <SectionTitle title="Storage" />
            <div className={styles.storageSectionContent}>
              <div className={styles.storageInfoText}>projector storage: 1 TB (350&nbsp;GB&nbsp;Available)</div>
              <div className={styles.storageInfoBar}>
                <div className={cn({ [styles.storageInfoBarContent]: true, [styles.storageInfoBarVideos]: true })}>Videos</div>
                <div className={cn({ [styles.storageInfoBarContent]: true, [styles.storageInfoBarPhotos]: true })}>Photos</div>
                <div
                  className={cn({ [styles.storageInfoBarContent]: true, [styles.storageInfoBarAvailable]: true })}
                >
                  350 GB
                </div>
              </div>
              <br />
              <button className={styles.storageManagementButton}>Manage</button>
            </div>
          </section>
          <section className={cn({ [styles.section]: true, [styles.userManagementSection]: true })}>
            <SectionTitle
              title="Manage Users"
              action={() => <Link className={styles.addUserButton} to={Pathname.requestAccess}>+</Link>}
            />
            <Collapse in={!loadingAccessRequests && !!accessRequests?.length} timeout={transitionTimeout}>
              <Zoom in={!loadingAccessRequests && !!accessRequests?.length} timeout={transitionTimeout}>
                <div>
                  {accessRequests?.map(({ id, firstname, lastname, email }, idx) => (
                    <UserRequest
                      email={email}
                      id={id}
                      key={idx}
                      firstname={firstname}
                      lastname={lastname}
                      setSnackbar={setSnackbar}
                    />
                  ))}
                </div>
              </Zoom>
            </Collapse>
            <Collapse in={!loadingAccessRequests && !accessRequests?.length} timeout={transitionTimeout}>
              <Zoom in={!loadingAccessRequests && !accessRequests?.length} timeout={transitionTimeout}>
                <div className={styles.accessRequestNotes}>No new requests</div>
              </Zoom>
            </Collapse>
            <Collapse in={loadingAccessRequests} timeout={transitionTimeout}>
              <Fade in={loadingAccessRequests} timeout={transitionTimeout}>
                <div className={styles.accessRequestNotes}>Loading...</div>
              </Fade>
            </Collapse>
          </section>
        </div>
      </div>

      <Snackbar {...snackbar} onClose={() => { setSnackbar({ ...snackbar, isVisible: false }); }} />
    </Page>
  );
};
