import React, { useContext, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import cn from 'classnames';
import moment from 'moment';
import useSWR from 'swr';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { AppContext } from 'Context';
import { Page } from 'Components';
import { dataFetcher, endpoints } from 'Api';
import PublishIcon from '@material-ui/icons/Publish';
import EditIcon from '@material-ui/icons/Edit';
import { ReactComponent as DragIcon } from 'Assets/drag.svg';
import styles from './index.module.css';

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const ContentGroup = ({ id: layoutID, groups, groupType }) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [items, setItems] = useState(groups);
  const [newOrder, updateNewOrder] = useState(undefined);

  const { isValidating: updatingNewOrder, mutate: updateOrder } = useSWR([
    groupType === 'category' ? endpoints.updateCategoryListVideoOrder : endpoints.updatePlayListVideoOrder,
    newOrder?.items,
    layoutID,
  ], {
    fetcher: (url, newItems, currentLayoutID) => dataFetcher(url, {
      ...(groupType === 'playlist') && { playlist_id: currentLayoutID },
      ...(groupType === 'category') && { category_id: currentLayoutID },
      items: newItems,
    }),
    onSuccess: ({ success }) => {
      if (success) {
        setAppSnackbar({
          isVisible: true,
          message: 'Order updated',
        });
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while updating your order',
        });
      }
    },
    onError: () => { },
  });

  const onDragEnd = (result) => {
    if (!result.destination) { return; }

    const newItems = reorder(items, result.source.index, result.destination.index);
    const changedItems = {};

    items?.forEach((item, idx) => {
      if (item?.id !== newItems[idx]?.id) {
        changedItems[newItems[idx]?.id] = (idx + 1);
      }
    });

    if (result.source.index !== result.destination.index) {
      updateNewOrder({ items: changedItems });
    }

    setItems(newItems);
  };

  useEffect(() => { if (newOrder) { updateOrder(); } }, [newOrder]);

  return (
    <div className={styles.contentGroup}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable">
          {(droppableProvided, droppableSnapshot) => (
            <div
              className={styles.contentGroupBody}
              ref={droppableProvided.innerRef}
              {...droppableProvided.droppableProps}
            >
              {items.map(({ id, description, title, thumbnails: images }, idx) => (
                <Draggable key={id} draggableId={id} index={idx}>
                  {(draggableProvided, draggableSnapshot) => (
                    <div
                      className={cn({
                        [styles.contentGroupBodyRow]: true,
                        [styles.dragged]: draggableSnapshot.isDragging,
                      })}
                      key={idx}
                      ref={draggableProvided.innerRef}
                      {...draggableProvided.draggableProps}
                      style={draggableProvided.draggableProps.style}
                    >
                      <div className={styles.contentGroupBodyCell}>
                        <div className={styles.categoryContainer}>
                          <div className={styles.categoryContainerImages}>
                            <div className={styles.dragIconContainer} {...draggableProvided.dragHandleProps}>
                              <DragIcon className={styles.dragIcon} />
                            </div>
                            <div className={styles.imageContainer}>
                              <img src={images[0]} alt={title} className={styles.image} />
                            </div>
                            <div className={styles.contentGroupTextContainer}>
                              <div className={styles.contentGroupTitle}>{title}</div>
                              <div className={styles.contentGroupDescription}>{description}</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div style={{ textAlign: 'center' }} className={styles.contentGroupBodyCell}>
                        <button className={styles.removeButton}>{`Remove from ${groupType}`}</button>
                      </div>
                    </div>
                  )}
                </Draggable>
              ))}
              {droppableProvided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </div>
  );
};

export const ContentLayoutDashboard = () => {
  const { setAppSnackbar } = useContext(AppContext);

  const { id, layoutType } = useParams();

  const [prevTitle, setPrevTitle] = useState(undefined);
  const [title, setTitle] = useState(undefined);
  const [isDisabled, setDisabled] = useState(true);
  const [categoryData, setCategoryData] = useState(undefined);
  const [playlistData, setPlaylistData] = useState(undefined);

  const { isValidating: gettingCategory, mutate: getCategory } = useSWR([endpoints.getMyCategory, id], {
    fetcher: (url, category_id) => dataFetcher(url, { category_id }),
    onSuccess: ({ success, data }) => {
      if (success) {
        const requiredData = data[0];
        let viewsCount = 0;

        requiredData?.videos?.forEach(({ views }) => { viewsCount += parseInt(views, 10); });

        setCategoryData({ ...requiredData, itemsCount: requiredData?.video_count, viewsCount });
        setTitle(requiredData?.title);
        setPrevTitle(requiredData?.title);
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { isValidating: gettingPlaylist, mutate: getPlaylist } = useSWR([endpoints.getMyPlaylist, id], {
    fetcher: (url, playlist_id) => dataFetcher(url, { playlist_id }),
    onSuccess: ({ success, data }) => {
      if (success) {
        const requiredData = data[0];
        let viewsCount = 0;

        requiredData?.videos?.forEach(({ views }) => { viewsCount += parseInt(views, 10); });

        setPlaylistData({ ...requiredData, itemsCount: requiredData?.video_count, viewsCount });
        setTitle(requiredData?.title);
        setPrevTitle(requiredData?.title);
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { isValidating: updatingCategory, mutate: updateCategory } = useSWR([endpoints.addEditCategory, id, title], {
    fetcher: (url, category_id, inputTitle) => dataFetcher(url, { category_id, title: inputTitle }),
    onSuccess: ({ success }) => {
      if (success) {
        setPrevTitle(title);
        setAppSnackbar({
          isVisible: true,
          message: 'Category title was updated',
        });
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while updating title',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating title' });
    },
  });

  const { isValidating: updatingPlaylist, mutate: updatePlaylist } = useSWR([endpoints.addEditPlaylist, id, title], {
    fetcher: (url, playlist_id, inputTitle) => dataFetcher(url, { playlist_id, title: inputTitle }),
    onSuccess: ({ success }) => {
      if (success) {
        setPrevTitle(title);
        setAppSnackbar({
          isVisible: true,
          message: 'Playlist title was updated',
        });
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while updating title',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating title' });
    },
  });

  useEffect(() => {
    if (layoutType === 'category') {
      getCategory();
    } else {
      getPlaylist();
    }
  }, []);

  return (
    <Page className={styles.page} contentClassName={styles.pageContent}>
      <div className={styles.preview}>
        <div className={styles.previewContent}>
          <img src="" className={styles.previewImage} />
          <div className={styles.previewInputContainer}>
            {gettingCategory
              ? (
                <div className={styles.previewTitleLoading}>Loading ...</div>
              )
              : categoryData
                ? (
                  <input
                    className={styles.previewTitle}
                    defaultValue={categoryData?.title || ''}
                    disabled={isDisabled}
                    onChange={(e) => { setTitle(e?.target?.value); }}
                  />
                )
                : null}
            {gettingPlaylist
              ? (
                <div className={styles.previewTitleLoading}>Loading ...</div>
              )
              : playlistData
                ? (
                  <input
                    autoFocus={!isDisabled}
                    className={styles.previewTitle}
                    defaultValue={playlistData?.title || ''}
                    disabled={isDisabled}
                    onChange={(e) => { setTitle(e?.target?.value); }}
                  />
                )
                : null}
            {isDisabled
              ? <EditIcon className={styles.previewIcon} onClick={() => { setDisabled(false); }} />
              : (
                <PublishIcon
                  className={styles.previewIcon}
                  onClick={() => {
                    if (title?.trim()) {
                      if (layoutType === 'category' ? !updatingCategory : !updatingPlaylist) {
                        if (title !== prevTitle) {
                          if (layoutType === 'category') {
                            updateCategory();
                          } else {
                            updatePlaylist();
                          }
                        }
                        setDisabled(true);
                      }
                    }
                  }}
                />
              )}
          </div>
          <div className={styles.previewDetails}>
            {(layoutType === 'category' ? gettingCategory : gettingPlaylist)
              ? 'Loading ...'
              : `${(layoutType === 'category' ? categoryData : playlistData)?.itemsCount}
                 Video${((layoutType === 'category' ? categoryData : playlistData)?.itemsCount > 0) ? 's' : ''}
                 • ${(layoutType === 'category' ? categoryData : playlistData)?.viewsCount}
                 View${((layoutType === 'category' ? categoryData : playlistData)?.viewsCount > 0) ? 's' : ''}
                 • Updated ${moment((layoutType === 'category'
                ? categoryData
                : playlistData)?.updated).format('MM/DD')}`}
          </div>
        </div>
      </div>
      <div className={styles.contentGroupContainer}>
        {(layoutType === 'category' ? gettingCategory : gettingPlaylist)
          ? (
            <div className={styles.loader}>
              <div className={styles.loaderContent}>Loading ...</div>
            </div>
          )
          : (layoutType === 'category' ? categoryData : playlistData)?.videos?.length
            ? (
              <ContentGroup
                id={id}
                groups={(layoutType === 'category' ? categoryData : playlistData)?.videos}
                groupType={layoutType}
              />
            )
            : <div className={styles.dataText}>No Videos uploaded yet.</div>}
      </div>
    </Page>
  );
};

// "API: to update order of video under playlist
// updatePlayListVideoOrder
// params:
// token, items and  playlist_id
// items = {"video_id":"order_number"}"
