import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { AppContext } from 'Context';
import { Page, Text } from 'Components';
import { dataFetcher, endpoints } from 'Api';
import { Pathname } from 'Routes';
import { ReactComponent as AddIcon } from 'Assets/add.svg';
import { ReactComponent as DragIcon } from 'Assets/drag.svg';
import { ReactComponent as EditIcon } from 'Assets/edit.svg';
import { ReactComponent as DropdownIcon } from 'Assets/chevron-down.svg';
import styles from './index.module.css';

const CreateMenuDropdown = ({ alignRight, defaultName, isLoading, onSelect = () => { }, options }) => {
  const [name, setName] = useState(defaultName);
  const [isOpen, setOpen] = useState(false);

  return (
    <div
      className={styles.createMenuDropdown}
      onClick={() => { if (!isOpen && !isLoading) { setOpen(true); } }}
      onKeyPress={undefined}
      role="button"
      tabIndex={0}
    >
      <div className={styles.createMenuDropdownInput}>
        {isLoading ? 'Loading ...' : name}
        &nbsp;
        <DropdownIcon style={{ ...isOpen && { transform: 'scaleX(-1)' } }} />
      </div>
      {isOpen && !isLoading && (
        <>
          <div
            className={styles.createMenuDropdownScrim}
            onClick={() => { setOpen(false); }}
            onKeyPress={undefined}
            role="button"
            tabIndex={0}
          />
          <div className={cn({ [styles.createMenuDropdownList]: true, [styles.alignRight]: alignRight })}>
            {options.map(({ name: title, value }, idx) => (
              <div
                className={styles.createMenuDropdownItem}
                key={idx}
                onClick={() => {
                  onSelect(value);
                  setName(title);
                  setOpen(false);
                }}
                onKeyPress={undefined}
                role="button"
                tabIndex={0}
              >
                {title}
              </div>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

const CategoryIDDropdown = ({ onSelect }) => {
  const [options, setOptions] = useState([]);

  // the following useSWR hook's first parameter needs to be an array in this case
  const { isValidating: fetchingCategories, mutate: fetchCategories } = useSWR([endpoints.getMyCategory], {
    fetcher: (url) => dataFetcher(url),
    onSuccess: ({ success, data }) => {
      if (success) {
        setOptions(data?.map(({ title: name, id: value }) => ({ name, value })));
      }
    },
    onError: () => { },
  });

  useEffect(() => { fetchCategories(); }, []);

  return (
    <CreateMenuDropdown
      alignRight
      isLoading={fetchingCategories}
      defaultName="Select Category"
      onSelect={onSelect}
      options={options}
    />
  );
};

const CreateButton = ({
  bottomLeftComponent: BLC,
  buttonLabel,
  isCompact,
  onCreate,
  title,
}) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [val, setVal] = useState(undefined);
  const [isMenuOpen, setMenuOpen] = useState(false);
  const [creating, setCreatingStatus] = useState(false);

  const { mutate: addNewPlaylist } = useSWR([endpoints.addEditPlaylist, val, onCreate], {
    fetcher: (url, playlistName) => dataFetcher(url, { title: playlistName }),
    onSuccess: ({ success }) => {
      if (success) {
        if (onCreate) {
          onCreate();
        }
        setCreatingStatus(false);
        setVal(undefined);
        setMenuOpen(false);
        setAppSnackbar({
          isVisible: true,
          message: `Playlist '${val}' was created`,
        });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your playlist' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your playlist' });
    },
  });

  return (
    <div className={styles.createButtonWrapper}>
      <button
        className={cn({ [styles.createButton]: true, [styles.compact]: isCompact })}
        onClick={() => { if (!isMenuOpen) { setMenuOpen(true); } }}
      >
        <span>{buttonLabel}</span>
        {!isCompact && <AddIcon className={styles.addIcon} />}
      </button>
      {isMenuOpen && (
        <div className={styles.createMenuContainer}>
          <div
            className={styles.createButtonScrim}
            onClick={() => { if (!creating) { setVal(undefined); setMenuOpen(false); } }}
            onKeyPress={undefined}
            role="button"
            tabIndex={0}
          />
          <div className={styles.createMenu}>
            <div className={styles.createMenuTitle}>{title}</div>
            <input
              className={styles.createMenuInput}
              defaultValue={val}
              onInput={(e) => { setVal(e.target.value); }}
            />
            <div className={styles.createMenuButtons}>
              {BLC && (
                <div className={styles.createMenuButtonsLeft}>
                  <BLC />
                </div>
              )}
              <div className={styles.createMenuButtonsRight}>
                <button
                  className={styles.createMenuCancelButton}
                  disabled={creating}
                  onClick={() => { setVal(undefined); setMenuOpen(false); }}
                >
                  Cancel
                </button>
                <button
                  className={styles.createMenuCreateButton}
                  disabled={creating || (!val?.trim())}
                  onClick={() => {
                    setCreatingStatus(true);
                    addNewPlaylist();
                  }}
                >
                  {creating ? 'Creating ...' : 'Create'}
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

const CategoryCreateButton = ({ buttonLabel, isCompact, onCreate }) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [categoryID, setCategoryID] = useState(undefined);
  const [categoryType, setCategoryType] = useState('category');
  const [val, setVal] = useState(undefined);
  const [isMenuOpen, setMenuOpen] = useState(false);
  const [creating, setCreatingStatus] = useState(false);

  const { mutate: addNewCategory } = useSWR([endpoints.addEditCategory, val, categoryID, onCreate], {
    fetcher: (url, categoryName, parentCategoryID) => dataFetcher(url, {
      title: categoryName,
      ...parentCategoryID && { parent_id: parentCategoryID }
    }),
    onSuccess: ({ success }) => {
      if (success) {
        setCreatingStatus(false);
        setVal(undefined);
        setCategoryType('category');
        setCategoryID(undefined);
        setMenuOpen(false);
        if (categoryType === 'category') {
          if (onCreate) {
            onCreate();
          }
        }
        setAppSnackbar({
          isVisible: true,
          message: `${categoryID ? 'Sub-Category' : 'Category'} '${val}' was created`,
        });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your category' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your category' });
    },
  });

  useEffect(() => { setCategoryID(undefined); }, [categoryType]);

  useEffect(() => { setVal(undefined); setCategoryType('category'); setCategoryID(undefined); }, [setMenuOpen]);

  return (
    <div className={styles.createButtonWrapper}>
      <button
        className={cn({ [styles.createButton]: true, [styles.compact]: isCompact })}
        onClick={() => { setMenuOpen(true); }}
      >
        <span>{buttonLabel}</span>
        {!isCompact && <AddIcon className={styles.addIcon} />}
      </button>
      {isMenuOpen && (
        <div className={styles.createMenuContainer}>
          <div
            className={styles.createButtonScrim}
            onClick={() => { if (!creating) { setCategoryType('category'); setMenuOpen(false); } }}
            onKeyPress={undefined}
            role="button"
            tabIndex={0}
          />
          <div className={styles.createMenu}>
            <div className={styles.createMenuTitle}>
              <div className={styles.categoryDropdownContainer}>
                <CreateMenuDropdown
                  defaultName="New Category"
                  defaultValue={categoryType}
                  onSelect={setCategoryType}
                  options={[
                    { name: 'New Category', value: 'category' },
                    { name: 'New Sub Category', value: 'sub-category' },
                  ]}
                />
                {(categoryType === 'sub-category') && <CategoryIDDropdown onSelect={setCategoryID} />}
              </div>
            </div>
            <input
              className={styles.createMenuInput}
              defaultValue={val}
              onInput={(e) => { setVal(e.target.value); }}
            />
            <div className={styles.createMenuButtons}>
              <div className={styles.createMenuButtonsRight}>
                <button
                  className={styles.createMenuCancelButton}
                  disabled={creating}
                  onClick={() => { setCategoryType('category'); setMenuOpen(false); }}
                >
                  Cancel
                </button>
                <button
                  className={styles.createMenuCreateButton}
                  disabled={((categoryType === 'sub-category') ? !categoryID : false) || !val || !(val?.trim()) || creating}
                  onClick={() => { setCreatingStatus(true); addNewCategory(); }}
                >
                  {creating ? 'Creating ...' : 'Create'}
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

const EditButton = ({ onClick }) => (
  <button className={styles.editButton} onClick={onClick}>
    <div>Edit</div>
    <div className={styles.editButtonDropdown}>▾</div>
  </button>
);

const newOrderList = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const ContentGroup = ({ groups, groupType, isLoading, title }) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [items, setItems] = useState(groups);
  const [newOrder, updateNewOrder] = useState(undefined);

  const { isValidating: updatingNewOrder, mutate: updateOrder } = useSWR([
    groupType === 'category' ? endpoints.updateCategoryOrder : endpoints.updatePlaylistOrder,
    newOrder?.items,
  ], {
    fetcher: (url, newItems) => dataFetcher(url, { items: newItems }),
    onSuccess: ({ success }) => {
      if (success) {
        setAppSnackbar({
          isVisible: true,
          message: 'Order updated',
        });
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while updating your order',
        });
      }
    },
    onError: () => { },
  });

  const onDragEnd = (result) => {
    if (!result.destination) { return; }

    const newItems = newOrderList(items, result.source.index, result.destination.index);
    const changedItems = {};

    items?.forEach((item, idx) => {
      if (item?.id !== newItems[idx]?.id) {
        changedItems[newItems[idx]?.id] = (idx + 1);
      }
    });

    if (result.source.index !== result.destination.index) {
      updateNewOrder({ items: changedItems });
    }

    setItems(newItems);
  };

  useEffect(() => { setItems(groups); }, [groups]);
  useEffect(() => { if (newOrder) { updateOrder(); } }, [newOrder]);

  return (
    <div className={styles.contentGroup}>
      <div className={styles.contentGroupHead}>
        <div className={styles.contentGroupHeadRow}>
          <div className={cn({ [styles.contentGroupHeadCell]: true, [styles.contentGroupText]: true })}>
            {title}
          </div>
          <div
            className={cn({ [styles.contentGroupHeadCell]: true, [styles.contentGroupText]: true })}
            style={{ textAlign: 'center' }}
          >
            Actions
          </div>
        </div>
      </div>
      {isLoading
        ? (
          <div className={styles.contentGroupBody}>
            <div className={styles.loader}>
              <div className={styles.loaderContent}>Loading Categories ...</div>
            </div>
          </div>
        )
        : ((items?.length)
          ? (
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId={groupType}>
                {(droppableProvided) => (
                  <div
                    className={styles.contentGroupBody}
                    ref={droppableProvided.innerRef}
                    {...droppableProvided.droppableProps}
                  >
                    {items.map(({ id, title: name, thumbnails: images, totalImages }, idx) => (
                      <Draggable key={id} draggableId={id} index={idx}>
                        {(draggableProvided, draggableSnapshot) => (
                          <div
                            className={cn({
                              [styles.contentGroupBodyRow]: true,
                              [styles.dragged]: draggableSnapshot.isDragging,
                            })}
                            key={idx}
                            ref={draggableProvided.innerRef}
                            {...draggableProvided.draggableProps}
                            style={draggableProvided.draggableProps.style}
                          >
                            <div className={styles.contentGroupBodyCell}>
                              <div className={cn({ [styles.name]: true, [styles.contentGroupText]: true })}>{name}</div>
                              <div className={styles.categoryContainer}>
                                <div className={styles.categoryContainerImages}>
                                  <div className={styles.dragIconContainer} {...draggableProvided.dragHandleProps}>
                                    <DragIcon className={styles.dragIcon} />
                                  </div>
                                  <div className={styles.imageContainer}>
                                    {images?.map((imageURL, idx2) => (
                                      <img key={idx2} src={imageURL} alt={name} className={styles.image} />
                                    ))}
                                    {((totalImages - images?.length) > 0) && (
                                      <div className={styles.image}>
                                        <div className={styles.totalImages}>{`+${totalImages - images?.length}`}</div>
                                      </div>
                                    )}
                                  </div>
                                </div>
                                <Link to={Pathname.getContentLayoutPath(groupType, id)}>
                                  <EditIcon className={styles.editIcon} style={{ color: '#818181' }} />
                                </Link>
                              </div>
                            </div>
                            <div style={{ textAlign: 'center' }} className={styles.contentGroupBodyCell}>
                              <Link to={Pathname.getContentLayoutPath(groupType, id)}>
                                <EditButton />
                              </Link>
                            </div>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {droppableProvided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          )
          : (
            <div className={styles.contentGroupBody}>
              <div className={styles.contentGroupBodyRow}>
                <br />
                {groupType === 'category'
                  ? 'Categories you create will be displayed here'
                  : 'Playlists you create will be displayed here'}
              </div>
            </div>
          ))}
    </div>
  );
};

// const PlaylistDropdown = ({ onSelect }) => (
//   <CreateMenuDropdown
//     options={[
//       { name: 'Public', value: 'Public' },
//       { name: 'Group', value: 'Group' },
//       { name: 'Private', value: 'Private' },
//     ]}
//     onSelect={onSelect}
//   />
// );

export const ContentLayout = () => {
  const { setAppSnackbar } = useContext(AppContext);

  const [categoriesData, setCategoriesData] = useState([]);
  const [playlistsData, setPlaylistsData] = useState([]);

  const { isValidating: gettingCategories, mutate: getCategories } = useSWR(endpoints.getMyCategory, {
    onSuccess: ({ success, data }) => {
      if (success) {
        setCategoriesData(data?.sort(({ order_number: order_number_1 }, { order_number: order_number_2 }) => {
          if (order_number_1 > order_number_2) { return 1; }

          if (order_number_2 > order_number_1) { return -1; }

          return 0;
        }));
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while fetching your categories',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while fetching your categories' });
    },
  });

  const { isValidating: gettingPlaylists, mutate: getPlaylists } = useSWR(endpoints.getMyPlaylist, {
    onSuccess: ({ success, data }) => {
      if (success) {
        setPlaylistsData(data);
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while fetching your categories',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while fetching your categories' });
    },
  });

  useEffect(() => { getCategories(); getPlaylists(); }, []);

  return (
    <Page className={styles.page} contentClassName={styles.pageContent}>
      <div className={styles.contentLayoutHeader}>
        <Text.PageTitle className={styles.pageTitle}>Content Layout</Text.PageTitle>
        <div className={styles.createButtonsContainer}>
          <CreateButton
            // bottomLeftComponent={PlaylistDropdown}
            buttonLabel="Add playlist"
            isCompact
            onCreate={getPlaylists}
            title="Playlist Title"
            type="playlist"
          />
          <CategoryCreateButton
            isCompact
            buttonLabel="Add Categories"
            // onCreate={getCategories}
            type="category"
          />
          <CreateButton
            // bottomLeftComponent={PlaylistDropdown}
            buttonLabel="Create new playlist"
            onCreate={getPlaylists}
            title="Playlist Title"
            type="playlist"
          />
          <CategoryCreateButton
            buttonLabel="Create Categories"
            // onCreate={getCategories}
            type="category"
          />
        </div>
      </div>
      <div className={styles.contentGroupContainer}>
        <ContentGroup
          groups={categoriesData}
          groupType="category"
          isLoading={gettingCategories}
          title="Categories"
        />
        <ContentGroup
          groups={playlistsData}
          groupType="playlist"
          isLoading={gettingPlaylists}
          title="Playlists"
        />
      </div>
    </Page>
  );
};
