import React, { useContext, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { CircularProgress as MuiCircularProgress, withStyles } from '@material-ui/core';
import { dataFetcher, endpoints } from 'Api';
import { ProfilePage } from 'Components';
import { AppContext } from 'Context';
import { Pathname } from 'Routes';
import styles from './index.module.css';

const CircularProgress = withStyles({
  circle: {
    color: '#fff !important'
  }
})(MuiCircularProgress);

const ImageGroupTitle = ({ title }) => (<h3 className={styles.imageGroupTitle}>{title}</h3>);

const ImageGroup = ({ title, videos, isGrid }) => (
  <div className={styles.imageGroup}>
    <ImageGroupTitle title={title} />
    <div className={styles.imageGroupImagesWrapper}>
      <div className={cn({ [styles.imageGroupImagesContainer]: true, [styles.isGrid]: isGrid })}>
        <div className={styles.imageGroupImagesContent}>
          {videos?.map(({ id, thumbnails, title: videoTitle }, idx) => (
            <Link className={styles.imageGroupImageLink} key={idx} to={Pathname.getVideoPath(id)}>
              <img src={thumbnails} className={styles.imageGroupImage} />
              <div>
                <h4>{videoTitle}</h4>
              </div>
            </Link>
          ))}
          <div className={cn({ [styles.imageGroupImageLink]: true, [styles.dummy]: true })} />
        </div>
      </div>
    </div>
  </div>
);

export const Profile = () => {
  const { setAppSnackbar } = useContext(AppContext);
  const { profileID } = useParams();

  const [selectedCategoryID, setSelectedCategoryID] = useState(undefined);
  const [loadingCategories, setLoadingCategories] = useState(false);
  const [loading, setLoading] = useState(true);
  const [profileName, setProfileName] = useState(undefined);
  const [profileHeroImage, setProfileHeroImage] = useState(undefined);
  const [resumeWatching, setResumeWatching] = useState([]);
  const [playlists, setPlaylists] = useState([]);
  const [categories, setCategories] = useState([]);
  const [subCategories, setSubCategories] = useState([]);

  const { mutate: getFriendProfile } = useSWR([endpoints.getFriendsVideosList, profileID], {
    fetcher: (url, user_id) => dataFetcher(url, { user_id }),
    onSuccess: ({ success, videos, freind, CurrentlyWatching, playlist, category }) => {
      if (success) {
        if (freind?.firstname) {
          setProfileName(freind?.firstname?.substring(0, 10));
        } else if (freind?.email) {
          setProfileName(freind?.email?.split('@')[0]?.substring(0, 10));
        }
        if (videos?.length) {
          setProfileHeroImage(videos[0]?.thumbnails);
        }
        if (CurrentlyWatching?.length) {
          setResumeWatching(CurrentlyWatching);
        }
        if (playlist?.length) {
          setPlaylists(playlist);
        }
        if (category?.length) {
          setCategories(category);
        }
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }

      setLoading(false);
    },
    onError: () => {
      setLoading(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { mutate: getCategoryDetails } = useSWR([endpoints.getCategoryDetail, profileID, selectedCategoryID], {
    fetcher: (url, user_id, category_id) => dataFetcher(url, { user_id, category_id }),
    onSuccess: ({ success, videos }) => {
      if (success) {
        setSubCategories([{ title: '', videos }]);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
      setLoadingCategories(false);
    },
    onError: () => {
      setLoadingCategories(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  useEffect(() => {
    if (selectedCategoryID) {
      setLoadingCategories(true);
      getCategoryDetails();
    }
  }, [selectedCategoryID]);

  useEffect(() => { getFriendProfile(); }, []);

  return (
    <ProfilePage
      containerClassName={styles.pageContainer}
      mainClassName={cn({ [styles.mainContent]: true, [styles.loading]: loading })}
      userName={profileName}
      isProfileHeader
      userProfileLink={Pathname.getFriendProfilePath(profileID)}
    >
      {loading
        ? <CircularProgress />
        : (
          <>
            <div className={styles.heading}>
              <div className={styles.headingBGFade} />
              <div className={styles.headingText}>
                {profileName}
                {'\'s video collection'}
                {((categories?.length + playlists?.length) === 0) && ' is \'empty\''}
              </div>
            </div>

            <br />
            <br />

            {!!categories?.length && (
              <div className={styles.imageGroup}>
                <div className={styles.imageGroupImagesWrapper}>
                  <div className={styles.imageGroupImagesContainer}>
                    <div className={styles.imageGroupImagesContent}>
                      {categories?.map(({ id, title }, idx) => (
                        <div
                          className={cn({
                            [styles.imageGroupImageLink]: true,
                            [styles.categoryButton]: true,
                            [styles.active]: selectedCategoryID === id,
                          })}
                          key={idx}
                          tabIndex="0"
                          role="button"
                          onKeyPress={undefined}
                          onClick={() => {
                            if (selectedCategoryID === id) {
                              setSelectedCategoryID(undefined);
                            } else {
                              setSelectedCategoryID(id);
                            }
                          }}
                        >
                          {title}
                        </div>
                      ))}
                      <div
                        className={cn({
                          [styles.categoryButton]: true,
                          [styles.dummy]: true,
                        })}
                      />
                    </div>
                  </div>
                </div>
              </div>
            )}

            {selectedCategoryID
              ? loadingCategories
                ? (
                  <div className={styles.categoriesLoader}>
                    <CircularProgress />
                  </div>
                )
                : (
                  <>
                    {!!subCategories?.length && (
                      subCategories?.map((subCategory, idx) => (
                        !!subCategory?.videos?.length && (
                          <ImageGroup
                            isGrid
                            videos={subCategory?.videos}
                            key={idx}
                            title={subCategory?.title}
                          />
                        )
                      ))
                    )}
                  </>
                )
              : (
                <>
                  {!!resumeWatching?.length && (
                    <ImageGroup
                      title="Resume Watching"
                      videos={resumeWatching}
                    />
                  )}

                  {!!categories?.length && (
                    categories?.map((category) => (
                      !!category?.videos?.length && (
                        <ImageGroup
                          isGrid
                          videos={category?.videos}
                          key={category?.id}
                          title={category?.title}
                        />
                      )
                    ))
                  )}

                  {!!playlists?.length && (
                    playlists?.map((playlist) => (
                      !!playlist?.videos?.length && (
                        <ImageGroup
                          isGrid
                          videos={playlist?.videos}
                          key={playlist?.id}
                          title={playlist?.title}
                        />
                      )
                    ))
                  )}
                </>
              )}
          </>
        )}
    </ProfilePage>
  );
};
