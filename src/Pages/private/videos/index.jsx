import React, { useContext, useState, useEffect } from 'react';
import cn from 'classnames';
import {
  FormControlLabel,
  Radio as MuiRadio,
  RadioGroup,
  Table,
  TableBody,
  TableCell as MuiTableCell,
  TableHead,
  TableRow,
  withStyles,
} from '@material-ui/core';
import useSWR from 'swr';
import CheckIcon from '@material-ui/icons/Check';
import CancelIcon from '@material-ui/icons/Cancel';
import { dataFetcher, endpoints } from 'Api';
import { GroupSelector, Page, Text } from 'Components';
import { AppContext } from 'Context';
import { ReactComponent as FilterIcon } from 'Assets/filter.svg';
import styles from './index.module.css';

const videoVisibilityOptions = [
  { name: 'Anyone can view on projector', value: '2' },
  { name: 'Only I can view', value: '1' },
  { name: 'Choose a group to share with', value: '3' },
];

const Radio = withStyles({ root: { color: 'black' }, checked: { color: 'black !important' } })(MuiRadio);

const PrefDropDown = ({ name, isDefaultOpen, options, onClick }) => {
  const [isOpen, setOpen] = useState(isDefaultOpen);

  return (
    <div
      className={styles.prefDropDown}
      onClick={() => { setOpen(!isOpen); if (onClick) { onClick(); } }}
      onKeyDown={undefined}
      role="button"
      tabIndex={0}
    >
      <div className={styles.prefDropDownText}>{name}</div>
      <div className={cn({ [styles.prefDropDownIcon]: true, [styles.prefDropDownActive]: options && isOpen })}>▾</div>
      {isOpen && options && (
        <>
          <div
            className={styles.prefDropdownScrim}
            onClick={() => { setOpen(false); }}
            onKeyPress={undefined}
            role="button"
            tabIndex={0}
          />
          <div className={styles.optionsContainer}>
            {options.map(({ name: actionName, action }, idx) => (
              <div
                className={styles.option}
                key={idx}
                onClick={action}
                onKeyPress={undefined}
                role="button"
                tabIndex={0}
              >
                {actionName}
              </div>
            ))}
          </div>
        </>
      )}
    </div>
  );
};

const TableCell = withStyles({
  root: {
    borderColor: '#E1E2EB',
    color: '#02071A',
    fontFamily: 'Poppins, sans-serif',
  }
})(MuiTableCell);

const TableHeaderCell = withStyles({
  root: {
    borderTopStyle: 'solid',
    borderTopWidth: '1px',
    fontWeight: '600',
    paddingBottom: '0 !important',
    paddingTop: '0 !important',
    whiteSpace: 'nowrap',
  }
})(TableCell);

const Checkbox = ({ header, status, onClick }) => (
  <div
    className={cn({
      [styles.checkbox]: true,
      [styles.header]: header,
      [styles.checked]: status,
      [styles.indeterminate]: status === undefined,
    })}
    onClick={onClick}
    onKeyPress={undefined}
    role="button"
    tabIndex={0}
  />
);

const CategoriesMenu = ({ categoryID, defaultValue, isSubcategory, onSelect, placeholder, title }) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [isOpen, setOpen] = useState(false);
  const [val, setVal] = useState(undefined);
  const [options, setOptions] = useState([]);
  const [newCategoryName, setNewCategoryName] = useState([]);
  const [createMenuIsVisible, setCreateMenuVisibility] = useState(false);

  const showCreateMenu = () => { setCreateMenuVisibility(true); };
  const hideCreateMenu = () => { setCreateMenuVisibility(false); };

  const { isValidating: gettingCategories, mutate: getCategories } = useSWR([endpoints.getMyCategory, categoryID], {
    fetcher: (url, parent_id) => dataFetcher(url, { ...isSubcategory && { parent_id } }),
    onSuccess: ({ success, data }) => {
      if (success) {
        if (defaultValue) {
          data?.every(({ id }, idx) => {
            if (id === defaultValue) {
              setVal(data[idx]?.title);

              return false;
            }

            return true;
          });
        }
        setOptions(data?.map(({ id, title: name }) => ({ name, value: id })));
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while fetching your categories',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while fetching your categories' });
    },
  });

  const { isValidating: addingNewCategory, mutate: addNewCategory } = useSWR([
    endpoints.addEditCategory, newCategoryName, categoryID,
  ], {
    fetcher: (url, categoryName, parentCategoryID) => dataFetcher(url, {
      title: categoryName,
      ...parentCategoryID && { parent_id: parentCategoryID }
    }),
    onSuccess: ({ success }) => {
      if (success) {
        hideCreateMenu();
        getCategories();
        setAppSnackbar({
          isVisible: true,
          message: `${isSubcategory ? 'Sub-Category' : 'Category'} '${newCategoryName}' was created`,
        });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your category' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your category' });
    },
  });

  useEffect(() => {
    if (isSubcategory) {
      onSelect(undefined);
    }
    setVal(placeholder);
    getCategories();
  }, [categoryID]);

  return (
    <div className={styles.categoriesMenu}>
      <div className={styles.categoriesMenuTitle}>{title}</div>
      <div
        className={styles.categoriesMenuInput}
        onClick={() => { if (isSubcategory ? categoryID : true) { setOpen(true); } }}
        onKeyPress={undefined}
        role="button"
        tabIndex={0}
      >
        <div className={styles.categoriesMenuInputText}>
          {gettingCategories ? 'Loading ...' : val || placeholder}
        </div>
        <div className={styles.categoriesMenuInputIcon}>▾</div>
      </div>
      {!gettingCategories && isOpen && (
        <>
          <div
            className={styles.prefDropdownScrim}
            onClick={() => { if (!addingNewCategory) { setCreateMenuVisibility(false); setOpen(false); } }}
            onKeyPress={undefined}
            role="button"
            tabIndex={0}
          />
          <div className={styles.categoriesMenuOptionsContainer}>
            {createMenuIsVisible
              ? (
                <div className={styles.newCategoryContainer}>
                  <div className={styles.newCategoryBar}>
                    {`Enter new ${isSubcategory ? 'Sub-Category' : 'Category'}`}
                    <button
                      className={styles.newCategoryButton}
                      disabled={addingNewCategory}
                      onClick={hideCreateMenu}
                    >
                      Back
                    </button>
                  </div>
                  <input
                    className={styles.newCategoryInput}
                    onChange={(e) => { setNewCategoryName(e?.target?.value); }}
                    type="text"
                  />
                  <button
                    className={cn({ [styles.newCategoryButton]: true, [styles.newCategoryButtonCreate]: true })}
                    disabled={addingNewCategory || !(newCategoryName.toString()?.trim())}
                    onClick={addNewCategory}
                  >
                    {addingNewCategory ? 'Creating ...' : 'Create'}
                  </button>
                </div>
              )
              : (
                <>
                  {options.map(({ name, value }, idx) => (
                    <div
                      className={styles.categoriesMenuOption}
                      key={idx}
                      onClick={() => { setVal(name); onSelect(value); setOpen(false); }}
                      onKeyPress={undefined}
                      role="button"
                      tabIndex={0}
                    >
                      {name}
                    </div>
                  ))}
                  <button
                    className={cn({ [styles.categoriesMenuOption]: true, [styles.categoriesMenuOptionAdd]: true })}
                    onClick={showCreateMenu}
                  >
                    {`Add new ${title}`}
                  </button>
                </>
              )}
          </div>
        </>
      )}
    </div>
  );
};

const CategoriesMenuContainer = ({
  defaultCategory,
  defaultSubCategory,
  id,
  onChange,
  onClose,
  setActionStatus,
  visibility,
}) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [currentCategory, setCurrentCategory] = useState(defaultCategory);
  const [currentSubCategory, setSubCurrentCategory] = useState(defaultSubCategory);
  const [inputCategory, setInputCategory] = useState(defaultCategory);
  const [inputSubCategory, setInputSubCategory] = useState(defaultSubCategory);
  const [updateData, setUpdateData] = useState(undefined);

  const { isValidating: updating, mutate: updateContent } = useSWR([
    endpoints.addVideoContent, updateData,
  ], {
    fetcher: (url, formdata) => dataFetcher(url, formdata),
    onSuccess: ({ success }) => {
      setActionStatus(false);

      if (success) {
        setCurrentCategory(inputCategory);
        setSubCurrentCategory(inputSubCategory);
        setAppSnackbar({ isVisible: true, message: 'category was updated successfully' });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
    },
    onError: () => {
      setActionStatus(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const update = () => {
    const formData = new FormData();

    formData.append('video_id', id);
    formData.append('category_id', inputCategory);
    formData.append('subcategory_id', inputSubCategory);
    if (visibility !== '0') {
      formData.append('status', 1);
    }
    setActionStatus(true);
    setUpdateData(formData);
  };

  useEffect(() => { if (updateData) { updateContent(); } }, [updateData]);

  useEffect(() => {
    if (currentCategory && currentSubCategory) {
      onChange(id, [
        ['category_id', currentCategory],
        ['subcategory_id', currentSubCategory],
        ...((visibility !== '0') ? ['status', '1'] : [])
      ]);
    }
  }, [currentCategory, currentSubCategory]);

  return (
    <div className={styles.editMenu}>
      <div className={styles.categoriesMenuContainer}>
        <CategoriesMenu
          defaultValue={inputCategory || defaultCategory}
          onSelect={setInputCategory}
          placeholder="Select Category"
          title="Category"
        />
        <CategoriesMenu
          defaultValue={inputSubCategory || defaultSubCategory}
          onSelect={setInputSubCategory}
          categoryID={inputCategory}
          isSubcategory
          placeholder="Select Sub Category"
          title="Sub Category"
        />
      </div>
      <br />
      <button
        className={styles.editMenuUpdateButton}
        disabled={updating
          || !inputCategory || !inputSubCategory
          || ((currentCategory === inputCategory) && (currentSubCategory === inputSubCategory))}
        onClick={update}
      >
        {updating ? 'Updating ...' : 'Update'}
      </button>
      <input id="close-button-input" onClick={onClose} style={{ display: 'none' }} />
    </div>
  );
};

const EditMenu = ({ formField, id, name, onChange, onClose, setActionStatus, value }) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [currentValue, setCurrentValue] = useState(value);
  const [inputValue, setInputValue] = useState(value);
  const [updateData, setUpdateData] = useState(undefined);

  const { isValidating: updating, mutate: updateContent } = useSWR([
    endpoints.addVideoContent, updateData, name,
  ], {
    fetcher: (url, formdata) => dataFetcher(url, formdata),
    onSuccess: ({ success }) => {
      setActionStatus(false);

      if (success) {
        setCurrentValue(inputValue);
        setAppSnackbar({ isVisible: true, message: `${name} was updated successfully` });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
    },
    onError: () => {
      setActionStatus(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const update = () => {
    const formData = new FormData();

    formData.append('video_id', id);
    formData.append(formField, inputValue);

    setActionStatus(true);
    setUpdateData(formData);
  };

  useEffect(() => { if (updateData) { updateContent(); } }, [updateData]);

  useEffect(() => {
    if (currentValue !== value) {
      onChange(id, [[formField, currentValue]]);
    }
  }, [currentValue]);

  return (
    <div className={styles.editMenu}>
      <div className={styles.editMenuTitle}>{name}</div>
      <textarea
        className={styles.editMenuInput}
        defaultValue={value}
        onChange={(e) => { setInputValue(e?.target?.value); }}
        placeholder={value}
      />
      <br />
      <button
        className={styles.editMenuUpdateButton}
        disabled={updating || inputValue === currentValue}
        onClick={update}
      >
        {updating ? 'Updating ...' : 'Update'}
      </button>
      <input id="close-button-input" onClick={onClose} style={{ display: 'none' }} />
    </div>
  );
};

const VisibilityMenu = ({
  category,
  defaultGroup,
  id,
  onChange,
  onClose,
  setActionStatus,
  subCategory,
  value,
}) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [currentValue, setCurrentValue] = useState(value);
  const [inputValue, setInputValue] = useState(value);
  const [group, setGroup] = useState(defaultGroup);
  const [updateData, setUpdateData] = useState(undefined);

  const { isValidating: updating, mutate: updateContent } = useSWR([
    endpoints.addVideoContent, updateData,
  ], {
    fetcher: (url, formdata) => dataFetcher(url, formdata),
    onSuccess: ({ success }) => {
      setActionStatus(false);

      if (success) {
        setCurrentValue(inputValue);
        setAppSnackbar({ isVisible: true, message: 'Visibility was updated successfully' });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
    },
    onError: () => {
      setActionStatus(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const update = () => {
    const formData = new FormData();

    formData.append('video_id', id);
    formData.append('visibility', inputValue);
    if (category && subCategory) {
      formData.append('status', 1);
    }

    if (inputValue === '3') {
      formData.append('group_id', group);
    }

    setActionStatus(true);
    setUpdateData(formData);
  };

  useEffect(() => { if (updateData) { updateContent(); } }, [updateData]);

  useEffect(() => {
    if (currentValue !== value) {
      onChange(id, [['visibility', currentValue], ...((category && subCategory) ? [['status', '1']] : [])]);
    }
  }, [currentValue]);

  return (
    <div className={styles.editMenu}>
      <div className={styles.editMenuTitle}>Visibility</div>
      <RadioGroup
        className={styles.visibilityRadio}
        defaultValue={value}
        name="visibility"
        onChange={(e) => { setGroup(undefined); setInputValue(e?.target?.value); }}
        value={inputValue}
      >
        {videoVisibilityOptions.map(({ name, value: val }, idx) => (
          <FormControlLabel value={val} key={idx} control={<Radio color="primary" />} label={name} />
        ))}
      </RadioGroup>
      {inputValue === '3' && <GroupSelector onSelect={(val) => { setGroup(val); }} selectedOption={group || defaultGroup} />}
      <br />
      <button
        className={styles.editMenuUpdateButton}
        disabled={updating || ((inputValue === '3') ? (!group || (group === defaultGroup)) : (inputValue === currentValue))}
        onClick={update}
      >
        {updating ? 'Updating ...' : 'Update'}
      </button>
      <input id="close-button-input" onClick={onClose} style={{ display: 'none' }} />
    </div>
  );
};

const PreloadData = ({ children }) => (
  <tr className={styles.preLoadDataContainer}>
    <td className={styles.preLoadData} colSpan={7}>
      <div className={styles.preloadDataContent}>{children}</div>
    </td>
  </tr>
);

export const YourVideos = () => {
  const [selectedVideo, selectVideo] = useState(undefined);
  const [videoData, setVideoData] = useState(undefined);
  const [handlingAction, setActionHandlingStatus] = useState(false);
  const [videoList, setVideoList] = useState([]);
  const [editMenu, setEditMenu] = useState(undefined);
  const [visibilityMenu, setVisibilityMenu] = useState(undefined);
  const [categoriesMenu, setCategoriesMenu] = useState(false);

  const { isValidating: fetchingVideo, mutate: fetchVideos } = useSWR(endpoints.getMyVideosList, {
    onSuccess: ({ success, data }) => {
      if (success) {
        setVideoList(data);
      }
    },
    onError: () => { },
  });

  const { isValidating: deletingVideo, mutate: deleteVideo } = useSWR([endpoints.deleteVideo, videoData?.id], {
    fetcher: (url, video_id) => dataFetcher(url, { video_id }),
    onSuccess: ({ success, data }) => {
      if (success) {
        fetchVideos();
      }
    },
    onError: () => { },
  });

  const onDataChange = (id, changes = []) => {
    const changeObjects = {};

    changes.forEach(([key, value]) => {
      changeObjects[key] = value;
    });

    setVideoList(videoList.map((d) => ({ ...d, ...(id === d.id) && changeObjects })));
  };

  const onMenuClose = () => {
    if (!handlingAction) {
      setCategoriesMenu(false);
      setEditMenu(undefined);
      setVisibilityMenu(undefined);
    }
  };

  const toggleVideoSelection = (id, data) => {
    if (selectedVideo === id) {
      selectVideo(undefined);
      setVideoData(undefined);
    } else {
      selectVideo(id);
      setVideoData(data);
    }
  };

  useEffect(() => { if (editMenu) { setCategoriesMenu(undefined); } }, [editMenu]);

  useEffect(() => { if (categoriesMenu) { setEditMenu(undefined); } }, [categoriesMenu]);

  useEffect(() => { fetchVideos(); }, []);

  useEffect(() => {
    const videosPage = document.getElementById('videos-page');

    const onScroll = () => {
      const preferencesBar = document.getElementById('menu-bar-table');

      if (preferencesBar?.offsetTop !== undefined) {
        if (window.pageYOffset > preferencesBar?.offsetTop) {
          videosPage?.classList.add(styles.fixedPreferenceContainer);
          videosPage.style.setProperty('--menu-bar-width', `${videosPage.offsetWidth}px`);
        } else {
          videosPage?.classList.remove(styles.fixedPreferenceContainer);
        }
      } else {
        videosPage?.classList.remove(styles.fixedPreferenceContainer);
      }
    };

    window.addEventListener('scroll', onScroll);

    return () => { window.removeEventListener('scroll', onScroll); };
  }, []);

  return (
    <Page className={styles.page} contentClassName={styles.pageContent} id="videos-page">
      <Text.PageTitle className={styles.pageTitle}>Your Videos</Text.PageTitle>
      <div className={styles.filterBar}>
        <div className={styles.filterBarOrder}>Most Recent</div>
        <div className={styles.filterButton}>
          <FilterIcon className={styles.filterButtonIcon} />
          <div className={styles.filterButtonText}>Filter</div>
        </div>
      </div>
      <div className={styles.videoListContainer}>
        <Table id="menu-bar-table">
          <TableHead className={styles.tableHeader}>
            {selectedVideo ? (
              <>
                <TableRow className={styles.preferenceContainer}>
                  <TableHeaderCell className={styles.preferences} colSpan={7} scope="col">
                    <div className={styles.preferencesContent}>
                      <div className={styles.prefOptions}>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <div className={styles.numVideosSelected}>1 video selected</div>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <PrefDropDown
                          name="Edit"
                          options={[
                            {
                              name: 'Title',
                              action: () => {
                                setCategoriesMenu(false);
                                setVisibilityMenu(undefined);
                                setEditMenu(undefined);
                                setTimeout(() => {
                                  setEditMenu([videoData?.id, 'Title', videoData?.title, 'title']);
                                }, 10);
                              }
                            },
                            {
                              name: 'Description',
                              action: () => {
                                setCategoriesMenu(false);
                                setVisibilityMenu(undefined);
                                setEditMenu(undefined);
                                setTimeout(() => {
                                  setEditMenu([videoData?.id, 'Description', videoData?.description, 'description']);
                                }, 10);
                              },
                            },
                            {
                              name: 'visibility',
                              action: () => {
                                setCategoriesMenu(false);
                                setVisibilityMenu(undefined);
                                setEditMenu(undefined);
                                setTimeout(() => {
                                  setVisibilityMenu([
                                    videoData?.id,
                                    'Visibility',
                                    videoData?.visibility,
                                    'visibility',
                                    videoData?.group_id,
                                    videoData?.category_id,
                                    videoData?.dubcategory_id,
                                  ]);
                                }, 10);
                              },
                            },
                          ]}
                        />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <PrefDropDown
                          name="Categories"
                          isDefaultOpen
                          onClick={() => {
                            setEditMenu(undefined);
                            setVisibilityMenu(undefined);
                            setCategoriesMenu([
                              videoData?.id,
                              videoData?.category_id,
                              videoData?.subcategory_id,
                              videoData?.visibility,
                            ]);
                          }}
                        />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <PrefDropDown
                          name="More Actions"
                          options={[
                            { name: 'Download', action: () => { document.querySelector('#video-download').click(); } },
                            {
                              name: (deletingVideo
                                ? 'Deleting Video...'
                                : 'Delete Video'),
                              action: async () => { await deleteVideo(); },
                            }
                          ]}
                        />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </div>
                      <div className={styles.preferencesContentButtons}>
                        {(editMenu || categoriesMenu) && (
                          <>
                            <label
                              className={styles.cancelEditMenu}
                              htmlFor="close-button-input"
                              style={{ cursor: 'pointer', color: '#5AA5EF' }}
                            >
                              Cancel
                            </label>
                            &nbsp;&nbsp;&nbsp;
                          </>
                        )}
                        <CancelIcon
                          className={styles.closeButton}
                          onClick={() => {
                            setEditMenu(undefined);
                            setVisibilityMenu(false);
                            setCategoriesMenu(false);
                            selectVideo(undefined);
                          }}
                        />
                        &nbsp;&nbsp;&nbsp;
                      </div>
                    </div>
                    <a
                      download={videoData?.title}
                      target="_blank"
                      id="video-download"
                      rel="noreferrer"
                      href={videoData?.video_file}
                      hidden
                    >
                      Download
                    </a>
                    {editMenu && (
                      <EditMenu
                        formField={editMenu[3]}
                        id={editMenu[0]}
                        name={editMenu[1]}
                        onChange={onDataChange}
                        onClose={onMenuClose}
                        setActionStatus={setActionHandlingStatus}
                        value={editMenu[2]}
                      />
                    )}
                    {visibilityMenu && (
                      <VisibilityMenu
                        category={visibilityMenu[5]}
                        subCategory={visibilityMenu[6]}
                        defaultGroup={visibilityMenu[4]}
                        id={visibilityMenu[0]}
                        onChange={onDataChange}
                        onClose={onMenuClose}
                        setActionStatus={setActionHandlingStatus}
                        value={visibilityMenu[2]}
                      />
                    )}
                    {categoriesMenu && (
                      <CategoriesMenuContainer
                        defaultCategory={categoriesMenu[1]}
                        defaultSubCategory={categoriesMenu[2]}
                        id={categoriesMenu[0]}
                        onChange={onDataChange}
                        onClose={onMenuClose}
                        setActionStatus={setActionHandlingStatus}
                        visibility={categoriesMenu[3]}
                      />
                    )}
                  </TableHeaderCell>
                </TableRow>
                <tr className={styles.preferenceContainerFill} />
              </>
            ) : null}
            <TableRow>
              <TableHeaderCell align="center" className={cn({ [styles.headerCell]: true, [styles.checkboxCell]: true })}>
                <Checkbox header onClick={() => { selectVideo(undefined); }} status={selectedVideo ? undefined : false} />
              </TableHeaderCell>
              <TableHeaderCell align="left" className={styles.headerCell}>Video Preview</TableHeaderCell>
              <TableHeaderCell align="center" className={styles.headerCell}>Published</TableHeaderCell>
              <TableHeaderCell align="center" className={styles.headerCell}>Drafts</TableHeaderCell>
              <TableHeaderCell align="center" className={styles.headerCell}>Private</TableHeaderCell>
              <TableHeaderCell align="center" className={styles.headerCell}>Date Published</TableHeaderCell>
              <TableHeaderCell align="center" className={styles.headerCell}>Views</TableHeaderCell>
            </TableRow>
          </TableHead>
          <TableBody className={styles.tableBody}>
            {fetchingVideo
              ? <PreloadData>Loading ...</PreloadData>
              : videoList?.length
                ? videoList?.map(({
                  id, thumbnails = [], title, description, publish_date, status, views, visibility
                }, idx, arr) => ((
                  <TableRow className={cn({ [styles.tableRow]: true, [styles.selectedRow]: (selectedVideo === id), })} key={id}>
                    <TableCell className={styles.bodyCell}>
                      <Checkbox status={selectedVideo === id} onClick={() => { toggleVideoSelection(id, arr[idx]); }} />
                    </TableCell>
                    <TableCell align="left" className={cn({ [styles.bodyCell]: true, [styles.videoPreviewCell]: true })}>
                      <div className={styles.previewContainer}>
                        <img className={styles.previewImage} src={thumbnails[0]} alt={title} />
                        <div className={styles.previewText}>
                          <span className={styles.previewTitle}>{title}</span>
                          <span className={styles.previewDescription}>{description}</span>
                        </div>
                      </div>
                    </TableCell>
                    <TableCell align="center" className={styles.bodyCell}>
                      {status === '1' ? <CheckIcon /> : null}
                    </TableCell>
                    <TableCell align="center" className={styles.bodyCell}>
                      {status === '0' ? <CheckIcon /> : null}
                    </TableCell>
                    <TableCell align="center" className={styles.bodyCell}>
                      {visibility === '1' ? <CheckIcon /> : null}
                    </TableCell>
                    <TableCell align="center" className={styles.bodyCell}>{publish_date?.split(' ')[0]}</TableCell>
                    <TableCell align="center" className={styles.bodyCell}>{views}</TableCell>
                  </TableRow>
                )))
                : <PreloadData>No videos uploaded</PreloadData>}
          </TableBody>
        </Table>
      </div>
    </Page>
  );
};
