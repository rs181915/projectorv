import React from 'react';
import mastercardLogoImage from 'Assets/mastercard@2x.png';
import { SettingsPage } from '../components/page';
import { DataView } from '../components/data-view';
import styles from './index.module.css';

export const BillingSettings = () => (
  <SettingsPage
    title="Billing and Payment"
    description="Edit Payment method, add and remove cards"
  >
    <DataView
      data={[
        {
          labelTitle: 'Add New Card',
          labelHasAddIcon: true,
          data: (
            <div className={styles.paymentCard}>
              <div className={styles.paymentCardNum}>
                5&nbsp;3&nbsp;4&nbsp;4&nbsp;&nbsp;
                *&nbsp;*&nbsp;*&nbsp;*&nbsp;&nbsp;
                *&nbsp;*&nbsp;*&nbsp;*&nbsp;&nbsp;
                *&nbsp;*&nbsp;*&nbsp;*
              </div>
              <img alt="payment card" src={mastercardLogoImage} className={styles.paymentCardProvider} />
            </div>
          ),
        },
      ]}
    />
  </SettingsPage>
);
