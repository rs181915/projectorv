import React from 'react';
import { NavLink } from 'react-router-dom';
import { Page } from 'Components';
import { Pathname } from 'Routes';
import styles from './index.module.css';

const sidebarLinks = [
  { name: 'Account', link: Pathname.getSettingsPath(Pathname.settings.accounts) },
  { name: 'Notifications', link: Pathname.getSettingsPath(Pathname.settings.notifications) },
  { name: 'Privacy', link: Pathname.getSettingsPath(Pathname.settings.privacy) },
  { name: 'Connected Accounts', link: Pathname.getSettingsPath(Pathname.settings.manageUsers) },
  { name: 'Billings and Payments', link: Pathname.getSettingsPath(Pathname.settings.billings) },
];

export const SettingsPage = ({ children, title, description }) => (
  <Page noBottomBar noFixedSidebar contentClassName={styles.pageContent}>
    <div className={styles.settingsPage}>
      <div className={styles.sidebar}>
        <div className={styles.sidebarContent}>
          {sidebarLinks.map(({ name, link }, idx) => (
            <NavLink
              key={idx}
              to={link}
              activeClassName={styles.currentSidebarLink}
              className={styles.sidebarLink}
            >
              {name}
            </NavLink>
          ))}
        </div>
      </div>
      <div className={styles.contentWrapper}>
        <div className={styles.pageTitle}>{title}</div>
        <div className={styles.pageDescription}>{description}</div>
        <hr className={styles.pageDivider} />
        <div className={styles.content}>{children}</div>
      </div>
    </div>
  </Page>
);
