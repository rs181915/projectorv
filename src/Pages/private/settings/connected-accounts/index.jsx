import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import cn from 'classnames';
import pt from 'prop-types';
import useSWR from 'swr';
import {
  Box as MuiBox,
  CircularProgress,
  Collapse,
  Fade,
  Table,
  TableBody,
  TableCell as MuiTableCell,
  TableHead,
  TableRow,
  withStyles,
  Zoom,
} from '@material-ui/core';
import { Button, Modal, Snackbar } from 'Components';
import { AppContext } from 'Context';
import { Pathname } from 'Routes';
import { dataFetcher, endpoints } from 'Api';
import { ReactComponent as AddIcon } from 'Assets/add-circle.svg';
import { SettingsPage } from '../components/page';
import { DataView } from '../components/data-view';
import styles from './index.module.css';

const defaultSnackbar = { isVisible: false, type: undefined, message: undefined };

const Box = withStyles({
  root: {
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
  }
})(MuiBox);
const TableHeaderCell = withStyles({
  root: {
    fontWeight: '600',
    borderBottom: '1px solid transparentize(#707070, 0.82)',
    padding: 0,
  },
})(MuiTableCell);
const TableBodyCell = withStyles({
  root: {
    fontWeight: '400',
    borderBottom: '1px solid transparentize(#707070, 0.82)',
    padding: 0,
  },
})(MuiTableCell);

const UserRequest = ({ id, firstname, setSnackbar }) => {
  const [action, setAction] = useState(undefined);
  const [isVisible, setVisibility] = useState(true);

  const { isValidating: handlingAction, mutate: handleAction } = useSWR([endpoints.updateViewRequest, id, action], {
    fetcher: (url, user, status) => dataFetcher(url, { user, status }),
    onSuccess: ({ success }) => {
      if (success) {
        setVisibility(false);
        setSnackbar({
          isVisible: true,
          message: `${firstname ? `${firstname}'s r` : 'R'}equest ${action === 1 ? 'accepted' : 'declined'}`,
        });
      } else {
        setSnackbar({ isVisible: true, message: 'Oops! Something went wrong', type: 'error' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, message: 'Oops! Something went wrong', type: 'error' });
    },
  });

  useEffect(() => { if (action) { handleAction(); } }, [handleAction, action]);

  return (
    <Collapse in={isVisible} timeout={250}>
      <div className={styles.userBoxRequest}>
        <div className={styles.userBoxRequestDescription}>
          <span className={styles.userBoxRequestUserName}>{firstname}</span>
          <> wants to view your content</>
        </div>
        <hr className={styles.userBoxDivider} />
        <div className={styles.userBoxRequestActions}>
          <Button
            isDisabled={handlingAction}
            className={cn({ [styles.userBoxRequestActionButton]: true, [styles.userBoxRequestAcceptButton]: true })}
            onClick={() => { setAction(1); }}
            isOutlined
            isBlue
          >
            {(handlingAction && (action === 1)) ? 'Accepting...' : 'Accept'}
          </Button>
          <Button
            isDisabled={handlingAction}
            className={styles.userBoxRequestActionButton}
            onClick={() => { setAction(2); }}
            isOutlined
            isBlack
          >
            {(handlingAction && (action === 2)) ? 'Declining...' : 'Decline'}
          </Button>
        </div>
      </div>
    </Collapse>
  );
};

UserRequest.defaultProps = {
  firstname: undefined,
};

UserRequest.propTypes = {
  id: pt.string.isRequired,
  firstname: pt.string,
  setSnackbar: pt.func.isRequired,
};

const AddBox = ({ alignRight, className, description, onAdd, title }) => (
  <div className={cn({ [className]: className, [styles.addBox]: true, [styles.alignRight]: alignRight })}>
    <div className={styles.addBoxTitle}>
      <div className={styles.addBoxTitleText}>{title}</div>
      <AddIcon className={styles.addBoxTitleIcon} onClick={onAdd} />
    </div>
    <div className={styles.addBoxDescription}>{description}</div>
  </div>
);

const GroupTableHead = () => (
  <TableHead>
    <TableRow>
      <TableHeaderCell className={styles.tableHeaderCell}>Groups Name</TableHeaderCell>
      <TableHeaderCell className={styles.tableHeaderCell} align="right">Viewers</TableHeaderCell>
    </TableRow>
  </TableHead>
);

const PersonButton = ({ data, isDisabled, isSelected, onClick }) => {
  const { email, firstname, lastname, image: dpURL } = data;
  const name = `${firstname} ${lastname}`;

  return (
    <button
      className={cn({ [styles.personButton]: true, [styles.selected]: isSelected })}
      disabled={isDisabled}
      onClick={onClick}
    >
      <div className={styles.personButtonDP}>
        {dpURL
          ? <img alt={`person - ${name}`} className={styles.personDPImage} src={dpURL} />
          : <div className={styles.personDPText}>{name.charAt(0)}</div>}
      </div>
      <div className={styles.personButtonTextContent}>
        <div className={styles.personButtonName}>{name}</div>
        <div className={styles.personButtonEmail}>{email}</div>
      </div>
    </button>
  );
};

PersonButton.defaultProps = {
  isDisabled: false,
  isSelected: false,
  onClick: undefined,
};

PersonButton.propTypes = {
  data: pt.object.isRequired,
  isDisabled: pt.bool,
  isSelected: pt.bool,
  onClick: pt.func,
};

export const ConnectedAccountSettings = () => {
  const navigate = useNavigate();
  const { userDetails } = useContext(AppContext);

  const [snackbar, setSnackbar] = useState(defaultSnackbar);
  const [userConnections, setUserConnections] = useState([]);

  const [userManagementModalIsVisible, setUserManagementModalVisibility] = useState(false);
  const [addViewerModalIsVisible, setAddViewerModalVisibility] = useState(false);
  const [addViewerModalSettings, updateAddViewerModalSettings] = useState({ title: '', description: '' });

  const [accessRequests, setAccessRequests] = useState(undefined);
  const [groups, setGroups] = useState(undefined);

  const { isValidating: gettingUserConnections, mutate: getUserConnections } = useSWR([
    endpoints.getAllViewRequestSent,
    1,
    'null',
  ], {
    fetcher: (url, status) => dataFetcher(url, { status }),
    onSuccess: ({ success, data }) => {
      if (success) {
        setUserConnections(data);
      } else {
        setUserConnections([]);
      }
    },
    onError: () => { setUserConnections([]); },
  });

  const { isValidating: loadingAccessRequests, mutate: fetchAccessRequests } = useSWR([endpoints.getAllViewRequest, 0], {
    fetcher: (url, status) => dataFetcher(url, { status }),
    onSuccess: ({ success, data }) => {
      if (success && data) {
        setAccessRequests(data?.filter(({ status }) => (status === '0')));
      } else {
        setAccessRequests([]);
      }
    },
    onError: () => {
      setAccessRequests([]);
    },
  });

  const { isValidating: loadingGroups, mutate: fetchGroups } = useSWR(endpoints.getMyGroups, {
    onSuccess: ({ success, data }) => {
      if (success && data) {
        setGroups(data);
      } else {
        setGroups([]);
      }
    },
    onError: () => {
      setGroups([]);
    },
  });

  useEffect(() => {
    if (userManagementModalIsVisible) {
      fetchAccessRequests();
      fetchGroups();
    } else {
      setAddViewerModalVisibility(false);
      setAccessRequests([]);
      setGroups(undefined);
    }
  }, [userManagementModalIsVisible, fetchAccessRequests, fetchGroups]);

  useEffect(() => {
    if (!addViewerModalIsVisible) {
      updateAddViewerModalSettings({ title: '', description: '' });
    }
  }, [addViewerModalIsVisible]);

  useEffect(() => {
    getUserConnections();
  }, [getUserConnections]);

  return (
    <SettingsPage
      title="Connected accounts"
      description="Edit All associated accounts settings"
    >
      <DataView
        data={[
          {
            labelTitle: 'Add viewer / accept user',
            labelDescription: 'Add users to enjoy your home videos',
            labelHasAddIcon: true,
            onLabelClick: () => { setUserManagementModalVisibility(true); },
            data: (
              <div className={styles.addViewerData}>
                {gettingUserConnections
                  ? 'Getting User Connections ...'
                  : userConnections?.map((data) => (
                    <PersonButton
                      data={{
                        email: data?.email,
                        firstname: data?.firstname,
                        lastname: data?.lastname,
                        image: data?.dpURL,
                      }}
                    />
                  ))}
              </div>
            ),
          },
          {
            labelTitle: 'Secession Planning',
            labelDescription: 'Secession Planning',
            labelHasAddIcon: true,
          },
        ]}
      />

      <Modal
        cancelButton="Cancel"
        cancelButtonClassName={styles.modalCancelButton}
        continueButton="Add"
        continueButtonClassName={styles.modalContinueButton}
        disableBackdropClick
        isVisible={userManagementModalIsVisible}
        noFooter={!addViewerModalIsVisible}
        onCancelButtonClick={() => { setAddViewerModalVisibility(false); }}
        onHide={() => { setUserManagementModalVisibility(false); }}
        title={addViewerModalIsVisible ? addViewerModalSettings.title : 'Add Viewer | Accept User'}
      >
        {!addViewerModalIsVisible
          ? (
            <>
              <div className={styles.acceptUserBox}>
                <Collapse timeout={400} in={loadingAccessRequests}>
                  <Zoom timeout={400} in={loadingAccessRequests}>
                    <Box className={styles.contentBox}>
                      <CircularProgress />
                    </Box>
                  </Zoom>
                </Collapse>
                <Collapse timeout={400} in={!loadingAccessRequests && !!accessRequests?.length}>
                  <Zoom timeout={400} in={!loadingAccessRequests && !!accessRequests?.length}>
                    <>
                      <div className={styles.acceptUserBoxTitle}>Accept User</div>
                      <div className={styles.acceptUserBoxDescription}>
                        All user requests will appear below, so you can decide to accept or decline
                      </div>
                      {accessRequests?.map(({ id, firstname }) => (
                        <UserRequest key={id} id={id} setSnackbar={setSnackbar} firstname={firstname} />
                      ))}
                    </>
                  </Zoom>
                </Collapse>
                <Collapse timeout={400} in={!loadingAccessRequests && !accessRequests?.length}>
                  <Zoom timeout={400} in={!loadingAccessRequests && !accessRequests?.length}>
                    <Box className={styles.contentBox}>No view requests</Box>
                  </Zoom>
                </Collapse>
              </div>
              <hr className={styles.modalDivider} />
              <div className={styles.addBoxContainer}>
                <AddBox
                  description="Add Multiple users to this group.
                  All users in this group Will have access to your uploaded content."
                  onAdd={() => { navigate(Pathname.createGroup); }}
                  title={`${userDetails?.firstname ? `${userDetails?.firstname}'s ` : ''}Groups`}
                />
                <AddBox
                  alignRight
                  className={styles.individualViewerAddBox}
                  description="Add Individual viewers to view your content."
                  title="Individual Viewers"
                />
                <div className={styles.tableContainer}>
                  <Collapse timeout={400} in={!loadingGroups}>
                    <Fade timeout={400} in={!loadingGroups}>
                      <Table className={styles.table}>
                        <GroupTableHead />
                        <TableBody>
                          {groups?.length
                            ? (
                              <>
                                {groups?.map(({ title, id, members_count }) => (
                                  <TableRow key={id}>
                                    <TableBodyCell className={styles.tableBodyCell}>{title}</TableBodyCell>
                                    <TableBodyCell className={styles.tableBodyCell} align="right">
                                      {members_count}
                                    </TableBodyCell>
                                  </TableRow>
                                ))}
                              </>
                            )
                            : (
                              <TableRow>
                                <TableBodyCell
                                  className={styles.tableBodyCell}
                                  align="center"
                                  colSpan="2"
                                >
                                  No new groups
                                </TableBodyCell>
                              </TableRow>
                            )}
                        </TableBody>
                      </Table>
                    </Fade>
                  </Collapse>
                  <Collapse timeout={400} in={loadingGroups}>
                    <Fade timeout={400} in={loadingGroups}>
                      <Table className={styles.table}>
                        <GroupTableHead />
                        <TableBody>
                          <TableRow>
                            <TableBodyCell align="center" className={styles.tableBodyCell} colSpan="2">
                              Loading...
                            </TableBodyCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </Fade>
                  </Collapse>
                </div>
                <div className={styles.addViewerBox}>
                  <div className={styles.addViewerBoxText}>No new Individual viewers have been added</div>
                  <Button
                    className={styles.addViewerBoxButton}
                    isGray
                    onClick={() => {
                      updateAddViewerModalSettings({
                        description: 'We will automatically add the viewer to your authorized list of viewers',
                        title: 'Add New Viewer',
                      });
                      setAddViewerModalVisibility(true);
                    }}
                  >
                    Add Viewer
                  </Button>
                </div>
              </div>
            </>
          )
          : (
            <>
              <div className={styles.modalDescription}>{addViewerModalSettings.description}</div>
              <div className={styles.viewersCountContainer}>
                <div className={styles.viewersCountTitle}>Viewers</div>
                <div className={styles.viewersCount}>0 of 999</div>
              </div>
              <div className={styles.viewerDetailsInputGrid}>
                <div className={styles.viewerDetailsIndex}>&nbsp;</div>
                <div className={styles.viewerDetailsColumnName}>Email</div>
                <div className={styles.viewerDetailsColumnName}>First Name</div>
                <div className={styles.viewerDetailsColumnName}>Last Name</div>
                <div className={styles.viewerDetailsIndex}>1</div>
                <div className={styles.viewerDetailsInputBox}>
                  <input className={styles.viewerDetailsInput} placeholder="Email" />
                </div>
                <div className={styles.viewerDetailsInputBox}>
                  <input className={styles.viewerDetailsInput} placeholder="First Name" />
                </div>
                <div className={styles.viewerDetailsInputBox}>
                  <input className={styles.viewerDetailsInput} placeholder="Last Name" />
                </div>
                <hr className={cn({ [styles.modalDivider]: true, [styles.viewerDetailsInputGridDivider]: true })} />
                <div className={styles.viewerDetailsIndex}>2</div>
                <div className={styles.viewerDetailsInputBox}>
                  <input className={styles.viewerDetailsInput} placeholder="Email" />
                </div>
                <div className={styles.viewerDetailsInputBox}>
                  <input className={styles.viewerDetailsInput} placeholder="First Name" />
                </div>
                <div className={styles.viewerDetailsInputBox}>
                  <input className={styles.viewerDetailsInput} placeholder="Last Name" />
                </div>
              </div>
            </>
          )}
      </Modal>

      <Snackbar
        {...snackbar}
        onClose={() => {
          setSnackbar({
            ...snackbar,
            message: '',
            isVisible: false
          });
        }}
      />
    </SettingsPage>
  );
};
