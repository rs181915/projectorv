import React, { useContext, useEffect, useState } from 'react';
import cn from 'classnames';
import useSWR from 'swr';
import { debounce } from 'lodash';
import { Collapse, Fade } from '@material-ui/core';
import { validate as isValidEmail } from 'email-validator';
import PasswordValidator from 'password-validator';
import { Modal, Snackbar } from 'Components';
import { AppContext } from 'Context';
import { AxiosCancelToken, dataFetcher, endpoints } from 'Api';
import { ReactComponent as AddPersonIcon } from 'Assets/add-person.svg';
import { SettingsPage } from '../components/page';
import { DataView } from '../components/data-view';
import { Input } from '../../../public/authentication/components/input-field';
import authStyles from '../../../public/authentication/auth.module.css';
import styles from './index.module.css';

const passwordSchema = (new PasswordValidator()).is().min(6).has()
  .symbols(1)
  .has()
  .digits(1);
const defaultSnackbar = { isVisible: false, type: undefined, message: undefined };
const transitionTimeout = 300;

const AccountData = ({ changeElement, data, onChange }) => (
  <div className={styles.accountDataContainer}>
    <div className={styles.accountData}>{data}</div>
    {changeElement || (
      <div
        className={styles.accountDataAction}
        onClick={onChange}
        onKeyPress={undefined}
        role="button"
        tabIndex={0}
      >
        Change
      </div>
    )}
  </div>
);

const EmailEditModal = ({ isVisible, onHide: onClose }) => {
  const { userDetails, setUserDetails } = useContext(AppContext);

  const [email, setEmail] = useState('');
  const [resetCode, setResetCode] = useState('');
  const [checkingEmailAvailability, setEmailAvailabilityCheckingStatus] = useState(false);
  const [emailIsAvailable, setEmailAvailability] = useState(false);
  const [resetCodeSent, setResetCodeStatus] = useState(false);
  const [cancelToken, setCancelToken] = useState(undefined);

  const [snackbar, setSnackbar] = useState(defaultSnackbar);

  const { isValidating: sendingResetCode, mutate: sendResetCode } = useSWR([endpoints.changeEmailRequest], {
    onSuccess: ({ success }) => {
      if (success) {
        setResetCodeStatus(true);
      } else {
        setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while sending Reset Code' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while sending Reset Code' });
    },
  });

  const { mutate: checkEmailAvailability } = useSWR([endpoints.signupEmail, email, cancelToken?.token], {
    fetcher: (url, inputEmail, uniqueCancelToken) => dataFetcher(url, { email: inputEmail }, uniqueCancelToken),
    onSuccess: ({ success }) => {
      setEmailAvailabilityCheckingStatus(false);

      if (success) {
        setEmailAvailability(true);
      } else {
        setEmailAvailability(false);
        setSnackbar({ isVisible: true, type: 'error', message: 'This email is already taken' });
      }
    },
    onError: () => {
      setEmailAvailabilityCheckingStatus(false);
    },
  });

  const onHide = () => {
    setEmail('');
    setResetCode('');
    setEmailAvailability(false);
    setResetCodeStatus(false);
    setEmailAvailabilityCheckingStatus(false);
    setSnackbar(defaultSnackbar);
    onClose();
  };

  const { isValidating: updatingEmail, mutate: updateEmail } = useSWR([endpoints.updateEmailAddress, email, resetCode], {
    fetcher: (url, inputEmail, inputResetCode) => dataFetcher(url, { reset_code: inputResetCode, email: inputEmail }),
    onSuccess: ({ success }) => {
      if (success) {
        setSnackbar({ isVisible: true, type: 'success', message: 'Email was updated successfully' });
        setTimeout(onHide, 1500);
        setUserDetails({ ...userDetails, email });
      } else {
        setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating email' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating email' });
    },
  });

  const emailInputHandler = debounce(({ target: { value: inputEmail } }) => {
    if (isValidEmail(inputEmail)) {
      setEmail(inputEmail);
    } else {
      setEmail('');
      setEmailAvailability(false);
    }
  }, 750, { trailing: true });

  useEffect(() => {
    if (email) {
      setEmailAvailabilityCheckingStatus(false);
      checkEmailAvailability();
    }
  }, [email, setEmailAvailabilityCheckingStatus, checkEmailAvailability]);

  useEffect(() => { if (isVisible) { sendResetCode(); } }, [isVisible, sendResetCode]);

  return (
    <>
      <Modal
        isVisible={isVisible}
        title="Update Email"
        onHide={onHide}
        cancelButton="Cancel"
        cancelButtonClassName={styles.actionButton}
        onCancelButtonClick={onHide}
        continueButton={updatingEmail ? 'Updating...' : 'Update'}
        continueButtonClassName={styles.actionButton}
        onContinueButtonClick={updateEmail}
        continueButtonProps={{
          isDisabled: checkingEmailAvailability
            || resetCode?.length !== 5 || updatingEmail || sendingResetCode || !emailIsAvailable
        }}
      >
        <br />

        <Collapse in={resetCodeSent} timeout={transitionTimeout}>
          <Fade in={resetCodeSent} timeout={transitionTimeout}>
            <>
              <Input
                auto-complete="one-time-code"
                className={styles.inputField}
                containerClassName={styles.inputFieldContainer}
                labelClassName={styles.inputFieldLabel}
                label="Enter reset code"
                onInput={(e) => {
                  const { target: { value: inputResetCode } } = e;

                  setResetCode(inputResetCode);
                }}
                type="code"
              />
              <br />
              <Input
                className={styles.inputField}
                containerClassName={styles.inputFieldContainer}
                isLoading={checkingEmailAvailability}
                labelClassName={styles.inputFieldLabel}
                label="Enter new Email"
                placeholder="example@projector.com"
                onChange={(e) => {
                  e.persist();
                  emailInputHandler(e);
                  setEmailAvailabilityCheckingStatus(false);

                  if (cancelToken) { cancelToken.cancel(); }

                  if (!e?.target?.value) {
                    setEmail('');
                    setEmailAvailability(false);
                  } else {
                    setCancelToken(AxiosCancelToken.source());
                  }
                }}
                type="email"
              />
            </>
          </Fade>
        </Collapse>

        <Collapse in={sendingResetCode} timeout={transitionTimeout}>
          <Fade in={sendingResetCode} timeout={transitionTimeout}>
            <div>Sending Reset Code</div>
          </Fade>
        </Collapse>
      </Modal>

      <Snackbar
        isVisible={snackbar?.isVisible}
        message={snackbar?.message || ''}
        onClose={() => { setSnackbar(defaultSnackbar); }}
        type={snackbar?.type}
      />
    </>
  );
};

const PhoneNumberEditModal = ({ isVisible, onHide: onClose }) => {
  const { userDetails, setUserDetails } = useContext(AppContext);

  const [mobile, setMobile] = useState('');

  const [snackbar, setSnackbar] = useState(defaultSnackbar);

  const onHide = () => {
    setMobile('');
    setSnackbar(defaultSnackbar);
    onClose();
  };

  const { isValidating: updatingMobile, mutate: updateMobile } = useSWR([endpoints.updateMyProfile, mobile], {
    fetcher: (url, inputMobile) => dataFetcher(url, { mobile: inputMobile }),
    onSuccess: ({ success }) => {
      if (success) {
        setSnackbar({ isVisible: true, type: 'success', message: 'Phone number was updated successfully' });
        setTimeout(onHide, 1500);
        setUserDetails({ ...userDetails, mobile });
      } else {
        setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating phone number' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating phone number' });
    },
  });

  return (
    <>
      <Modal
        isVisible={isVisible}
        title="Update Phone number"
        onHide={onHide}
        cancelButton="Cancel"
        cancelButtonClassName={styles.actionButton}
        onCancelButtonClick={onHide}
        continueButton={updatingMobile ? 'Updating...' : 'Update'}
        continueButtonClassName={styles.actionButton}
        onContinueButtonClick={updateMobile}
        continueButtonProps={{ isDisabled: (mobile?.length < 1) || updatingMobile }}
      >
        <br />
        <Input
          className={styles.inputField}
          containerClassName={styles.inputFieldContainer}
          labelClassName={styles.inputFieldLabel}
          label="Enter Phone number"
          onInput={(e) => {
            const { target: { value: inputMobile } } = e;

            setMobile(inputMobile);
          }}
          type="code"
        />
      </Modal>

      <Snackbar
        isVisible={snackbar?.isVisible}
        message={snackbar?.message || ''}
        onClose={() => { setSnackbar(defaultSnackbar); }}
        type={snackbar?.type}
      />
    </>
  );
};

const PasswordEditModal = ({ isVisible, onHide: onClose }) => {
  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');

  const [snackbar, setSnackbar] = useState(defaultSnackbar);

  const passwordHealth = (
    newPassword
      ? passwordSchema.validate(newPassword)
        ? newPassword.length > 10
          ? 'strong'
          : 'medium'
        : 'weak'
      : ''
  );

  const onHide = () => {
    setCurrentPassword('');
    setNewPassword('');
    setSnackbar(defaultSnackbar);
    onClose();
  };

  const {
    isValidating: updatingPassword,
    mutate: updatePassword,
  } = useSWR([endpoints.updatePassword, currentPassword, newPassword], {
    fetcher: (url, inputCurrentPassword, inputNewPassword) => dataFetcher(url, {
      old_password: inputCurrentPassword,
      new_password: inputNewPassword,
    }),
    onSuccess: ({ success }) => {
      if (success) {
        setSnackbar({ isVisible: true, type: 'success', message: 'Password was updated successfully' });
        setTimeout(onHide, 1500);
      } else {
        setSnackbar({ isVisible: true, type: 'error', message: 'Incorrect current password' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  return (
    <>
      <Modal
        isVisible={isVisible}
        cancelButton="Cancel"
        cancelButtonClassName={styles.actionButton}
        continueButton={updatingPassword ? 'Updating...' : 'Update'}
        continueButtonClassName={styles.actionButton}
        continueButtonProps={{
          isDisabled: (!((passwordHealth === 'strong') || (passwordHealth === 'medium'))) || updatingPassword
        }}
        onCancelButtonClick={onHide}
        onContinueButtonClick={updatePassword}
        onHide={onHide}
        title="Update Password"
      >
        <br />
        <Input
          className={styles.inputField}
          containerClassName={styles.inputFieldContainer}
          label="Enter current password"
          labelClassName={styles.inputFieldLabel}
          maxLength={16}
          minLength={6}
          onInput={(e) => {
            const { target: { value: inputCurrentPassword } } = e;

            setCurrentPassword(inputCurrentPassword);
          }}
          type="password"
        />
        <br />
        <Input
          className={styles.inputField}
          containerClassName={styles.inputFieldContainer}
          label="Enter new password"
          labelClassName={styles.inputFieldLabel}
          maxLength={16}
          minLength={6}
          onInput={(e) => {
            const { target: { value: inputNewPassword } } = e;

            setNewPassword(inputNewPassword);
          }}
          type="password"
        />
        <div className={cn({ [authStyles.passwordStrengthBar]: true, [styles.passwordStrengthBar]: true })}>
          <div
            className={cn({
              [authStyles.passwordStrengthBarContent]: true,
              ...passwordHealth && { [authStyles[passwordHealth]]: true }
            })}
            style={{
              width: passwordHealth === 'weak'
                ? '30%'
                : passwordHealth === 'medium'
                  ? '55%'
                  : passwordHealth === 'strong'
                    ? '100%'
                    : '0%'
            }}
          />
        </div>
        <div className={cn({ [authStyles.passwordStrengthNotes]: true, [styles.passwordStrengthNotes]: true })}>
          <div>Use a minimum of 6 characters (case sensitive) with at lease one number and one special character.</div>
          <div
            className={cn({
              [authStyles.currentPasswordStrength]: true,
              ...passwordHealth && { [authStyles[passwordHealth]]: true },
            })}
            onKeyPress={undefined}
            role="button"
            tabIndex={0}
          >
            {passwordHealth}
          </div>
        </div>
      </Modal>

      <Snackbar
        isVisible={snackbar?.isVisible}
        message={snackbar?.message || ''}
        onClose={() => { setSnackbar(defaultSnackbar); }}
        type={snackbar?.type}
      />
    </>
  );
};

const NameEditModal = ({ isVisible, onHide: onClose }) => {
  const { userDetails, setUserDetails } = useContext(AppContext);

  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');

  const [snackbar, setSnackbar] = useState(defaultSnackbar);

  const onHide = () => {
    setFirstname('');
    setLastname('');
    setSnackbar(defaultSnackbar);
    onClose();
  };

  const { isValidating: updatingName, mutate: updateName } = useSWR([endpoints.updateMyProfile, firstname, lastname], {
    fetcher: (url, inputFirstname, inputLastname) => dataFetcher(url, { firstname: inputFirstname, lastname: inputLastname }),
    onSuccess: ({ success }) => {
      if (success) {
        setSnackbar({ isVisible: true, type: 'success', message: 'Name was updated successfully' });
        setUserDetails({ ...userDetails, firstname, lastname });
        setTimeout(onHide, 1500);
      } else {
        setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating name' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating name' });
    },
  });

  return (
    <>
      <Modal
        isVisible={isVisible}
        title="Update Name"
        onHide={onHide}
        cancelButton="Cancel"
        cancelButtonClassName={styles.actionButton}
        onCancelButtonClick={onHide}
        continueButton={updatingName ? 'Updating...' : 'Update'}
        continueButtonClassName={styles.actionButton}
        onContinueButtonClick={updateName}
        continueButtonProps={{ isDisabled: !(firstname || lastname) || updatingName }}
      >
        <br />
        <Input
          className={styles.inputField}
          containerClassName={styles.inputFieldContainer}
          label="Enter first name"
          labelClassName={styles.inputFieldLabel}
          onInput={(e) => {
            const { target: { value: inputFirstname } } = e;

            setFirstname(inputFirstname);
          }}
          type="code"
        />
        <br />
        <Input
          className={styles.inputField}
          containerClassName={styles.inputFieldContainer}
          label="Enter last name"
          labelClassName={styles.inputFieldLabel}
          onInput={(e) => {
            const { target: { value: inputLastname } } = e;

            setLastname(inputLastname);
          }}
          type="code"
        />
      </Modal>

      <Snackbar
        isVisible={snackbar?.isVisible}
        message={snackbar?.message || ''}
        onClose={() => { setSnackbar(defaultSnackbar); }}
        type={snackbar?.type}
      />
    </>
  );
};

export const AccountSettings = () => {
  const { userDetails, setUserDetails } = useContext(AppContext);

  const [avatar, setAvatar] = useState(undefined);
  const [avatarUploadError, setAvatarUploadErrorStatus] = useState(false);
  const [nameEditModalIsVisible, setNameEditModalVisibility] = useState(false);
  const [emailEditModalIsVisible, setEmailEditModalVisibility] = useState(false);
  const [phoneNumberEditModalIsVisible, setPhoneNumberEditModalVisibility] = useState(false);
  const [passwordEditModalIsVisible, setPasswordEditModalVisibility] = useState(false);

  const [snackbar, setSnackbar] = useState(defaultSnackbar);

  const revealNameEditModal = () => { setNameEditModalVisibility(true); };
  const hideNameEditModal = () => { setNameEditModalVisibility(false); };
  const revealEmailEditModal = () => { setEmailEditModalVisibility(true); };
  const hideEmailEditModal = () => { setEmailEditModalVisibility(false); };
  const revealPhoneNumberEditModal = () => { setPhoneNumberEditModalVisibility(true); };
  const hidePhoneNumberEditModal = () => { setPhoneNumberEditModalVisibility(false); };
  const revealPasswordEditModal = () => { setPasswordEditModalVisibility(true); };
  const hidePasswordEditModal = () => { setPasswordEditModalVisibility(false); };

  const { isValidating: updatingAvatar, mutate: updateAvatar } = useSWR([endpoints.updateProfileImge, avatar], {
    fetcher: (url, inputAvatar) => dataFetcher(url, { image: inputAvatar }),
    onSuccess: ({ success, image }) => {
      if (success) {
        setAvatarUploadErrorStatus(false);
        setSnackbar({ isVisible: true, type: 'success', message: 'Avatar was updated successfully' });
        setUserDetails({ ...userDetails, dpURL: image });
      } else {
        setAvatarUploadErrorStatus(true);
        setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating your avatar' });
      }
    },
    onError: () => {
      setAvatarUploadErrorStatus(true);
      setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while updating your avatar' });
    },
  });

  useEffect(() => { if (avatar) { updateAvatar(); } }, [avatar, updateAvatar]);

  return (
    <>
      <SettingsPage
        title="Account"
        description="Choose how your friends see you on Projector"
      >
        <DataView
          data={[
            {
              labelTitle: 'Profile',
              data: (
                <>
                  <AccountData
                    data={(
                      <>
                        <input
                          accept="image/*"
                          disabled={updatingAvatar}
                          className={styles.hiddenFormField}
                          id="account-profile-pic"
                          onChange={({ target: { files } }) => {
                            const reader = new FileReader();
                            const tempAvatar = files[0];

                            if (tempAvatar) { reader.readAsDataURL(tempAvatar); }

                            reader.addEventListener('load', () => { setAvatar(reader.result); }, false);
                          }}
                          type="file"
                        />
                        {userDetails?.dpURL
                          ? <img alt="profile" className={styles.profilePicture} src={userDetails?.dpURL || ''} />
                          : (
                            <div className={styles.profilePicture}>
                              <AddPersonIcon className={styles.profilePictureIcon} />
                            </div>
                          )}
                      </>
                    )}
                    changeElement={avatarUploadError
                      ? <div className={styles.avatarEditButton}>Retry Upload</div>
                      : (
                        <label
                          className={styles.avatarEditButton}
                          htmlFor="account-profile-pic"
                        >
                          {updatingAvatar ? 'Uploading Avatar' : 'Edit Avatar'}
                        </label>
                      )}
                    onChange={revealNameEditModal}
                  />
                  <br />
                  <AccountData data={`${userDetails?.firstname} ${userDetails?.lastname}`} onChange={revealNameEditModal} />
                  <br />
                </>
              ),
            },
            {
              labelTitle: 'Email',
              data: (
                <AccountData data={userDetails?.email} onChange={revealEmailEditModal} />
              ),
            },
            {
              labelTitle: 'Phone number',
              data: (
                <AccountData
                  data={userDetails?.mobile || (
                    <> -&nbsp;&nbsp;&nbsp;-&nbsp;-&nbsp;-&nbsp;&nbsp;&nbsp;-&nbsp;-&nbsp;-&nbsp;&nbsp;&nbsp;-&nbsp;-&nbsp;-</>
                  )}
                  onChange={revealPhoneNumberEditModal}
                />
              ),
            },
            {
              labelTitle: 'Password',
              data: (
                <AccountData data="***************" onChange={revealPasswordEditModal} />
              ),
            },
          ]}
        />
      </SettingsPage>

      <NameEditModal isVisible={nameEditModalIsVisible} onHide={hideNameEditModal} />
      <EmailEditModal isVisible={emailEditModalIsVisible} onHide={hideEmailEditModal} />
      <PhoneNumberEditModal isVisible={phoneNumberEditModalIsVisible} onHide={hidePhoneNumberEditModal} />
      <PasswordEditModal isVisible={passwordEditModalIsVisible} onHide={hidePasswordEditModal} />

      <Snackbar
        isVisible={snackbar?.isVisible}
        message={snackbar?.message || ''}
        onClose={() => { setSnackbar(defaultSnackbar); }}
        type={snackbar?.type}
      />
    </>
  );
};
