import React from 'react';
import { SettingsPage } from '../components/page';

export const PrivacySettings = () => (
  <SettingsPage
    title="Privacy"
    description="Edit Projector Privacy settings"
  />
);
