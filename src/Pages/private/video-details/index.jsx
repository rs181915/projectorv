import React, { useContext, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import useSWR from 'swr';
import moment from 'moment';
import { CircularProgress as MuiCircularProgress, withStyles } from '@material-ui/core';
import { dataFetcher, endpoints } from 'Api';
import { ProfilePage } from 'Components';
import { Pathname } from 'Routes';
import { AppContext } from 'Context';
import { ReactComponent as BackIcon } from 'Assets/chevron-left.svg';
import { ReactComponent as PlayIcon } from 'Assets/play.svg';
import styles from './index.module.css';

const CircularProgress = withStyles({
  circle: {
    color: '#fff !important'
  }
})(MuiCircularProgress);

export const VideoDetails = () => {
  const { setAppSnackbar } = useContext(AppContext);
  const navigate = useNavigate();
  const { videoID } = useParams();

  const [videoDetails, setVideoDetails] = useState({});
  const [loading, setLoading] = useState(true);
  const [isVideoInWatchlist, setVideoInWatchlist] = useState(false);
  const [updatingInWatchlist, setWatchlistUpdationStatus] = useState(false);

  const { mutate: updateInWatchlist } = useSWR([endpoints.updateWatchList, videoID, isVideoInWatchlist], {
    fetcher: (url, id, shouldDelete) => dataFetcher(url, {
      video_id: id,
      action: shouldDelete ? 'delete' : 'add',
    }),
    onSuccess: ({ success }) => {
      if (success) {
        setVideoInWatchlist(!isVideoInWatchlist);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }

      setWatchlistUpdationStatus(false);
    },
    onError: () => {
      setWatchlistUpdationStatus(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { mutate: getVideoDetails } = useSWR([endpoints.getVideosDetail, videoID], {
    fetcher: (url, id) => dataFetcher(url, { id }),
    onSuccess: ({ success, data }) => {
      if (success) {
        setVideoDetails(data);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }

      setLoading(false);
    },
    onError: () => {
      setLoading(false);
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  useEffect(() => {
    getVideoDetails();
  }, []);

  return (
    <>
      {!!(videoDetails?.thumbnails?.length) && (
        <>
          <div
            className={styles.bgLayer}
            style={{ backgroundImage: `url(${videoDetails?.thumbnails[0]})` }}
          />
          <div className={styles.bgDarkLayer} />
        </>
      )}
      <div className={styles.bgDarkLayer} />
      <ProfilePage
        className={styles.page}
        containerClassName={styles.pageContainer}
        mainClassName={styles.main}
        noHeader
        noDefaultBg
        isProfileHeader
      >
        {loading
          ? <CircularProgress />
          : (
            <div className={styles.videoDetailsContainer}>
              <div className={styles.header}>
                <BackIcon className={styles.videoBackButton} onClick={() => { navigate(-1); }} />
              </div>
              <h1 className={styles.videoTitle}>{videoDetails?.title}</h1>
              <div className={styles.videoDetails}>
                <div className={styles.videoActions}>
                  <Link to={`${Pathname.getPlayerPath(videoID)}`} className={styles.videoPlayButton}>
                    <PlayIcon className={styles.videoPlayIcon} />
                    <>Play</>
                  </Link>
                  <button
                    className={styles.videoAddButton}
                    disabled={updatingInWatchlist}
                    onClick={updateInWatchlist}
                  >
                    {isVideoInWatchlist ? '✓' : '+'}
                  </button>
                </div>
                <div className={styles.videoInfo}>
                  {videoDetails?.publish_date && (
                    <>
                      {moment(videoDetails?.publish_date).format('YYYY')}
                      <div className={styles.videoInfoSeparator} />
                    </>
                  )}
                  <>{videoDetails?.Category}</>
                  <div className={styles.videoInfoSeparator} />
                  <>{videoDetails?.SubCategory}</>
                </div>
                <p className={styles.videoDescription}>{videoDetails?.description}</p>
              </div>
            </div>
          )}
      </ProfilePage>
    </>
  );
};
