import React, { useState, useContext, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { debounce } from 'lodash';
import { Collapse, Fade } from '@material-ui/core';
import { validate as isValidEmail } from 'email-validator';
import { Pathname } from 'Routes';
import { Button } from 'Components';
import { AppContext, AuthContext } from 'Context';
import { AxiosCancelToken, dataFetcher, endpoints } from 'Api';
import { Input } from '../components/input-field';
import { AuthPage } from '../components/page';
import { StepStatus } from '../components/step-status';
import styles from './index.module.css';
import authStyles from '../auth.module.css';

const transitionTimeout = 300;

export const SignIn = () => {
  const navigate = useNavigate();
  const { setAppSnackbar, userDetails, setUserDetails } = useContext(AppContext);
  const { forceAutoSignin, setToken, preferredEmail, setPreferredEmail } = useContext(AuthContext);

  const [email, setEmail] = useState(isValidEmail(preferredEmail) ? preferredEmail : '');
  const [password, setPassword] = useState('');
  const [emailIsFixed, fixEmail] = useState(false);
  const [emailIsValid, setEmailValidity] = useState(false);
  const [accountExists, setAccountExistence] = useState(undefined);
  const [checkingAccountExistence, setAccountExistenceCheckingStatus] = useState(false);
  const [cancelToken, setCancelToken] = useState(undefined);

  const { isValidating: signingInUser, mutate: signInUser } = useSWR([endpoints.signin, email, password], {
    fetcher: (url, inputEmail, inputPassword) => dataFetcher(url, { email: inputEmail, password: inputPassword }),
    onSuccess: ({ isNewUser, success, data }) => {
      if (success) {
        setUserDetails({ ...userDetails, isNewUser });
        setToken(data?.token);
        setAppSnackbar({ message: 'Logged in successfully', isVisible: true });
        forceAutoSignin();
      } else {
        setAppSnackbar({ type: 'error', message: 'Incorrect password', isVisible: true });
      }
    },
    onError: () => {
      setAppSnackbar({ type: 'error', message: 'Oops! Something went wrong', isVisible: true });
    },
  });

  const { mutate: checkAccountExistence } = useSWR([endpoints.signinEmail, email, cancelToken?.token], {
    fetcher: (url, inputEmail, uniqueCancelToken) => dataFetcher(url, { email: inputEmail }, uniqueCancelToken),
    onSuccess: ({ success }) => {
      setAccountExistenceCheckingStatus(false);

      if (success) {
        setAccountExistence(false);

        if (isValidEmail(preferredEmail)) { fixEmail(true); }
      } else {
        setAccountExistence(true);
      }

      setPreferredEmail(undefined);
    },
    onError: () => {
      setAccountExistenceCheckingStatus(false);
    },
  });

  const emailInputHandler = debounce(({ target: { value: inputEmail } }) => {
    if (isValidEmail(inputEmail)) {
      setEmail(inputEmail);
      setEmailValidity(true);
    } else {
      setEmail('');
      setAccountExistence(undefined);
      setEmailValidity(false);
    }
  }, 750, { trailing: true });

  useEffect(() => {
    if (email) {
      setAccountExistenceCheckingStatus(true);
      checkAccountExistence();
    }
  }, [email, setAccountExistenceCheckingStatus, checkAccountExistence]);

  useEffect(() => {
    if (preferredEmail && isValidEmail(preferredEmail)) {
      setAccountExistenceCheckingStatus(true);
      setEmailValidity(true);
      checkAccountExistence();
    }
  }, [preferredEmail, checkAccountExistence]);

  return (
    <AuthPage>
      {!emailIsFixed
        ? (
          <form onSubmit={(e) => { e.preventDefault(); }}>
            <StepStatus currentStep="1" totalSteps="2" />
            <Input
              defaultValue={email}
              isDisabled={(email === preferredEmail) && checkingAccountExistence}
              isLoading={checkingAccountExistence}
              label="Login with your email"
              onInput={(e) => {
                e.persist();
                emailInputHandler(e);
                setAccountExistenceCheckingStatus(false);

                if (cancelToken) { cancelToken.cancel(); }

                if (!e?.target?.value) {
                  setEmail('');
                  setAccountExistence(undefined);
                  setEmailValidity(false);
                } else {
                  setCancelToken(AxiosCancelToken.source());
                }
              }}
              placeholder="Email"
              type="email"
            />
            <input
              className={authStyles.hiddenFormField}
              name="password"
              onChange={() => { }}
              type="password"
              value={password}
            />
            <br />
            <Collapse
              in={(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
              timeout={transitionTimeout}
            >
              <Fade
                in={(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
                timeout={transitionTimeout}
              >
                <>
                  <div className={authStyles.recommendationText}>New to Projector?</div>
                  <br />
                </>
              </Fade>
            </Collapse>
            {accountExists === true
              ? (
                <Button
                  blockText
                  isBlue
                  isDisabled={checkingAccountExistence
                    || !(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
                  isFullWidth
                  isLarge
                  onClick={() => { setPreferredEmail(email); navigate(Pathname.authentication.signUp); }}
                >
                  Sign Up
                </Button>
              )
              : (
                <Button
                  blockText
                  isBlue
                  isDisabled={checkingAccountExistence
                    || !(emailIsValid && (accountExists !== undefined) && (accountExists === false))}
                  isFullWidth
                  submit
                  isLarge
                  onClick={() => { fixEmail(true); }}
                >
                  Continue
                </Button>
              )}
            <Collapse
              in={!(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
              timeout={transitionTimeout}
            >
              <Fade
                in={!(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
                timeout={transitionTimeout}
              >
                <div className={authStyles.suggestionText}>
                  <>New to Projector?&nbsp; </>
                  <Link className={authStyles.suggestionLink} to={Pathname.authentication.signUp}>Sign&nbsp;Up</Link>
                </div>
              </Fade>
            </Collapse>
          </form>
        )
        : (
          <form onSubmit={(e) => { e.preventDefault(); signInUser(); }}>
            <StepStatus currentStep="2" totalSteps="2" />
            <input className={authStyles.hiddenFormField} name="email" onChange={() => { }} type="email" value={email} />
            <Input
              label="Enter your password"
              maxLength={16}
              message="( Case Sensitive )"
              minLength={6}
              onInput={({ target: { value: inputPassword } }) => { setPassword(inputPassword); }}
              placeholder="Password"
              type="password"
            />
            <br />
            <div>
              <Button blockText isBlue isDisabled={signingInUser} isFullWidth isLarge submit>
                {signingInUser ? 'Signing In...' : 'Continue'}
              </Button>
            </div>
            <div className={authStyles.suggestionText}>
              <Link
                className={cn({ [authStyles.suggestionLink]: true, [styles.forgotPasswordLink]: true })}
                to={Pathname.authentication.forgotPassword}
              >
                Forgot&nbsp;Password?
              </Link>
            </div>
          </form>
        )}
    </AuthPage>
  );
};
