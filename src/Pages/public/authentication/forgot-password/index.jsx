import React, { useState, useContext, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { debounce } from 'lodash';
import { Collapse, Fade } from '@material-ui/core';
import { validate as isValidEmail } from 'email-validator';
import PasswordValidator from 'password-validator';
import { Pathname } from 'Routes';
import { Button, Snackbar } from 'Components';
import { AuthContext } from 'Context';
import { AxiosCancelToken, dataFetcher, endpoints } from 'Api';
import { Input } from '../components/input-field';
import { AuthPage } from '../components/page';
import { StepStatus } from '../components/step-status';
import styles from './index.module.css';
import authStyles from '../auth.module.css';

const passwordSchema = (new PasswordValidator()).is().min(6).has()
  .symbols(1)
  .has()
  .digits(1);
const transitionTimeout = 300;
const defaultSnackbar = { isVisible: false, type: undefined, message: undefined };

export const ForgotPassword = () => {
  const navigate = useNavigate();
  const { preferredEmail, setPreferredEmail } = useContext(AuthContext);

  const [email, setEmail] = useState(isValidEmail(preferredEmail) ? preferredEmail : '');
  const [resetCode, setResetCode] = useState('');
  const [password, setPassword] = useState('');
  const [resetCodeIsSent, setResetCodeStatus] = useState(false);
  const [passwordIsReset, setPasswordResetStatus] = useState(false);
  const [emailIsFixed, fixEmail] = useState(false);
  const [emailIsValid, setEmailValidity] = useState(false);
  const [accountExists, setAccountExistence] = useState(undefined);
  const [checkingAccountExistence, setAccountExistenceCheckingStatus] = useState(false);
  const [cancelToken, setCancelToken] = useState(undefined);

  const [snackbar, setSnackbar] = useState(defaultSnackbar);

  const passwordHealth = (
    password
      ? passwordSchema.validate(password)
        ? password.length > 10
          ? 'strong'
          : 'medium'
        : 'weak'
      : ''
  );

  const { isValidating: sendingResetCode, mutate: sendResetCode } = useSWR([endpoints.forgotPasswordEmail, email], {
    fetcher: (url, inputEmail) => dataFetcher(url, { email: inputEmail }),
    onSuccess: ({ success }) => {
      if (success) {
        setResetCodeStatus(true);
        fixEmail(true);
      } else {
        setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while sending Reset Code' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while sending Reset Code' });
    },
  });

  const {
    isValidating: resettingPassword,
    mutate: resetPassword,
  } = useSWR([endpoints.updatePasswordWithCode, email, password, resetCode], {
    fetcher: (url, inputEmail, inputPassword, inputResetCode) => dataFetcher(url, {
      email: inputEmail,
      password: inputPassword,
      reset_code: inputResetCode,
    }),
    onSuccess: ({ success, message }) => {
      if (success) {
        setPasswordResetStatus(true);
      } else {
        setSnackbar({ isVisible: true, message, type: 'error' });
      }
    },
    onError: () => {
      setSnackbar({ isVisible: true, message: 'Oops! Something went wrong', type: 'error' });
    },
  });

  const { mutate: checkAccountExistence } = useSWR([endpoints.signinEmail, email, cancelToken?.token], {
    fetcher: (url, inputEmail, uniqueCancelToken) => dataFetcher(url, { email: inputEmail }, uniqueCancelToken),
    onSuccess: ({ success }) => {
      setAccountExistenceCheckingStatus(false);

      if (success) {
        setAccountExistence(false);

        if (isValidEmail(preferredEmail)) { fixEmail(true); }
      } else {
        setAccountExistence(true);
      }

      setPreferredEmail(undefined);
    },
    onError: () => {
      setAccountExistenceCheckingStatus(false);
    },
  });

  const emailInputHandler = debounce(({ target: { value: inputEmail } }) => {
    if (isValidEmail(inputEmail)) {
      setEmail(inputEmail);
      setEmailValidity(true);
    } else {
      setEmail('');
      setAccountExistence(undefined);
      setEmailValidity(false);
    }
  }, 750, { trailing: true });

  useEffect(() => {
    if (email) {
      setAccountExistenceCheckingStatus(true);
      checkAccountExistence();
    }
  }, [email, setAccountExistenceCheckingStatus, checkAccountExistence]);

  useEffect(() => {
    if (preferredEmail && isValidEmail(preferredEmail)) {
      setAccountExistenceCheckingStatus(true);
      setEmailValidity(true);
      checkAccountExistence();
    }
  }, [preferredEmail, setAccountExistenceCheckingStatus, checkAccountExistence]);

  return (
    <AuthPage>
      {!emailIsFixed
        ? (
          <>
            <StepStatus currentStep="1" totalSteps="2" />
            <Input
              defaultValue={email}
              isDisabled={(email === preferredEmail) && checkingAccountExistence}
              isLoading={checkingAccountExistence}
              label="Enter your email"
              onChange={(e) => {
                e.persist();
                emailInputHandler(e);
                setAccountExistenceCheckingStatus(false);

                if (cancelToken) { cancelToken.cancel(); }

                if (!e?.target?.value) {
                  setEmail('');
                  setAccountExistence(undefined);
                  setEmailValidity(false);
                } else {
                  setCancelToken(AxiosCancelToken.source());
                }
              }}
              placeholder="Email"
              type="email"
            />
            <br />
            <Collapse
              in={(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
              timeout={transitionTimeout}
            >
              <Fade
                in={(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
                timeout={transitionTimeout}
              >
                <>
                  <div className={authStyles.recommendationText}>
                    <>We couldn&apos;t find your account.</>
                    <br />
                    <>New to Projector?</>
                  </div>
                  <br />
                </>
              </Fade>
            </Collapse>
            {accountExists === true
              ? (
                <Button
                  blockText
                  isBlue
                  isDisabled={checkingAccountExistence
                    || !(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
                  isFullWidth
                  isLarge
                  onClick={() => { setPreferredEmail(email); navigate(Pathname.authentication.signUp); }}
                >
                  Sign Up
                </Button>
              )
              : (
                <Button
                  blockText
                  isBlue
                  isDisabled={checkingAccountExistence
                    || sendingResetCode || !(emailIsValid && (accountExists !== undefined) && (accountExists === false))}
                  isFullWidth
                  isLarge
                  onClick={sendResetCode}
                >
                  {sendingResetCode ? 'Sending Reset Code...' : 'Continue'}
                </Button>
              )}
            <Collapse
              in={!(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
              timeout={transitionTimeout}
            >
              <Fade
                in={!(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
                timeout={transitionTimeout}
              >
                <div className={authStyles.suggestionText}>
                  <Link
                    className={cn({ [authStyles.suggestionLink]: true, [styles.signInLink]: true })}
                    to={Pathname.authentication.signIn}
                  >
                    Remember&nbsp;Password?
                  </Link>
                </div>
              </Fade>
            </Collapse>
          </>
        )
        : (!passwordIsReset && resetCodeIsSent
          ? (
            <>
              <StepStatus currentStep="2" totalSteps="2" />
              <Input
                auto-complete="one-time-code"
                label="Enter reset code"
                maxLength="5"
                message="Enter the Reset Code received in your Email"
                minLength="5"
                onInput={({ target: { value: inputResetCode } }) => { setResetCode(inputResetCode); }}
                placeholder="Reset Code"
                type="code"
              />
              <br />
              <br />
              <Input
                label="Enter new password"
                maxLength={16}
                minLength={6}
                onInput={({ target: { value: inputPassword } }) => { setPassword(inputPassword); }}
                placeholder="Password"
                type="password"
              />
              <div className={authStyles.passwordStrengthBar}>
                <div
                  className={cn({
                    [authStyles.passwordStrengthBarContent]: true,
                    ...passwordHealth && { [authStyles[passwordHealth]]: true }
                  })}
                  style={{
                    width: passwordHealth === 'weak'
                      ? '30%'
                      : passwordHealth === 'medium'
                        ? '55%'
                        : passwordHealth === 'strong'
                          ? '100%'
                          : '0%'
                  }}
                />
              </div>
              <div className={authStyles.passwordStrengthNotes}>
                <div>Use a minimum of 6 characters (case sensitive) with at lease one number and one special character.</div>
                <div
                  className={cn({
                    [authStyles.currentPasswordStrength]: true,
                    ...passwordHealth && { [authStyles[passwordHealth]]: true }
                  })}
                >
                  {passwordHealth}
                </div>
              </div>
              <br />
              <br />
              <form onSubmit={(e) => { e.preventDefault(); resetPassword(); }}>
                <input className={authStyles.hiddenFormField} name="email" onChange={() => { }} type="email" value={email} />
                <input
                  className={authStyles.hiddenFormField}
                  name="password"
                  onChange={() => { }}
                  type="password"
                  value={password}
                />
                <Button
                  blockText
                  isBlue
                  isDisabled={(
                    (resetCode?.length !== 5)
                    || (!((passwordHealth === 'strong') || (passwordHealth === 'medium')))
                    || resettingPassword
                  )}
                  isFullWidth
                  isLarge
                  submit
                >
                  {resettingPassword ? 'Resetting Password...' : 'Reset Password'}
                </Button>
              </form>
            </>
          )
          : (
            <>
              <div className={authStyles.recommendationText} style={{ textAlign: 'center' }}>Password reset successfully</div>
              <br />
              <br />
              <Button
                blockText
                isBlue
                isFullWidth
                isLarge
                link={Pathname.authentication.signIn}
              >
                Sign In
              </Button>
            </>
          )
        )}

      <br />
      <br />
      <br />
      <br />

      <Snackbar
        isVisible={snackbar?.isVisible}
        message={snackbar?.message || ''}
        onClose={() => { setSnackbar(defaultSnackbar); }}
        type={snackbar?.type}
      />
    </AuthPage>
  );
};
