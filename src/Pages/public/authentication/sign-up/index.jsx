import React, { useState, useContext, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import { debounce } from 'lodash';
import { Checkbox, Collapse, Fade } from '@material-ui/core';
import { validate as isValidEmail } from 'email-validator';
import PasswordValidator from 'password-validator';
import { Pathname } from 'Routes';
import { Button } from 'Components';
import { AppContext, AuthContext } from 'Context';
import { AxiosCancelToken, dataFetcher, endpoints } from 'Api';
import { Input } from '../components/input-field';
import { AuthPage } from '../components/page';
import { StepStatus } from '../components/step-status';
import styles from './index.module.css';
import authStyles from '../auth.module.css';

const passwordSchema = (new PasswordValidator()).is().min(6).has()
  .symbols(1)
  .has()
  .digits(1);
const transitionTimeout = 300;

export const SignUp = () => {
  const navigate = useNavigate();
  const { setAppSnackbar } = useContext(AppContext);
  const { preferredEmail, forceAutoSignin, setToken, setPreferredEmail } = useContext(AuthContext);

  const [email, setEmail] = useState(isValidEmail(preferredEmail) ? preferredEmail : '');
  const [password, setPassword] = useState('');
  const [emailIsFixed, fixEmail] = useState(false);
  const [emailIsValid, setEmailValidity] = useState(false);
  const [accountExists, setAccountExistence] = useState(undefined);
  const [checkingAccountExistence, setAccountExistenceCheckingStatus] = useState(false);
  const [cancelToken, setCancelToken] = useState(undefined);

  const [termsAreAgreed, setTermsAgreement] = useState(false);

  const passwordHealth = (
    password
      ? passwordSchema.validate(password)
        ? password.length > 10
          ? 'strong'
          : 'medium'
        : 'weak'
      : ''
  );

  const { isValidating: signingInUser, mutate: signInUser } = useSWR([endpoints.signin, email, password], {
    fetcher: (url, inputEmail, inputPassword) => dataFetcher(url, { email: inputEmail, password: inputPassword }),
    onSuccess: ({ success, data }) => {
      if (success) {
        setToken(data?.token);
        setAppSnackbar({ message: 'Logged in successfully', isVisible: true });
        forceAutoSignin();
      } else {
        setAppSnackbar({ type: 'error', message: 'Incorrect password', isVisible: true });
      }
    },
    onError: () => {
      setAppSnackbar({ type: 'error', message: 'Oops! Something went wrong', isVisible: true });
    },
  });

  const { isValidating: signingUpUser, mutate: signUpUser } = useSWR([endpoints.signup, email, password], {
    fetcher: (url, inputEmail, inputPassword) => dataFetcher(url, { email: inputEmail, password: inputPassword }),
    onSuccess: ({ success }) => {
      if (success) {
        // navigate(Pathname.authentication.signIn);
        signInUser();
      }
    },
    onError: () => { },
  });

  const { mutate: checkAccountExistence } = useSWR([endpoints.signupEmail, email, cancelToken?.token], {
    fetcher: (url, inputEmail, uniqueCancelToken) => dataFetcher(url, { email: inputEmail }, uniqueCancelToken),
    onSuccess: ({ success }) => {
      setAccountExistenceCheckingStatus(false);

      if (success) {
        setAccountExistence(false);

        if (isValidEmail(preferredEmail)) { fixEmail(true); }
      } else {
        setAccountExistence(true);
      }

      setPreferredEmail(undefined);
    },
    onError: () => {
      setAccountExistenceCheckingStatus(false);
    },
  });

  const emailInputHandler = debounce(({ target: { value: inputEmail } }) => {
    if (isValidEmail(inputEmail)) {
      setEmail(inputEmail);
      setEmailValidity(true);
    } else {
      setEmail('');
      setAccountExistence(undefined);
      setEmailValidity(false);
    }
  }, 750, { trailing: true });

  useEffect(() => {
    if (email) {
      setAccountExistenceCheckingStatus(true);
      checkAccountExistence();
    }
  }, [email, setAccountExistenceCheckingStatus, checkAccountExistence]);

  useEffect(() => {
    if (preferredEmail && isValidEmail(preferredEmail)) {
      setAccountExistenceCheckingStatus(true);
      setEmailValidity(true);
      checkAccountExistence();
    }
  }, [preferredEmail, setAccountExistenceCheckingStatus, checkAccountExistence]);

  return (
    <AuthPage>
      <Link to={Pathname.authentication.signIn} className={styles.signInLink}>Log In</Link>

      {!emailIsFixed
        ? (
          <form onSubmit={(e) => { e.preventDefault(); }}>
            <StepStatus currentStep="1" totalSteps="2" />
            <Input
              defaultValue={email}
              isDisabled={(email === preferredEmail) && checkingAccountExistence}
              isLoading={checkingAccountExistence}
              label="Enter your email"
              onInput={(e) => {
                e.persist();
                emailInputHandler(e);
                setAccountExistenceCheckingStatus(false);

                if (cancelToken) { cancelToken.cancel(); }

                if (!e?.target?.value) {
                  setEmail('');
                  setAccountExistence(undefined);
                  setEmailValidity(false);
                } else {
                  setCancelToken(AxiosCancelToken.source());
                }
              }}
              placeholder="Email"
              type="email"
            />
            <Collapse in={!accountExists} timeout={transitionTimeout}>
              <Fade in={!accountExists} timeout={transitionTimeout}>
                <div className={styles.newsletter}>
                  <Checkbox
                    className={styles.newsletterCheckbox}
                    color="primary"
                    id="newsletter-sub"
                    onChange={({ target: { checked: termsAgreed } }) => { setTermsAgreement(termsAgreed); }}
                  />
                  <label className={styles.newsletterText} htmlFor="newsletter-sub">
                    Yes! I would like to receive updates, special offers and other information from Projector App.
                  </label>
                </div>
              </Fade>
            </Collapse>
            <Collapse in={accountExists} timeout={transitionTimeout}>
              <Fade in={accountExists} timeout={transitionTimeout}>
                <>
                  <br />
                  <br />
                </>
              </Fade>
            </Collapse>
            <div className={styles.termsContainer}>
              <div className={styles.terms}>
                <Collapse in={!accountExists} timeout={transitionTimeout}>
                  <Fade in={!accountExists} timeout={transitionTimeout}>
                    <span>
                      Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                      laudantium, totam rem. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.
                      Totam rem emo enim ipsam voluptatem voluptas sit aspernatur aut odit aut fugit. Totam rem emo enim
                    </span>
                  </Fade>
                </Collapse>
                <Collapse in={accountExists} timeout={transitionTimeout}>
                  <Fade in={accountExists} timeout={transitionTimeout}>
                    <span>You seem to have an existing projector account, Login instead?</span>
                  </Fade>
                </Collapse>
              </div>

              {accountExists === true
                ? (
                  <Button
                    blockText
                    isBlue
                    isDisabled={checkingAccountExistence
                      || !(emailIsValid && (accountExists !== undefined) && (accountExists === true))}
                    isFullWidth
                    isLarge
                    onClick={() => { setPreferredEmail(email); navigate(Pathname.authentication.signIn); }}
                  >
                    login
                  </Button>
                )
                : (
                  <Button
                    blockText
                    isBlue
                    isDisabled={checkingAccountExistence
                      || !(termsAreAgreed && emailIsValid && (accountExists !== undefined) && (accountExists === false))}
                    isFullWidth
                    isLarge
                    submit
                    onClick={() => { fixEmail(true); }}
                  >
                    Agree and Continue
                  </Button>
                )}
            </div>
          </form>
        )
        : (
          <form onSubmit={(e) => { e.preventDefault(); signUpUser(); }}>
            <StepStatus currentStep="2" totalSteps="2" />
            <input className={authStyles.hiddenFormField} name="email" onChange={() => { }} type="email" value={email} />
            <Input
              label="Enter a password"
              maxLength={16}
              minLength={6}
              onInput={({ target: { value: inputPassword } }) => { setPassword(inputPassword); }}
              placeholder="Password"
              type="password"
            />
            <div className={authStyles.passwordStrengthBar}>
              <div
                className={cn({
                  [authStyles.passwordStrengthBarContent]: true,
                  ...passwordHealth && { [authStyles[passwordHealth]]: true }
                })}
                style={{
                  width: passwordHealth === 'weak'
                    ? '30%'
                    : passwordHealth === 'medium'
                      ? '55%'
                      : passwordHealth === 'strong'
                        ? '100%'
                        : '0%'
                }}
              />
            </div>
            <div className={authStyles.passwordStrengthNotes}>
              <div>Use a minimum of 6 characters (case sensitive) with at lease one number and one special character.</div>
              <div
                className={cn({
                  [authStyles.currentPasswordStrength]: true,
                  ...passwordHealth && { [authStyles[passwordHealth]]: true }
                })}
              >
                {passwordHealth}
              </div>
            </div>
            <div className={styles.notes}>
              <>You’ll be using this email/phone to log in:</>
              <br />
              <div className={styles.notesEmail}>{email}</div>
            </div>
            <div>
              <Button
                blockText
                isBlue
                isDisabled={(!((passwordHealth === 'strong') || (passwordHealth === 'medium')))
                  || signingInUser || signingUpUser}
                isFullWidth
                isLarge
                submit
              >
                {(signingUpUser || signingInUser) ? 'Signing Up...' : 'Continue'}
              </Button>
            </div>
          </form>
        )}
    </AuthPage>
  );
};
