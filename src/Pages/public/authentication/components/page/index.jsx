import React from 'react';
import { Link } from 'react-router-dom';
import { Logo } from 'Components';
import { Pathname } from 'Routes';
import styles from '../../auth.module.css';

export const AuthPage = ({ children }) => (
  <div className={styles.authPage}>
    <div className={styles.authPageContent}>
      <Link to={Pathname.home}>
        <Logo className={styles.logo} isLarge isLight />
      </Link>
      {children}
    </div>
  </div>
);
