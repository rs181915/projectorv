import React from 'react';
import styles from './index.module.css';

export const StepStatus = ({ currentStep, totalSteps }) => (
  <div className={styles.stepStatus}>{`Step ${currentStep} of ${totalSteps}`}</div>
);
