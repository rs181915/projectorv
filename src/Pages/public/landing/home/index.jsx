import React from 'react';
import cn from 'classnames';
import { Link } from 'react-router-dom';
import { Button } from 'Components';
import tvIcon from 'Assets/tv.svg';
import laptopIcon from 'Assets/laptop.svg';
import phoneIcon from 'Assets/phone.svg';
import { Pathname } from 'Routes';
import { Page } from '../components/page';
import styles from './index.module.css';

const devices = [
  {
    deviceCategory: 'TV',
    deviceNames: ['Amazon Fire TV', 'Android TV', 'Apple TV', 'Roku', 'Samsung'],
    frameURL: tvIcon,
  },
  {
    deviceCategory: 'WEB',
    deviceNames: ['Chrome OS', 'macOS', 'Windows PC'],
    frameURL: laptopIcon,
  },
  {
    deviceCategory: 'MOBILE & TABLET',
    deviceNames: ['Amazon Fire Tablets', 'Android Phones & Tablets', 'iPhone and iPad'],
    frameURL: phoneIcon,
  },
];

export const Home = () => (
  <Page>
    <section className={styles.heroContainer}>
      <div className={cn({ [styles.heroImageContainer]: true, [styles.strechEdge]: true })}>
        <img
          alt="Projector"
          className={styles.heroImage}
          src="https://www.bgwild.com/wp-content/uploads/2018/06/danka-peter-178-unsplash-1080x675.jpg"
        />
      </div>
      <div className={styles.heroText}>
        <h1 className={styles.heroHeading}>STREAM YOUR HOME MOVIES</h1>
        <h1 className={styles.heroSubheading}>View, share, preserve forever.</h1>
        <p className={styles.heroDescription}>
          Now you have the ability to consolidate, preserve, curate and share all your home videos that you cherish.
          Allow your family and close friends to watch your family grow and keep up like never before.
        </p>
      </div>
    </section>
    <br />
    <section className={styles.heroContainer}>
      <div className={styles.heroImageContainer}>
        <div className={styles.planPricingDetails}>
          <Button
            className={styles.planPricingDetailsButton}
            isDarkBlue
            isFullWidth
            isLarge
            isOutlined
            link={Pathname.plans}
          >
            Share and Store Today For $4.95/MONTH
          </Button>
          <br />
          <br />
          <Link
            className={styles.signUpLink}
            to={Pathname.authentication.signUp}
          >
            or sign up to view your family and friends home videos for free
          </Link>
        </div>
      </div>
      <div className={styles.heroText} />
    </section>
    <br />
    <br />
    <br />
    <br />
    <section className={styles.devices}>
      <h4 className={styles.devicesTitle}>Acceses your videos anytime anywhere</h4>
      <div className={styles.deviceList}>
        {
          devices.map(({ deviceCategory, deviceNames, frameURL }, idx) => (
            <div className={styles.deviceItem} key={idx}>
              <img src={frameURL} className={styles.deviceFrame} alt={deviceCategory} />
              <div className={styles.deviceCategory}>{deviceCategory}</div>
              <div className={styles.deviceNameList}>
                {
                  deviceNames.map((name, idx2) => <div className={styles.deviceName} key={idx2}>{name}</div>)
                }
              </div>
            </div>
          ))
        }
      </div>
    </section>
    <section className={styles.pageEndDetails}>
      <Button
        alignCenter
        className={styles.pageEndSignUpLink}
        isDarkBlue
        link={Pathname.authentication.signUp}
      >
        Sign up now
      </Button>
    </section>
  </Page>
);
