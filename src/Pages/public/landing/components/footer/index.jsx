import React from 'react';
import { Link } from 'react-router-dom';
import { Logo } from 'Components';
import { Pathname } from 'Routes';
import styles from './index.module.css';

const links = [
  { name: 'ProjectorStream', link: Pathname.projectorStream },
  { name: 'Subscriber Agreement Privacy', link: Pathname.terms },
  { name: 'Rights Do Not Sell My Info', link: Pathname.userRights },
  { name: 'About Us', link: Pathname.about },
  { name: 'ProjectorStream Partner Program', link: Pathname.projectorStreamPartnerProgram },
];

export const Footer = () => (
  <footer className={styles.footer}>
    <div className={styles.footerContent}>
      <Logo className={styles.brandName} isLight />
      <div className={styles.linksContainer}>
        {links.map(({ name, link }, idx) => (
          <Link className={styles.link} key={idx} to={link}>{name}</Link>
        ))}
      </div>
      <div className={styles.copyright}>{`Copyright ProjectorStream, LLC ${new Date().getFullYear()}`}</div>
    </div>
  </footer>
);
