import React from 'react';
import cn from 'classnames';
import { Link } from 'react-router-dom';
import cameraRollIcon from 'Assets/camera-roll.svg';
import viewersIcon from 'Assets/viewers.svg';
import { Button } from 'Components';
import { Page } from '../components/page';
import styles from './index.module.css';

const Plan = ({ name, price, priceDetails, storage, viewers, isSuggested }) => (
  <div className={cn({ [styles.plan]: true, [styles.suggested]: isSuggested })}>
    <div className={styles.name}>{name}</div>
    <div className={styles.planContent}>
      <div className={styles.price}>
        <div className={styles.priceAmount}>{price}</div>
        <div className={styles.priceDetails}>{priceDetails || <>&nbsp;</>}</div>
      </div>
      <div className={styles.featuresList}>
        <div className={styles.feature}>
          <img alt="storage" src={cameraRollIcon} className={styles.featureIcon} />
          <div className={styles.featureText}>{`${storage} of Storage`}</div>
        </div>
        <div className={styles.feature}>
          <img alt="viewers" src={viewersIcon} className={styles.featureIcon} />
          <div className={styles.featureText}>{`Connect ${viewers} viewers`}</div>
        </div>
      </div>
      <div className={styles.tryPlan}>
        <Button className={styles.tryPlanButton} isFullWidth isOutlined isBlue>Try free for a week</Button>
        <div className={styles.purchase}>
          <>or </>
          <Link to="#" className={styles.purchaseLink}>purchase now</Link>
        </div>
      </div>
    </div>
  </div>
);

export const Plans = () => (
  <Page>
    <div className={styles.message}>Choose the right Projector Stream for you</div>
    <div className={styles.plansList}>
      <Plan name="Standard" price="$4.95" storage="500 GB" viewers="10" priceDetails="user / month" />
      <Plan name="Premium" price="$9.95" storage="1 TB" viewers="unlimited" isSuggested />
    </div>
  </Page>
);
