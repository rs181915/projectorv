import { Config } from 'Config';
import { ClientError } from 'Error';

const eventListeners = (events = [], handler, listeners = [], actionName) => {
  const numEvents = events.length;
  const numListeners = listeners.length;
  const addEventListeners = (i, j) => listeners[j].addEventListener(events[i], handler);
  const removeEventListeners = (i, j) => listeners[j].removeEventListener(events[i], handler);
  const action = actionName ? addEventListeners : removeEventListeners;

  for (let i = 0; i < numEvents; i += 1) {
    for (let j = 0; j < numListeners; j += 1) {
      action(i, j);
    }
  }
};

export {
  eventListeners,
  ClientError,
  Config,
};
