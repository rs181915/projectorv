import React, { useState } from 'react';
import cn from 'classnames';
import { Header } from '../header';
import { Sidebar } from '../sidebar';
import styles from './index.module.css';

export const Page = ({
  children,
  className,
  containerClassName,
  contentClassName,
  glassCompactHeaderBG,
  id,
  isProfileHeader,
  noBottomBar,
  noDefaultBg,
  noFixedSidebar,
  noHeader,
  noRegularHeader,
  transparentCompactHeader,
  userName,
  userProfileLink,
}) => {
  const [sidebarIsVisible, setSidebarVisibility] = useState(false);

  const revealSidebar = () => { setSidebarVisibility(true); };
  const hideSidebar = () => { setSidebarVisibility(false); };

  return (
    <div className={cn({ [containerClassName]: containerClassName })}>
      <Header
        glassCompactHeaderBG={glassCompactHeaderBG}
        isProfileHeader={isProfileHeader}
        noHeader={noHeader}
        noRegularHeader={noRegularHeader}
        onMenuClick={revealSidebar}
        transparentCompactHeader={transparentCompactHeader}
        userName={userName}
        userProfileLink={userProfileLink}
      />
      <main className={cn({ [styles.page]: true, [styles.noDefaultBg]: noDefaultBg, [className]: className })}>
        <Sidebar
          noBottomBar={noBottomBar}
          noFixedSidebar={noFixedSidebar}
          onClose={hideSidebar}
          sidebarIsVisible={sidebarIsVisible}
        />
        <div className={cn({ [styles.pageContent]: true, [contentClassName]: contentClassName })} id={id}>
          {children}
        </div>
      </main>
    </div>
  );
};
