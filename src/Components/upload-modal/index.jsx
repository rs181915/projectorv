import React, { useContext, useRef, useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import cn from 'classnames';
import filesize from 'filesize';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import useSWR from 'swr';
import Lottie from 'react-lottie';
import {
  Dialog,
  DialogActions as MuiDialogActions,
  DialogContent,
  DialogTitle,
  Fade,
  FormControlLabel,
  Menu as MuiMenu,
  MenuItem as MuiMenuItem,
  Radio as MuiRadio,
  RadioGroup,
  Slide,
  Typography,
  withStyles,
} from '@material-ui/core';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import { FileDrop } from 'react-file-drop';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { AppContext } from 'Context';
import { Pathname } from 'Routes';
import { dataFetcher, endpoints } from 'Api';
import addFileImage from 'Assets/add-file@2x.png';
import copyLinkImage from 'Assets/copy-link@2x.png';
import { ReactComponent as AddIcon } from 'Assets/add.svg';
import { ReactComponent as CloseIcon } from 'Assets/close.svg';
import { ReactComponent as BackIcon } from 'Assets/chevron-left.svg';
import VideoUploading from 'Assets/video-uploading.json';
import VideoUploaded from 'Assets/video-uploaded.json';
import { GroupSelector } from '../group-selector';
import { Button } from '../button';
import { Text } from '../text';
import styles from './index.module.css';

const MenuItem = withStyles({
  root: {
    backgroundColor: 'transparent !important',
    fontFamily: 'Poppins, sans-serif',
    padding: '0 !important',
  },
})(MuiMenuItem);
const DialogActions = withStyles({ root: { padding: 'unset' } })(MuiDialogActions);
const Radio = withStyles({ root: { color: 'black' }, checked: { color: 'black !important' } })(MuiRadio);
const Menu = withStyles({
  paper: {
    backgroundColor: '#fff',
    borderRadius: 0,
    boxShadow: '0px 0px 12px 0px #00000029',
    width: 'var(--selectbox-menu-width)',
  },
})(MuiMenu);

const ProgressTracker = ({ steps = [], currentStep, }) => (
  currentStep
    ? (
      <>
        <div className={styles.currentStepName}>{steps[currentStep - 1]}</div>
        <div className={styles.progressTracker}>
          {steps.map((stepName, idx) => (
            <React.Fragment key={idx}>
              {idx ? <div className={styles.progressTrackerStepBar} /> : null}
              <div
                className={cn({
                  [styles.progressTrackerStepContainer]: true,
                  [styles.currentProgressStep]: (idx + 1) <= currentStep
                })}
              >
                <div className={styles.progressTrackerStepCount}>{idx + 1}</div>
                <div className={styles.progressTrackerStepName}>{stepName}</div>
              </div>
            </React.Fragment>
          ))}
        </div>
      </>
    )
    : null
);

const ContentBoxTitle = ({ title, description }) => (
  <div className={styles.contentBoxTitleContainer}>
    <Text.SectionTitle className={styles.contentBoxTitle}>{title}</Text.SectionTitle>
    {description && <p className={styles.contentBoxDescription}>{description}</p>}
  </div>
);

const ContentBox = ({ children, className, flex }) => (
  <section className={cn({ [styles.contentBoxContainer]: true, [className]: className })}>
    <div className={cn({ [styles.contentBox]: true, [styles.flex]: flex })}>
      {children}
    </div>
  </section>
);

const ContentBoxSection = ({ children, className, title, description }) => (
  <section className={cn({ [styles.contentBoxSection]: true, [className]: className })}>
    <ContentBoxTitle title={title} description={description} />
    <div className={styles.contentBoxSectionWrapper}>
      {children}
    </div>
  </section>
);

const VideoPreview = ({ defaultVideoThumbnail, details, type, link }) => {
  const { setAppSnackbar } = useContext(AppContext);

  const minutes = ~~((details?.duration) / 60);
  const seconds = ~~((details?.duration) % 60);

  return (
    <div className={styles.videoPreview}>
      <img className={styles.videoPreviewThumbnail} alt=" " src={defaultVideoThumbnail} />
      <div className={styles.videoPreviewDetails}>
        <h3 className={styles.videoPreviewDetailsTitle}>{`Video ${type === 'link' ? 'link' : 'details'}`}</h3>
        {(type === 'link') && (
          <div className={styles.videoPreviewLink}>
            <span
              style={{
                display: 'inline',
                flexGrow: '0',
                flexShrink: '0',
                maxWidth: '100%',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                width: 'calc(100% - 2.2em)',
              }}
            >
              {link}
            </span>
            <CopyToClipboard text={link} onCopy={() => { setAppSnackbar({ isVisible: true, message: 'Link Copied' }); }}>
              <button
                style={{
                  backgroundColor: 'transparent',
                  border: 'none',
                  cursor: 'pointer',
                  height: '2em',
                  margin: '0',
                  padding: '0',
                  position: 'relative',
                  width: '2em',
                }}
              >
                <img
                  style={{
                    height: '100%',
                    left: '0',
                    position: 'absolute',
                    top: '0',
                    width: '100%',
                  }}
                  src={copyLinkImage}
                />
              </button>
            </CopyToClipboard>
          </div>
        )}
        {(type === 'details') && (
          <table>
            <tbody>
              <tr>
                <td>Video play time</td>
                <td>
                  {details?.duration && (
                    `${~~(minutes / 10) ? minutes : `0${minutes}`}:${~~(seconds / 10) ? seconds : `0${seconds}`}`
                  )}
                </td>
              </tr>
              <tr>
                <td>Video size</td>
                <td>{details?.size && filesize(details?.size)}</td>
              </tr>
            </tbody>
          </table>
        )}
      </div>
    </div>
  );
};

const InputField = ({ alignLeft, className, defaultValue, icon: Icon, id, isDisabled, label, onChange, placeholder, type }) => {
  const [isFocused, setFocus] = useState(false);

  const focus = () => { setFocus(true); };
  const blur = () => { setFocus(false); };

  return (
    <label
      className={cn({
        [styles.inputField]: true,
        [styles.alignLeft]: alignLeft,
        [styles.inputFieldFocus]: isFocused,
        [className]: className,
      })}
      htmlFor={id}
    >
      <div className={styles.inputFieldText}>
        {type === 'textarea'
          ? (
            <textarea
              className={styles.input}
              defaultValue={defaultValue}
              disabled={isDisabled}
              id={id}
              onBlur={blur}
              onChange={({ target: { value } }) => { onChange(value); }}
              onFocus={focus}
              placeholder={placeholder}
              rows="3"
            />
          )
          : (
            <input
              className={styles.input}
              defaultValue={defaultValue}
              disabled={isDisabled}
              id={id}
              onBlur={blur}
              onChange={({ target: { value } }) => { onChange(value); }}
              onFocus={focus}
              placeholder={placeholder}
              type={type}
            />
          )}
        <div className={styles.inputFieldLabel}>{label}</div>
      </div>
      {Icon && <Icon />}
    </label>
  );
};

const PlaylistInput = ({
  className,
  defaultNames = [],
  defaultValues = [],
  description,
  isDisabled,
  label,
  onSelect,
}) => {
  const { setAppSnackbar } = useContext(AppContext);
  const defaultOptions = {};

  defaultValues.forEach((key, i) => { defaultOptions[key] = defaultNames[i]; });

  const [selectedOptions, selectOptions] = useState(defaultOptions);
  const [createMenuIsVisible, setCreateMenuVisibility] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [options, setOptions] = useState([]);
  const [newPlaylistName, setNewPlaylistName] = useState('');

  const showCreateMenu = () => { setCreateMenuVisibility(true); };
  const hideCreateMenu = () => { setCreateMenuVisibility(false); };

  const onValueSelect = (selectedVal, selectedName) => {
    const currentOptions = { ...selectedOptions };

    if (currentOptions[selectedVal]) {
      delete currentOptions[selectedVal];
    } else {
      currentOptions[selectedVal] = selectedName;
    }

    selectOptions(currentOptions);
    onSelect(Object.keys(currentOptions), Object.values(currentOptions));
  };

  const { isValidating: gettingPlaylists, mutate: getPlaylists } = useSWR(endpoints.getMyPlaylist, {
    onSuccess: ({ success, data }) => {
      if (success) {
        setOptions(data?.map(({ id, title }) => ({ name: title, value: id })));
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while fetching you playlists' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while fetching you playlists' });
    },
  });

  const { isValidating: addingNewPlaylist, mutate: addNewPlaylist } = useSWR([
    endpoints.addEditPlaylist, newPlaylistName,
  ], {
    fetcher: (url, playlistName) => dataFetcher(url, { title: playlistName }),
    onSuccess: ({ success }) => {
      if (success) {
        hideCreateMenu();
        getPlaylists();
        setAppSnackbar({
          isVisible: true,
          message: `Playlist '${newPlaylistName}' was created`,
        });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your playlist' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your playlist' });
    },
  });

  useEffect(() => { getPlaylists(); }, []);

  return (
    <div className={cn({ [styles.selectBoxContainer]: true, [className]: className })}>
      <div
        aria-controls="select-box"
        aria-label={label}
        className={styles.selectBox}
        onClick={(e) => {
          if (!isDisabled) {
            setAnchorEl(e.currentTarget);
            document.body.style.setProperty('--selectbox-menu-width', `${e.currentTarget.clientWidth}px`);
          }
        }}
        onKeyPress={undefined}
        role="menu"
        tabIndex={0}
      >
        <div className={styles.selectBoxText}>
          <div className={styles.selectBoxLabel}>{label}</div>
          <div className={styles.selectBoxDescription}>
            {!(Object.keys(selectedOptions)?.length) ? description : Object.values(selectedOptions).join(', ')}
          </div>
        </div>
        <div className={styles.selectBoxDropdownIcon}>▾</div>
      </div>

      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'bottom', horizontal: 2 }}
        getContentAnchorEl={null}
        id="select-box"
        keepMounted
        MenuListProps={{ className: styles.selectBoxMenuContent }}
        onClose={() => { hideCreateMenu(); setAnchorEl(null); }}
        open={Boolean(anchorEl)}
        PaperProps={{ className: styles.selectBoxMenu }}
        transitionDuration={0}
      >
        <div>
          {!createMenuIsVisible
            ? (
              <div className={styles.selectBoxOptionsList}>
                <MenuItem className={styles.selectBoxOptionsTitle} disableRipple disableTouchRipple>{label}</MenuItem>

                {gettingPlaylists && (
                  <MenuItem className={styles.selectBoxOption} disableRipple disableTouchRipple>Loading ...</MenuItem>
                )}

                {!gettingPlaylists && options?.map(({ name: optionName, value: optionValue }) => (optionName && (
                  <MenuItem
                    className={cn({
                      [styles.selectBoxOption]: true,
                      [styles.selected]: Object.keys(selectedOptions).includes(optionValue),
                    })}
                    disableRipple
                    disableTouchRipple
                    key={optionValue}
                    onClick={() => { onValueSelect(optionValue, optionName); }}
                  >
                    {Object.keys(selectedOptions).includes(optionValue)
                      ? <CheckBoxIcon />
                      : <CheckBoxOutlineBlankIcon />}
                    &nbsp;&nbsp;&nbsp;
                    <Typography variant="inherit" noWrap>
                      {optionName}
                    </Typography>
                  </MenuItem>
                )))}

                <MenuItem
                  className={cn({ [styles.selectBoxOption]: true, [styles.addButton]: true })}
                  disableRipple
                  disableTouchRipple
                  onClick={showCreateMenu}
                >
                  <Typography variant="inherit" noWrap>Add new</Typography>
                </MenuItem>
              </div>
            )
            : (
              <div className={styles.selectBoxCreate}>
                <div className={styles.selectBoxCreateHeader}>
                  <span className={styles.selectBoxCreateTitle}>{`Add new ${label}`}</span>
                  <button
                    className={styles.selectBoxCreateBackButton}
                    disabled={addingNewPlaylist}
                    onClick={hideCreateMenu}
                  >
                    Back
                  </button>
                </div>
                <input className={styles.selectBoxCreateInput} onChange={(e) => { setNewPlaylistName(e.target.value); }} />
                <div className={styles.selectBoxCreateFooter}>
                  <button
                    className={styles.selectBoxCreateAddButton}
                    disabled={addingNewPlaylist || !newPlaylistName?.trim()}
                    onClick={addNewPlaylist}
                    type="submit"
                  >
                    {addingNewPlaylist ? 'Adding ...' : 'Add'}
                  </button>
                </div>
              </div>
            )}
        </div>
      </Menu>
    </div>
  );
};

const CategoryInput = ({
  categoryID,
  className,
  defaultName,
  defaultValue,
  description,
  isDisabled,
  isSubcategory = false,
  label,
  onSelect,
}) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [selectedOptionValue, selectOptionValue] = useState(defaultValue || '');
  const [selectedOptionName, selectOptionName] = useState(defaultName || '');
  const [createMenuIsVisible, setCreateMenuVisibility] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [options, setOptions] = useState([]);
  const [newCategoryName, setNewCategoryName] = useState('');

  const showCreateMenu = () => { setCreateMenuVisibility(true); };
  const hideCreateMenu = () => { setCreateMenuVisibility(false); };

  const onValueSelect = (selectedVal, selectedName) => {
    selectOptionValue(selectedVal);
    selectOptionName(selectedName);
    onSelect(selectedVal, selectedName);
    setAnchorEl(null);
  };

  const { isValidating: gettingCategories, mutate: getCategories } = useSWR([endpoints.getMyCategory, categoryID], {
    fetcher: (url, parent_id) => dataFetcher(url, { ...categoryID && { parent_id } }),
    onSuccess: ({ success, data }) => {
      if (success) {
        setOptions(data?.map(({ id, title }) => ({ name: title, value: id })));
      } else {
        setAppSnackbar({
          isVisible: true,
          type: 'error',
          message: 'Oops! Something went wrong while fetching your categories',
        });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while fetching your categories' });
    },
  });

  const { isValidating: addingNewCategory, mutate: addNewCategory } = useSWR([
    endpoints.addEditCategory, newCategoryName, categoryID,
  ], {
    fetcher: (url, categoryName, parentCategoryID) => dataFetcher(url, {
      title: categoryName,
      ...parentCategoryID && { parent_id: parentCategoryID }
    }),
    onSuccess: ({ success }) => {
      if (success) {
        hideCreateMenu();
        getCategories();
        setAppSnackbar({
          isVisible: true,
          message: `${isSubcategory ? 'Sub-Category' : 'Category'} '${newCategoryName}' was created`,
        });
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your category' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong while creating your category' });
    },
  });

  useEffect(() => { getCategories(); }, []);

  useEffect(() => { if (isSubcategory) { getCategories(); } }, [categoryID]);

  return (
    <div className={cn({ [styles.selectBoxContainer]: true, [className]: className, [styles.disabled]: isDisabled })}>
      <div
        aria-controls="select-box"
        aria-label={label}
        className={styles.selectBox}
        onClick={(e) => {
          if (!isDisabled) {
            setAnchorEl(e.currentTarget);
            document.body.style.setProperty('--selectbox-menu-width', `${e.currentTarget.clientWidth}px`);
          }
        }}
        onKeyPress={undefined}
        role="menu"
        tabIndex={0}
      >
        <div className={styles.selectBoxText}>
          <div className={styles.selectBoxLabel}>{label}</div>
          <div className={styles.selectBoxDescription}>
            {!(selectedOptionValue && defaultValue) ? description : selectedOptionName}
          </div>
        </div>
        <div className={styles.selectBoxDropdownIcon}>▾</div>
      </div>

      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'bottom', horizontal: 2 }}
        getContentAnchorEl={null}
        id="select-box"
        keepMounted
        MenuListProps={{ className: styles.selectBoxMenuContent }}
        onClose={() => { hideCreateMenu(); setAnchorEl(null); }}
        open={Boolean(anchorEl)}
        PaperProps={{ className: styles.selectBoxMenu }}
        transitionDuration={0}
      >
        <div>
          {!createMenuIsVisible
            ? (
              <div className={styles.selectBoxOptionsList}>
                <MenuItem className={styles.selectBoxOptionsTitle} disableRipple disableTouchRipple>{label}</MenuItem>

                {gettingCategories && (
                  <MenuItem className={styles.selectBoxOption} disableRipple disableTouchRipple>Loading ...</MenuItem>
                )}

                {!gettingCategories && options?.map(({ name: optionName, value: optionValue }) => (optionName && (
                  <MenuItem
                    className={styles.selectBoxOption}
                    disableRipple
                    disableTouchRipple
                    key={optionValue}
                    onClick={() => { onValueSelect(optionValue, optionName); }}
                  >
                    <Typography variant="inherit" noWrap>{optionName}</Typography>
                  </MenuItem>
                )))}

                <MenuItem
                  className={cn({ [styles.selectBoxOption]: true, [styles.addButton]: true })}
                  disableRipple
                  disableTouchRipple
                  onClick={showCreateMenu}
                >
                  <Typography variant="inherit" noWrap>Add new</Typography>
                </MenuItem>
              </div>
            )
            : (
              <div className={styles.selectBoxCreate}>
                <div className={styles.selectBoxCreateHeader}>
                  <span className={styles.selectBoxCreateTitle}>{`Add new ${label}`}</span>
                  <button
                    className={styles.selectBoxCreateBackButton}
                    disabled={addingNewCategory}
                    onClick={hideCreateMenu}
                  >
                    Back
                  </button>
                </div>
                <input className={styles.selectBoxCreateInput} onChange={(e) => { setNewCategoryName(e.target.value); }} />
                <div className={styles.selectBoxCreateFooter}>
                  <button
                    className={styles.selectBoxCreateAddButton}
                    disabled={addingNewCategory || !newCategoryName?.trim()}
                    onClick={addNewCategory}
                    type="submit"
                  >
                    {addingNewCategory ? 'Adding ...' : 'Add'}
                  </button>
                </div>
              </div>
            )}
        </div>
      </Menu>
    </div>
  );
};

const DialogFooter = ({ children }) => (
  <DialogActions className={styles.footer}>
    <div className={styles.footerContent}>
      {children}
    </div>
  </DialogActions>
);

const videoVisibilityOptions = [
  { name: 'Anyone can view on projector', value: '2' },
  { name: 'Only I can view', value: '1' },
  { name: 'Choose a group to share with', value: '3' },
];
const defaultVideoDetails = {
  name: undefined,
  duration: undefined,
  size: undefined,
};
const defaultVideoVisibility = videoVisibilityOptions[0].value;

const Modal = ({ isVisible, onHide }) => {
  const { setAppSnackbar } = useContext(AppContext);
  const navigate = useNavigate();

  let domain = window.location.href;

  const fileInputRef = useRef(null);

  domain = domain?.replace(/http[s]?:\/\//, '');
  domain = domain?.split('/')[0];

  const [currentStep, setCurrentStep] = useState(undefined);
  const [defaultVideoThumbnail, setDefaultVideoThumbnail] = useState(undefined);
  const [videoCategory, setVideoCategory] = useState(undefined);
  const [videoCategoryName, setVideoCategoryName] = useState(undefined);
  const [videoDescription, setVideoDescription] = useState('');
  const [videoDetails, setVideoDetails] = useState(defaultVideoDetails);
  const [videoFile, setVideoFile] = useState(undefined);
  const [videoGroup, setVideoGroup] = useState(undefined);
  const [videoID, setVideoID] = useState(undefined);
  const [videoPlaylists, setVideoPlaylists] = useState([]);
  const [videoPlaylistsNames, setVideoPlaylistsNames] = useState([]);
  const [videoSubCategory, setVideoSubCategory] = useState(undefined);
  const [videoSubCategoryName, setVideoSubCategoryName] = useState(undefined);
  const [videoThumbnails, setVideoThumbnails] = useState([]);
  const [videoTitle, setVideoTitle] = useState('');
  const [videoVisibility, setVideoVisibility] = useState(defaultVideoVisibility);
  const [videoIsPublished, setVideoPublishStatus] = useState(false);
  const [videoIsUploaded, setVideoUploadStatus] = useState(false);

  const [videoUploadData, setVideoUploadData] = useState(undefined);
  const [videoCategoryPlaylistData, setVideoCategoryPlaylistData] = useState(undefined);
  const [videoVisibilityData, setVideoVisibilityData] = useState(undefined);

  const [uploadedData, setUploadedData] = useState({
    category_id: undefined,
    description: undefined,
    group_id: undefined,
    playlist_id: undefined,
    publish_date: undefined,
    status: undefined,
    subcategory_id: undefined,
    thumbnails: undefined,
    title: undefined,
    token: undefined,
    video_file: undefined,
    video_id: undefined,
    visibility: undefined,
  });

  const moveToPreviousStep = () => {
    setCurrentStep(-1);
    setTimeout(() => { setCurrentStep(currentStep - 1); }, 10);
  };
  const moveToNextStep = () => {
    if ((currentStep + 1) <= 3) {
      setCurrentStep(-1);
      setTimeout(() => { setCurrentStep(currentStep + 1); }, 10);
    }
  };

  const { isValidating: uploadingVideo, mutate: uploadVideoContent } = useSWR([endpoints.addVideoContent, videoUploadData], {
    fetcher: (url, formData) => dataFetcher(url, formData),
    onSuccess: ({ success, video_id }) => {
      if (success) {
        moveToNextStep();
        if (!videoIsUploaded) {
          setVideoID(video_id);
          setVideoUploadStatus(true);
        }
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { isValidating: uploadingCategoryPlaylist, mutate: updateCategoryPlaylistContent } = useSWR([
    endpoints.addVideoContent, videoCategoryPlaylistData,
  ], {
    fetcher: (url, formData) => dataFetcher(url, formData),
    onSuccess: ({ success }) => {
      if (success) {
        moveToNextStep();
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const { isValidating: uploadingVisibility, mutate: updateVisibilityContent } = useSWR([
    endpoints.addVideoContent, videoVisibilityData,
  ], {
    fetcher: (url, formData) => dataFetcher(url, formData),
    onSuccess: ({ success }) => {
      if (success) {
        setVideoPublishStatus(true);
        setTimeout(() => { onHide(); navigate(Pathname.yourVideos); }, 3000);
      } else {
        setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
      }
    },
    onError: () => {
      setAppSnackbar({ isVisible: true, type: 'error', message: 'Oops! Something went wrong' });
    },
  });

  const getBase64FromFile = (inputFile) => {
    const fileReader = new FileReader();

    return new Promise((resolve) => {
      fileReader.onerror = () => {
        fileReader.abort();
        resolve('');
      };

      fileReader.onload = () => {
        resolve(fileReader.result.replace(/^data:.+;base64,/, ''));
      };
      fileReader.readAsDataURL(inputFile);
    });
  };

  const getBase64VideoThumbnails = async () => {
    let base64VideoThumbnails = '';

    return new Promise((resolve) => {
      videoThumbnails?.forEach(async (videoThumbnail, idx) => {
        const base64Thumbnail = await getBase64FromFile(videoThumbnail);

        base64VideoThumbnails = `${base64VideoThumbnails}${base64Thumbnail}`;

        if ((idx + 1) < videoThumbnails?.length) { base64VideoThumbnails = `${base64VideoThumbnails}, }`; }

        if ((idx + 1) === videoThumbnails?.length) { resolve(base64VideoThumbnails); }
      });
    });
  };

  const uploadVideo = async () => {
    const formData = new FormData();

    if (!videoIsUploaded) {
      setUploadedData({
        ...uploadedData,
        description: videoDescription,
        title: videoTitle,
        video_file: videoFile,
        thumbnails: videoThumbnails,
      });

      if (videoDescription) { formData.append('description', videoDescription); }
      if (videoTitle) { formData.append('title', videoTitle); }
      if (videoThumbnails) { formData.append('thumbnails', await getBase64VideoThumbnails()); }
      formData.append('status', 0);
      formData.append('video_file', videoFile);
    } else {
      setUploadedData({
        ...uploadedData,
        ...videoThumbnails !== uploadedData.thumbnails && { thumbnails: videoThumbnails },
        ...videoDescription !== uploadedData.description && { description: videoDescription },
        ...videoTitle !== uploadedData.title && { title: videoTitle },
      });

      formData.append('video_id', videoID);

      if (videoThumbnails !== uploadedData.thumbnails) {
        formData.append('thumbnails', await getBase64VideoThumbnails());
      }
      if (videoDescription !== uploadedData.description) {
        formData.append('description', videoDescription);
      }
      if (videoTitle !== uploadedData.title) {
        formData.append('title', videoTitle);
      }
    }

    setVideoUploadData(formData);
  };

  const updateCategoryPlaylist = () => {
    const formData = new FormData();

    setUploadedData({
      ...uploadedData,
      ...videoCategory !== uploadedData.category_id && { category_id: videoCategory },
      ...videoSubCategory !== uploadedData.subcategory_id && { subcategory_id: videoSubCategory },
      ...videoPlaylists?.length !== uploadedData.playlist_id && { playlist_id: videoPlaylists?.join(',') },
    });

    formData.append('video_id', videoID);
    formData.append('category_id', videoCategory);
    formData.append('subcategory_id', videoSubCategory);
    formData.append('playlist_id', videoPlaylists?.join(','));
    formData.append('status', 0);

    setVideoCategoryPlaylistData(formData);
  };

  const updateVisibility = () => {
    const formData = new FormData();

    setUploadedData({
      ...uploadedData,
      ...videoVisibility !== uploadedData.visibility && { visibility: videoVisibility },
      ...(videoVisibility === '3') && { group_id: videoGroup },
    });

    formData.append('video_id', videoID);
    formData.append('visibility', videoVisibility);
    formData.append('status', 1);
    if (videoVisibility === '3') {
      formData.append('group_id', videoGroup);
    }

    setVideoVisibilityData(formData);
  };

  useEffect(() => {
    if (videoUploadData && currentStep === 1) { uploadVideoContent(); }
  }, [videoUploadData]);
  useEffect(() => {
    if (videoCategoryPlaylistData && currentStep === 2) { updateCategoryPlaylistContent(); }
  }, [videoCategoryPlaylistData]);
  useEffect(() => {
    if (videoVisibilityData && currentStep === 3) { updateVisibilityContent(); }
  }, [videoVisibilityData]);

  useEffect(() => (() => {
    videoThumbnails.forEach((videoThumbnail) => { URL.revokeObjectURL(videoThumbnail); });
  }), []);

  useEffect(() => {
    if (videoVisibility === videoVisibilityOptions[2].value) {
      document.getElementById('group-selector').scrollIntoView();
    }
  }, [videoVisibility]);

  const onTargetClick = () => {
    fileInputRef.current.click();
  };

  useEffect(() => { setVideoSubCategory(undefined); setVideoSubCategoryName(undefined); }, [videoCategory]);

  const onFileInputChange = (e) => {
    onFileInputChange();
    const file = e.target.files[0];
    const fileReader = new FileReader();

    if (file.type.match('image')) {
      fileReader.onload = () => {
        setDefaultVideoThumbnail(fileReader.result);
      };

      fileReader.readAsDataURL(file);
    } else {
      fileReader.onload = () => {
        const blob = new Blob([fileReader.result], { type: file.type });
        const url = URL.createObjectURL(blob);
        const video = document.getElementById('video-player');

        const snapImage = () => {
          const canvas = document.createElement('canvas');
          canvas.width = video.videoWidth;
          canvas.height = video.videoHeight;
          canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
          const image = canvas.toDataURL();
          const success = image.length > 100000;

          if (success) {
            setDefaultVideoThumbnail(image);
            URL.revokeObjectURL(url);
          }

          return success;
        };

        const timeupdate = () => {
          if (snapImage()) {
            video.removeEventListener('timeupdate', timeupdate);
            video.pause();
          }
        };

        video.addEventListener('loadeddata', () => {
          if (snapImage()) {
            setVideoDetails({ name: file.name, size: file.size, duration: video.duration });
            video.removeEventListener('timeupdate', timeupdate);
          }
        });

        video.addEventListener('timeupdate', timeupdate);
        video.preload = 'metadata';
        video.src = url;
        video.muted = true;
        video.playsInline = true;

        if (video.play) { video.play(); }
      };

      fileReader.readAsArrayBuffer(file);
    }

    setVideoFile(file);
    setCurrentStep(1);
  };

  return (
    <Fade in={isVisible} timeout={isVisible ? 300 : 250}>
      <div className={styles.body}>
        <video id="video-player" style={{ display: 'none' }} />
        <input
          accept="image/*"
          disabled={uploadingVideo}
          id="video-thumbnails"
          multiple
          onChange={(e) => {
            const files = e?.target?.files;
            const totalFiles = files.length;
            const tmpVideoThumbnails = [];

            for (let i = 0; i < totalFiles; i += 1) {
              tmpVideoThumbnails.push(files[i]);
            }

            setVideoThumbnails(tmpVideoThumbnails);
          }}
          style={{ display: 'none' }}
          type="file"
        />
        <input
          accept=".mp4,.mov"
          id="video-file"
          ref={fileInputRef}
          onChange={onFileInputChange}
          style={{ display: 'none' }}
          type="file"
        />

        <DialogTitle className={styles.header}>
          <div className={styles.headerContent}>
            <BackIcon className={styles.backButton} onClick={uploadingVideo ? undefined : onHide} />
            <Text.PageTitle className={styles.headerText}>
              {!videoIsPublished
                ? currentStep
                  ? (videoTitle || videoDetails.name || 'Upload Videos')
                  : 'Upload Videos'
                : 'Video Published'}
            </Text.PageTitle>
            <CloseIcon className={styles.closeButton} onClick={uploadingVideo ? undefined : onHide} />
          </div>
          {!videoIsPublished && (
            <ProgressTracker currentStep={currentStep} steps={['Details', 'Categories', 'Previous Details']} />
          )}
        </DialogTitle>

        {videoIsPublished
          ? (
            <DialogContent className={cn({ [styles.content]: true, [styles.publishedContainer]: true })}>
              <ContentBox className={styles.publishedContent}>
                <Lottie
                  options={{
                    loop: true,
                    autoplay: true,
                    animationData: VideoUploaded,
                    rendererSettings: {
                      preserveAspectRatio: 'xMidYMid slice'
                    },
                  }}
                  height={256}
                  style={{ flexShrink: 0 }}
                  width={256}
                />
                <Typography className={styles.videoPublishedText}>Video Published</Typography>
              </ContentBox>
            </DialogContent>
          )
          : !currentStep
            ? (
              <>
                <DialogContent dropEffect="copy" className={styles.content}>
                  <FileDrop onTargetClick={onTargetClick}>
                    <label htmlFor="video-file" className={styles.addFilesSection}>
                      <div className={styles.addFilesSectionImageContainer}>
                        <img alt="upload video icon" className={styles.addFilesSectionImage} src={addFileImage} />
                      </div>
                      <div className={styles.addFilesSectionText}>
                        <div className={styles.addFilesSectionTextTitle}>Drag and drop video files to upload</div>
                        <div className={styles.addFilesSectionTextDescription}>
                          Your videos will be private until you publish them.
                        </div>
                      </div>
                      <div className={styles.addFilesSectionButton}>Select Files</div>
                    </label>
                  </FileDrop>
                </DialogContent>
                <DialogFooter>
                  <div className={styles.uploadTerms}>
                    By submitting your videos to Projector,
                    you acknowledge that you agree to Projector’s Terms of Service and Community Guidelines.
                    Please make sure that you do not violate others’ copyright or privacy rights. Learn more
                  </div>
                </DialogFooter>
              </>
            )
            : currentStep === 1
              ? (
                <>
                  <DialogContent className={styles.content}>
                    {uploadingVideo && (
                      <>
                        <Lottie
                          options={{
                            loop: true,
                            autoplay: true,
                            animationData: VideoUploading,
                            rendererSettings: {
                              preserveAspectRatio: 'xMidYMid slice'
                            },
                          }}
                          height={16}
                          style={{ flexShrink: 0 }}
                          width="100%"
                        />
                        <br />
                      </>
                    )}
                    <ContentBox flex>
                      <div className={styles.contentBox}>
                        <ContentBoxSection title="Details" className={styles.detailsBoxSection}>
                          <InputField
                            defaultValue={videoTitle || ''}
                            label="Title"
                            isDisabled={uploadingVideo}
                            onChange={setVideoTitle}
                            placeholder="Add a title to your video"
                            type="text"
                          />
                          <InputField
                            defaultValue={videoDescription || ''}
                            label="Description"
                            isDisabled={uploadingVideo}
                            onChange={setVideoDescription}
                            placeholder="Tell your viewers what your video is about"
                            type="textarea"
                          />
                        </ContentBoxSection>
                        <ContentBoxSection
                          title="Thumbnail"
                          description="Select or upload a picture that shows what’s in your video.
                          A great thumbnail stands out and draws viewers’ attention."
                          className={styles.thumbnailBoxSection}
                        >
                          <div className={styles.videoThumbnailsContainer}>
                            <label
                              className={cn({ [styles.videoThumbnail]: true, [styles.thumbnailUploadButton]: true })}
                              htmlFor="video-thumbnails"
                            >
                              <AddIcon className={styles.videoThumbnailAddIcon} />
                            </label>
                            {videoThumbnails?.map((videoThumbnail, idx) => (
                              <div className={styles.videoThumbnail} key={idx}>
                                <img
                                  alt={`video thumbnail ${idx}`}
                                  className={styles.videoThumbnailImage}
                                  src={URL.createObjectURL(videoThumbnail)}
                                />
                              </div>
                            ))}
                          </div>
                        </ContentBoxSection>
                      </div>
                      <VideoPreview
                        defaultVideoThumbnail={defaultVideoThumbnail}
                        details={videoDetails}
                        type="details"
                      />
                    </ContentBox>
                  </DialogContent>
                  <DialogFooter>
                    <Button
                      className={styles.navButton}
                      isBlue
                      isDisabled={uploadingVideo || !(videoTitle && videoDescription && videoThumbnails?.length && videoFile)}
                      isOutlined
                      onClick={(videoIsUploaded
                        && (videoDescription === uploadedData.description)
                        && (videoTitle === uploadedData.title)
                        && (videoThumbnails === uploadedData.thumbnails)
                      )
                        ? moveToNextStep
                        : uploadVideo}
                    >
                      {videoIsUploaded
                        ? uploadingVideo ? 'Updating ...' : 'Next'
                        : uploadingVideo ? 'Uploading ...' : 'Next'}
                    </Button>
                  </DialogFooter>
                </>
              )
              : currentStep === 2
                ? (
                  <>
                    <DialogContent className={styles.content}>
                      <ContentBox>
                        <ContentBoxSection
                          className={styles.detailsBoxSection}
                          description="This section is Required in order to proceed"
                          title="Categories"
                        >
                          <div className={styles.categoriesContainer}>
                            <CategoryInput
                              defaultValue={videoCategory}
                              defaultName={videoCategoryName}
                              description="Select a main category that your video fits into."
                              label="Category"
                              onSelect={(val, name) => { setVideoCategory(val); setVideoCategoryName(name); }}
                            />
                            <CategoryInput
                              categoryID={videoCategory}
                              defaultValue={videoSubCategory}
                              defaultName={videoSubCategoryName}
                              description="Better sort your video into a specific subcategory."
                              isDisabled={!videoCategory}
                              isSubcategory
                              label="Sub-Category"
                              onSelect={(val, name) => { setVideoSubCategory(val); setVideoSubCategoryName(name); }}
                            />
                            <PlaylistInput
                              className={styles.playlist}
                              defaultValues={videoPlaylists}
                              defaultNames={videoPlaylistsNames}
                              description="Add your video to one or more playlist.
                              Playlist’s can help your audience view special collections."
                              label="Playlist"
                              onSelect={(val, name) => {
                                setVideoPlaylists([...val]);
                                setVideoPlaylistsNames(name);
                              }}
                            />
                          </div>
                        </ContentBoxSection>
                      </ContentBox>
                    </DialogContent>
                    <DialogFooter>
                      <Button
                        className={styles.navButton}
                        isBlue
                        isDisabled={uploadingCategoryPlaylist}
                        isOutlined
                        onClick={moveToPreviousStep}
                      >
                        Previous
                      </Button>
                      <Button
                        className={styles.navButton}
                        isBlue
                        isDisabled={uploadingCategoryPlaylist
                          || !(videoCategory && videoSubCategory)}
                        isOutlined
                        onClick={((videoCategory === uploadedData.category_id)
                          && (videoSubCategory === uploadedData.subcategory_id))
                          ? moveToNextStep
                          : updateCategoryPlaylist}
                      >
                        {uploadingCategoryPlaylist ? 'Updating ...' : 'Next'}
                      </Button>
                    </DialogFooter>
                  </>
                )
                : currentStep === 3
                  ? (
                    <>
                      <DialogContent className={styles.content}>
                        <ContentBox flex>
                          <ContentBoxSection
                            title="Previous Details"
                            description="Video preview how the audience will see this"
                            className={styles.summaryBoxSection}
                          >
                            <div className={styles.visibilityContainer}>
                              <Text.SectionTitle className={styles.visibilityTitle}>Visibility</Text.SectionTitle>
                              <p className={styles.visibilityDescription}>Choose when to publish and who can see your video.</p>
                              <RadioGroup
                                className={styles.visibilityRadio}
                                defaultValue={videoVisibilityOptions[0].value}
                                name="visibility"
                                value={videoVisibility}
                                onChange={(e) => { setVideoVisibility(e?.target?.value); }}
                              >
                                {videoVisibilityOptions.map(({ name, value }, idx) => (
                                  <FormControlLabel
                                    value={value}
                                    key={idx}
                                    control={<Radio color="primary" />}
                                    label={name}
                                  />
                                ))}
                              </RadioGroup>
                              {videoVisibility === videoVisibilityOptions[2].value && (
                                <GroupSelector
                                  id="group-selector"
                                  onSelect={(val) => { setVideoGroup(val); }}
                                  selectedOption={videoGroup}
                                />
                              )}
                            </div>
                            <VideoPreview
                              defaultVideoThumbnail={defaultVideoThumbnail}
                              link={`${domain}${Pathname.getVideoPath(videoID)}`}
                              type="link"
                            />
                          </ContentBoxSection>
                        </ContentBox>
                      </DialogContent>
                      <DialogFooter>
                        <Button
                          className={styles.navButton}
                          isBlue
                          isDisabled={uploadingVisibility}
                          isOutlined
                          onClick={moveToPreviousStep}
                        >
                          Previous
                        </Button>
                        <Button
                          className={styles.navButton}
                          isBlue
                          isDisabled={uploadingVisibility || !(videoVisibility === '3' ? videoGroup : true)}
                          isOutlined
                          onClick={updateVisibility}
                        >
                          {uploadingVisibility ? 'Publishing ...' : 'Publish'}
                        </Button>
                      </DialogFooter>
                    </>
                  )
                  : null}
      </div>
    </Fade>
  );
};

export const UploadModal = ({ onHide, isVisible }) => (
  <Dialog
    disableBackdropClick
    onClose={onHide}
    open={isVisible}
    PaperComponent={Modal}
    PaperProps={{ onHide, isVisible }}
    TransitionComponent={Slide}
    transitionDuration={isVisible ? 300 : 250}
    TransitionProps={{ direction: 'up' }}
  />
);
