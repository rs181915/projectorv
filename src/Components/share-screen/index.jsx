import React, { useContext, useEffect, useState } from 'react';
import cn from 'classnames';
import pt from 'prop-types';
import useSWR from 'swr';
import { debounce } from 'lodash';
import {
  Dialog as MuiDialog,
  DialogActions as MuiDialogActions,
  DialogContent as MuiDialogContent,
  DialogTitle as MuiDialogTitle,
  LinearProgress as MuiLinearProgress,
  withStyles,
  Fade,
} from '@material-ui/core';
import { AxiosCancelToken, dataFetcher } from 'Api';
import { AppContext } from 'Context';
import { ReactComponent as AddPersonIcon } from 'Assets/add-person.svg';
import { ReactComponent as LinkIcon } from 'Assets/link.svg';
import { Button } from '../button';
import styles from './index.module.css';

const Dialog = withStyles({ paper: { boxShadow: 'none', backgroundColor: 'transparent' } })(MuiDialog);
const DialogTitle = withStyles({ root: { backgroundColor: '#fff' } })(MuiDialogTitle);
const DialogContent = withStyles({ root: { backgroundColor: '#fff', borderRadius: '0px' } })(MuiDialogContent);
const DialogActions = withStyles({ root: { backgroundColor: '#fff' } })(MuiDialogActions);
const LinearProgress = withStyles({ root: { height: '4px' } })(MuiLinearProgress);

const PersonButton = ({ data, isDisabled, isSelected, onClick }) => {
  const { email, firstname, lastname, image: dpURL } = data;
  const name = `${firstname} ${lastname}`;

  return (
    <button
      className={cn({ [styles.personButton]: true, [styles.selected]: isSelected })}
      disabled={isDisabled}
      onClick={onClick}
    >
      <div className={styles.personButtonDP}>
        {dpURL
          ? <img alt={`person - ${name}`} className={styles.personDPImage} src={dpURL} />
          : <div className={styles.personDPText}>{name.charAt(0)}</div>}
      </div>
      <div className={styles.personButtonTextContent}>
        <div className={styles.personButtonName}>{name}</div>
        <div className={styles.personButtonEmail}>{email}</div>
      </div>
    </button>
  );
};

PersonButton.defaultProps = {
  isDisabled: false,
  isSelected: false,
  onClick: undefined,
};

PersonButton.propTypes = {
  data: pt.object.isRequired,
  isDisabled: pt.bool,
  isSelected: pt.bool,
  onClick: pt.func,
};

export const ShareScreen = ({
  actionButtonLoadingText,
  actionButtonText,
  apiEndpoint,
  errorText,
  hasLink,
  hideContent,
  imageEditable,
  inputPlaceholder,
  isVisible,
  linkDescription,
  linkTitle,
  multipleSelect,
  notes,
  noUsersFoundText,
  onContinue,
  onHide: closeHandler,
  requireTitle,
  successText,
  title,
  titleEditable,
  titlePlaceholder,
}) => {
  const { setAppSnackbar } = useContext(AppContext);

  const [titleName, setTitleName] = useState('');
  const [imageBase64, setImageBase64] = useState('');
  const [emailToSearch, setEmailToSearch] = useState('');
  const [searchingUsers, setUserSearchingStatus] = useState(false);
  const [cancelToken, setCancelToken] = useState(undefined);
  const [selectedUsers, updateSelectedUsers] = useState([]);
  const [operationStatus, setOperationStatus] = useState(undefined);
  const [userList, setUserList] = useState(undefined);

  const [handlingAction, setActionHandlingStatus] = useState(false);

  const onHide = () => {
    if (!handlingAction) {
      setEmailToSearch('');
      setImageBase64('');
      updateSelectedUsers([]);
      setUserSearchingStatus(false);
      setUserList(undefined);
      if (closeHandler) { closeHandler(); }
    }
  };
  const selectUser = (userData) => {
    updateSelectedUsers(multipleSelect ? [...selectedUsers, userData] : [userData]);
  };
  const removeUser = (userData) => {
    updateSelectedUsers(selectedUsers.filter((selectedUserData) => selectedUserData.id !== userData.id));
  };

  const { mutate: searchUsersByEmail } = useSWR([apiEndpoint, emailToSearch, cancelToken?.token], {
    fetcher: (url, inputEmail, uniqueCancelToken) => dataFetcher(url, { email: inputEmail }, uniqueCancelToken),
    onSuccess: ({ success, data }) => {
      setUserSearchingStatus(false);

      if (success) {
        if (emailToSearch !== '') {
          setUserList(data?.filter((userData) => ((userData.status === null)
            || (userData.status === undefined)))?.slice(0, 5));
        }
      } else {
        setUserList([]);
      }
    },
    onError: () => {
      setUserSearchingStatus(false);
      setUserList(undefined);
    },
  });

  const searchInputHandler = debounce(({ target: { value: inputEmail } }) => {
    if (inputEmail) {
      setEmailToSearch(inputEmail);
    } else {
      setEmailToSearch('');
      setUserList(undefined);
    }
  }, 750, { trailing: true });

  useEffect(() => {
    if (emailToSearch) {
      setUserSearchingStatus(true);
      searchUsersByEmail();
    }
  }, [emailToSearch, setUserSearchingStatus, searchUsersByEmail]);

  useEffect(() => {
    if (operationStatus === true) {
      setAppSnackbar({ isVisible: true, message: successText });
    } else if (operationStatus === false) {
      setAppSnackbar({ isVisible: true, type: 'error', message: errorText });
    }
  }, [operationStatus, errorText, successText, setAppSnackbar]);

  return (
    <Dialog className={styles.shareScreen} onClose={onHide} open={isVisible} TransitionComponent={Fade}>
      <DialogTitle className={styles.shareScreenHeading}>
        <div className={styles.shareScreenHeadingContent}>
          {imageEditable
            ? (
              <>
                {hideContent && (
                  <input
                    accept="image/*"
                    className={styles.hiddenFormField}
                    disabled={handlingAction || !hideContent}
                    id="share-screen-image-input"
                    onChange={({ target: { files } }) => {
                      const reader = new FileReader();
                      const file = files[0];

                      if (file) { reader.readAsDataURL(file); }

                      reader.addEventListener('load', () => { setImageBase64(reader.result); }, false);
                    }}
                    type="file"
                  />
                )}
                <div className={styles.addPersonIconContainer}>
                  {imageBase64
                    ? (
                      <>
                        <label className={styles.addPersonImageContainer} htmlFor="share-screen-image-input">
                          <img alt="Group Icon" className={styles.imagePreview} src={imageBase64} />
                        </label>
                        {(!handlingAction && hideContent) && (
                          <button
                            className={styles.imageDeleteButton}
                            onClick={() => {
                              document.getElementById('share-screen-image-input').value = '';
                              setImageBase64(undefined);
                            }}
                          />
                        )}
                      </>
                    )
                    : (
                      <label
                        className={cn({ [styles.addPersonImageContainer]: true, [styles.hasBG]: true })}
                        htmlFor="share-screen-image-input"
                      >
                        <AddPersonIcon className={styles.addPersonIcon} />
                      </label>
                    )}
                </div>
              </>
            )
            : (
              <div className={styles.addPersonIconContainer}>
                <div className={cn({ [styles.addPersonImageContainer]: true, [styles.hasBG]: true })}>
                  <AddPersonIcon className={styles.addPersonIcon} />
                </div>
              </div>
            )}
          {titleEditable
            ? (
              <input
                className={styles.titleInput}
                disabled={handlingAction || !hideContent}
                onInput={({ target: { value: inputTitle } }) => { setTitleName(inputTitle); }}
                placeholder={titlePlaceholder}
                type="text"
              />
            )
            : <>{title}</>}
        </div>
        {!hideContent && (
          <input
            className={styles.shareScreenEmailInput}
            disabled={handlingAction}
            onChange={(e) => {
              e.persist();
              searchInputHandler(e);
              setUserSearchingStatus(false);

              if (cancelToken) { cancelToken.cancel(); }

              if (!e?.target?.value) {
                setEmailToSearch('');
                setUserList(undefined);
              } else {
                setCancelToken(AxiosCancelToken.source());
              }
            }}
            placeholder={inputPlaceholder}
            type="text"
          />
        )}
        {searchingUsers && <LinearProgress className={styles.loader} />}
      </DialogTitle>

      {hideContent && (
        <DialogContent className={styles.shareScreenContent}>
          <br />
        </DialogContent>
      )}

      {!hideContent && (
        <DialogContent className={styles.shareScreenContent}>
          {!userList && <div className={styles.notes}>{notes}</div>}

          {userList && !userList?.length && <div className={styles.notes}>{noUsersFoundText}</div>}

          {userList?.map((userData) => {
            const isSelected = selectedUsers.some((ud) => ud.id === userData.id);

            return (
              <PersonButton
                data={userData}
                isDisabled={searchingUsers}
                isSelected={isSelected}
                key={userData?.id}
                onClick={() => { if (isSelected) { removeUser(userData); } else { selectUser(userData); } }}
              />
            );
          })}

          {!!(selectedUsers?.length) && (
            <>
              {!!(selectedUsers?.filter((sud) => (!userList?.some((ud) => ud.id === sud.id)))?.length) && (
                <div className={styles.selectionHeading}>Previously Selected</div>
              )}
              {selectedUsers?.map((selectedUserData) => (
                !userList?.some((userData) => userData.id === selectedUserData.id) && (
                  <PersonButton
                    data={selectedUserData}
                    isDisabled={handlingAction}
                    isSelected
                    key={selectedUserData?.id}
                    onClick={() => { removeUser(selectedUserData); }}
                  />
                )
              ))}
            </>
          )}
        </DialogContent>
      )}
      <DialogActions className={styles.shareScreenActions} style={{ borderTopLeftRadius: '0', borderTopRightRadius: '0' }}>
        <Button
          className={styles.shareScreenDoneButton}
          isBlue
          isDisabled={(requireTitle && !titleName) || (!hideContent && !(selectedUsers?.length)) || handlingAction}
          onClick={() => {
            onContinue([
              onHide,
              setActionHandlingStatus,
              setOperationStatus,
              updateSelectedUsers,
              setUserList,
              selectedUsers,
              titleName,
              imageBase64
            ]);
          }}
        >
          {handlingAction ? actionButtonLoadingText : actionButtonText}
        </Button>
      </DialogActions>

      {hasLink && (
        <DialogActions className={cn({ [styles.shareScreenActions]: true, [styles.linkShare]: true })}>
          <div className={styles.shareScreenHeadingContent}>
            <LinkIcon className={styles.linkIcon} />
            <>{linkTitle}</>
          </div>
          <div className={styles.shareScreenLinkText}>{linkDescription}</div>
          <div className={styles.shareScreenLinkButton}>Copy Link</div>
        </DialogActions>
      )}
    </Dialog>
  );
};

ShareScreen.defaultProps = {
  errorText: undefined,
  hasLink: false,
  hideContent: false,
  imageEditable: false,
  linkDescription: undefined,
  linkTitle: undefined,
  multipleSelect: false,
  requireTitle: false,
  successText: undefined,
  title: undefined,
  titleEditable: false,
  titlePlaceholder: undefined,
};

ShareScreen.propTypes = {
  actionButtonLoadingText: pt.string.isRequired,
  actionButtonText: pt.string.isRequired,
  apiEndpoint: pt.string.isRequired,
  errorText: pt.string,
  hasLink: pt.bool,
  hideContent: pt.bool,
  imageEditable: pt.bool,
  inputPlaceholder: pt.string.isRequired,
  isVisible: pt.bool.isRequired,
  linkDescription: pt.oneOfType([pt.string, pt.element]),
  linkTitle: pt.string,
  multipleSelect: pt.bool,
  notes: pt.string.isRequired,
  noUsersFoundText: pt.string.isRequired,
  onContinue: pt.func.isRequired,
  onHide: pt.func.isRequired,
  requireTitle: pt.bool,
  successText: pt.string,
  title: pt.string,
  titleEditable: pt.bool,
  titlePlaceholder: pt.string,
};
