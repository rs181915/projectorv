import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import cn from 'classnames';
import useSWR from 'swr';
import CheckIcon from '@material-ui/icons/Check';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import { endpoints } from 'Api';
import { Pathname } from 'Routes';
import styles from './index.module.css';

export const GroupSelector = ({ id: groupID, onSelect, selectedOption = null }) => {
  const location = useLocation();

  const [options, setOptions] = useState([]);

  const { isValidating: loadingGroups, mutate: fetchGroups } = useSWR(endpoints.getMyGroups, {
    onSuccess: ({ success, data }) => {
      if (success && data) {
        setOptions(data);
      } else {
        setOptions([]);
      }
    },
    onError: () => {
      setOptions([]);
    },
  });

  useEffect(() => {
    if (!location.search?.includes(Pathname.createGroup)) {
      fetchGroups();
    }
  }, [location.search]);

  return (
    <div className={styles.groupContainer} id={groupID}>
      {loadingGroups
        ? (
          <div className={styles.group}>
            <CheckBoxOutlineBlankIcon style={{ opacity: 0 }} />
            &nbsp;&nbsp;
            <>Loading ...</>
          </div>
        )
        : options?.map(({ title, id }) => (
          <div
            className={styles.group}
            key={id}
            onClick={() => { onSelect(id); }}
            onKeyPress={undefined}
            role="button"
            tabIndex={0}
          >
            {selectedOption === id
              ? <CheckIcon />
              : <CheckBoxOutlineBlankIcon style={{ opacity: 0 }} />}
            &nbsp;&nbsp;
            {title}
          </div>
        ))}
      <br />
      <Link className={cn({ [styles.group]: true, [styles.addGroup]: true })} to={Pathname.createGroup}>
        <CheckBoxOutlineBlankIcon style={{ opacity: 0 }} />
        &nbsp;&nbsp;
        <>New Group</>
      </Link>
    </div>
  );
};
