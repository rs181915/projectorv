import React, { useContext, useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import cn from 'classnames';
import { Drawer as MuiDrawer, Menu as MuiMenu, withStyles } from '@material-ui/core';
import { axios, endpoints } from 'Api';
import { AppContext, AuthContext } from 'Context';
import { Pathname } from 'Routes';
import { ReactComponent as AddIcon } from 'Assets/add-circle.svg';
import { ReactComponent as MenuIcon } from 'Assets/menu.svg';
import { ReactComponent as VideoUploadIcon } from 'Assets/video-upload.svg';
import { ReactComponent as ProfileIcon } from 'Assets/profile.svg';
import { ReactComponent as PersonIcon } from 'Assets/person-profile.svg';
import { ReactComponent as HomeIcon } from 'Assets/home-header.svg';
import { ReactComponent as SearchIcon } from 'Assets/search.svg';
import { ReactComponent as WatchlistIcon } from 'Assets/star.svg';
import { Logo } from '../logo';
import { ShareScreen } from '../share-screen';
import { UploadModal } from '../upload-modal';
import styles from './index.module.css';

const Drawer = withStyles({ paper: { backgroundColor: '#181a24' } })(MuiDrawer);
const Menu = withStyles({
  paper: {
    border: '1px solid #000',
    boxShadow: 'none',
    borderRadius: 0,
    maxHeight: 'calc(100vh - 44px)',
    width: 'unset',
  }
})(MuiMenu);

const HeaderIconButton = ({ icon: Icon, text, onClick, link }) => (
  <Link className={styles.profileHeaderIconButton} onClick={onClick} to={link}>
    {Icon && <Icon className={styles.profileHeaderIconButtonIcon} />}
    <div className={styles.profileHeaderIconButtonText}>{text}</div>
  </Link>
);

const ProfileMenuProfileButton = ({ email, Icon, link, name, onClick }) => (
  <Link className={styles.profileMenuProfile} onClick={onClick} to={link}>
    <Icon className={styles.profileMenuProfileIcon} />
    <div className={styles.profileMenuProfileName}>{name || email}</div>
  </Link>
);

const ProfileDrawerProfileButton = ({ email, Icon, link, name, onClick }) => (
  <Link className={styles.profileBarProfile} onClick={onClick} to={link}>
    <div className={styles.profileBarProfileIconContainer}>
      <Icon className={styles.profileBarProfileIcon} />
    </div>
    <div className={styles.profileBarProfileName}>{name || email}</div>
  </Link>
);

export const Header = ({
  glassCompactHeaderBG,
  isProfileHeader,
  hideMenu,
  noHeader,
  noRegularHeader,
  onMenuClick,
  transparentCompactHeader,
  userName,
  userProfileLink,
}) => {
  const { userConnections, userDetails } = useContext(AppContext);
  const { removeToken, setUserSignInStatus } = useContext(AuthContext);
  const location = useLocation();
  const navigate = useNavigate();

  const links = [
    { name: 'Content Dashboard', link: Pathname.dashboard },
    { name: 'Account Settings', link: Pathname.getSettingsPath(Pathname.settings.accounts) },
    { name: 'Edit Profile', link: Pathname.access },
    { name: 'Log Out', link: Pathname.authentication.signIn, onClick: () => { removeToken(); setUserSignInStatus(false); } },
  ];
  const profiles = userConnections;

  const [uploadModalIsVisible, setUploadModalVisibility] = useState(false);
  const [accessRequestModalIsVisible, setAccessRequestModalVisibility] = useState(false);
  const [tempGroupCreated, setTempGroupCreationStatus] = useState(false);
  const [tempGroupID, setTempGroupID] = useState(undefined);
  const [groupCreationModalIsVisible, setGroupCreationModalVisibility] = useState(false);
  const [profileMenuIsVisible, setProfileMenuVisibility] = useState(false);
  const [profileMenuAnchorEl, setProfileMenuAnchorEl] = useState(null);
  const [pageHasScrolled, setPageScroll] = useState(false);

  const revealUploadModal = () => { setUploadModalVisibility(true); };
  const hideUploadModal = () => { setUploadModalVisibility(false); navigate(location.pathname); };
  const revealAccessRequestModal = () => { setAccessRequestModalVisibility(true); };
  const hideAccessRequestModal = () => { setAccessRequestModalVisibility(false); navigate(location.pathname); };
  const revealGroupCreationModal = () => { setGroupCreationModalVisibility(true); };
  const hideGroupCreationModal = () => {
    setTempGroupID(undefined);
    setTempGroupCreationStatus(false);
    setGroupCreationModalVisibility(false);
    navigate(location.pathname);
  };
  const revealProfileMenu = (e) => { setProfileMenuAnchorEl(e.currentTarget); setProfileMenuVisibility(true); };
  const hideProfileMenu = () => { setProfileMenuAnchorEl(null); setProfileMenuVisibility(false); };

  useEffect(() => {
    if (location.search?.includes(Pathname.uploadVideo)) {
      revealUploadModal();
    } else if (location.search?.includes(Pathname.requestAccess)) {
      revealAccessRequestModal();
    } else if (location.search?.includes(Pathname.createGroup)) {
      revealGroupCreationModal();
    }
  }, [location.search]);

  useEffect(() => {
    const scrollHandler = () => {
      if ((document.body.scrollTop > 0) || (document.documentElement.scrollTop > 0)) {
        setPageScroll(true);
      } else {
        setPageScroll(false);
      }
    };

    window.addEventListener('scroll', scrollHandler);

    return () => { window.removeEventListener('scroll', scrollHandler); };
  }, []);

  const requestProfileAccess = async ([onHide, setActionStatus, setOperationStatus, setSelectedUsers, setUserList, users]) => {
    setActionStatus(true);

    const { data: requestProfileAccessData } = await axios
      .post(endpoints.sendViewRequest, { users: `${users.map((userData) => userData.id)}` })
      .catch(() => ({}));

    setUserList(undefined);

    if (requestProfileAccessData?.success) {
      setSelectedUsers([]);
      setOperationStatus(true);
      onHide();
    } else {
      setOperationStatus(false);
    }

    setActionStatus(false);
  };

  const createGroup = async ([, setActionStatus, setOperationStatus, , , , groupTitle, groupIcon]) => {
    setActionStatus(true);

    const { data: createGroupData } = await axios
      .post(endpoints.addNewGroup, { title: groupTitle, image: groupIcon })
      .catch(() => ({}));

    if (createGroupData?.success) {
      setTempGroupID(createGroupData?.id);
      setOperationStatus(true);
      setTempGroupCreationStatus(true);
    } else {
      setOperationStatus(false);
    }

    setActionStatus(false);
  };

  const addMembersToGroup = async ([
    onHide, setActionStatus, setOperationStatus, setSelectedUsers, setUserList, users, , ,
  ]) => {
    setActionStatus(true);

    const { data: createGroupData } = await axios
      .post(endpoints.addMemberToGroup, { group_id: tempGroupID, users: `${users.map((userData) => userData.id)}` })
      .catch(() => ({}));

    setUserList(undefined);

    if (createGroupData?.success) {
      setSelectedUsers([]);
      setOperationStatus(true);
      onHide();
    } else {
      setOperationStatus(false);
    }

    setActionStatus(false);
  };

  return (
    <>

      <header
        className={cn({
          [styles.glassCompactHeaderBG]: glassCompactHeaderBG,
          [styles.header]: true,
          [styles.noHeader]: noHeader,
          [styles.noRegularHeader]: noRegularHeader || isProfileHeader,
          [styles.pageScrolled]: pageHasScrolled,
          [styles.transparentCompactView]: transparentCompactHeader,
        })}
      >
        <div className={styles.headerContent}>
          <Link className={styles.regularView} to={Pathname.access}>
            <Logo />
          </Link>
          {hideMenu
            ? <>&nbsp;</>
            : (
              <button className={cn({ [styles.menuButton]: true, [styles.compactView]: true })} onClick={onMenuClick}>
                <MenuIcon className={styles.menuIcon} />
              </button>
            )}
          <div className={styles.options}>
            <button
              className={cn({ [styles.uploadButton]: true, [styles.regularView]: true })}
              onClick={revealUploadModal}
            >
              <span className={styles.uploadText}>Upload</span>
              <div className={styles.uploadIcon}>+</div>
            </button>
            <button
              className={cn({ [styles.uploadButton]: true, [styles.compactView]: true })}
              onClick={revealUploadModal}
            >
              <VideoUploadIcon className={styles.uploadIcon} />
            </button>
            <button
              aria-controls="profile-menu"
              className={cn({ [styles.profileButton]: true, [styles.regularView]: true })}
              onClick={revealProfileMenu}
            >
              <PersonIcon className={styles.profileIcon} />
              <div className={styles.dropdownIcon}>▾</div>
            </button>

            <button
              className={cn({ [styles.profileButton]: true, [styles.compactView]: true })}
              onClick={revealProfileMenu}
            >
              <PersonIcon className={styles.profileIcon} />
            </button>
          </div>
        </div>
      </header>
      <div
        className={cn({
          [styles.headerFill]: true,
          [styles.noHeader]: noHeader,
          [styles.noRegularHeader]: noRegularHeader || isProfileHeader,
        })}
      />

      <header className={cn({ [styles.profileHeader]: true, [styles.hidden]: !isProfileHeader })}>
        <div className={styles.profileHeaderContent}>
          <div className={styles.leftContent}>
            <HeaderIconButton icon={HomeIcon} text="Home" link={Pathname.access} />
            <HeaderIconButton icon={SearchIcon} text="Search" link={Pathname.search} />
            <HeaderIconButton icon={WatchlistIcon} text="Watchlist" link={Pathname.watchlist} />
            {/* {(userName && userProfileLink) && (
              <HeaderIconButton
                icon={ProfileIcon}
                text={`${(userName?.length > 16) ? `${userName?.substring(0, 16)}...` : userName}'s Page`}
                link={userProfileLink}
              />
            )} */}
          </div>
          <div className={styles.rightContent}>
            <HeaderIconButton text="UPLOAD" link={`${Pathname.uploadVideo}`} />
            <HeaderIconButton
              aria-controls="profile-menu"
              text={userDetails?.name?.substring(0, 10)
                || userDetails?.email?.split('@')[0]?.substring(0, 10)}
              onClick={revealProfileMenu}
              link="#"
            />
          </div>
        </div>
      </header>

      <Menu
        anchorEl={profileMenuAnchorEl}
        id="profile-menu"
        MenuListProps={{ className: styles.profileMenu }}
        onClose={hideProfileMenu}
        open={profileMenuIsVisible}
        PopoverClasses={{ paper: styles.profileMenuContainer }}
      >
        <div className={styles.profileMenuCurrentProfileDetails}>
          <div className={styles.profileMenuCurrentProfileDetailsText}>
            {userDetails?.firstname}
            <> </>
            {userDetails?.lastname}
          </div>
          <ProfileIcon className={styles.profileMenuCurrentProfileDetailsIcon} />
        </div>
        <hr className={styles.profileMenuDivider} />
        <div className={styles.profileMenuProfilesContainer}>
          {profiles.map(({ firstname, lastname, email, id }, idx) => (
            <ProfileMenuProfileButton
              email={email}
              Icon={PersonIcon}
              key={idx}
              link={Pathname.getFriendProfilePath(id)}
              name={`${firstname} ${lastname}`.trim()}
            />
          ))}
          <ProfileMenuProfileButton Icon={AddIcon} link="#" name="Add Profile" onClick={revealAccessRequestModal} />
        </div>
        <div className={styles.profileMenuLinksContainer}>
          {links.map(({ name, link, onClick }, idx) => (
            <Link to={link} key={idx} onClick={onClick} className={styles.profileMenuLink}>{name}</Link>
          ))}
        </div>
      </Menu>

      <Drawer anchor="right" open={profileMenuIsVisible} className={styles.profileBar} onClose={hideProfileMenu}>
        <div className={styles.profileBarContainer}>
          <div className={styles.profileBarContent}>
            <div className={styles.profileBarProfilesContainer}>
              <div>
                <div className={styles.profileBarProfilesContent}>
                  {profiles.map(({ firstname, lastname, email, id }, idx) => (
                    <ProfileDrawerProfileButton
                      email={email}
                      Icon={ProfileIcon}
                      key={idx}
                      link={Pathname.getFriendProfilePath(id)}
                      name={`${firstname} ${lastname}`.trim()}
                    />
                  ))}
                  <ProfileDrawerProfileButton Icon={AddIcon} link="#" name="Add Profile" onClick={revealAccessRequestModal} />
                </div>
              </div>
            </div>
            <div className={styles.profileBarLinksContainer}>
              {links.map(({ name, link, onClick }, idx) => (
                <Link to={link} key={idx} onClick={onClick} className={styles.profileBarLink}>{name}</Link>
              ))}
            </div>
          </div>
        </div>
      </Drawer>

      <UploadModal isVisible={uploadModalIsVisible} onHide={hideUploadModal} />

      <ShareScreen
        actionButtonLoadingText="Requesting Access..."
        actionButtonText="Request Access"
        apiEndpoint={endpoints.searchUserByEmail}
        errorText="Oops! Something went wrong"
        inputPlaceholder="Search Users"
        isVisible={accessRequestModalIsVisible}
        multipleSelect
        notes="Enter an email to search"
        noUsersFoundText="No users found with this email"
        onContinue={requestProfileAccess}
        onHide={hideAccessRequestModal}
        successText="Request sent"
        title="Request Access to View"
      />

      <ShareScreen
        actionButtonLoadingText={tempGroupCreated ? 'Adding Users...' : 'Creating Group...'}
        actionButtonText={tempGroupCreated ? 'Add Users' : 'Create Group'}
        apiEndpoint={endpoints.searchUserByEmailFromFrendList}
        errorText={tempGroupCreated ? 'Oops! Something went wrong' : 'This group name is not available'}
        hideContent={!tempGroupCreated}
        imageEditable
        inputPlaceholder="Search Friends"
        isVisible={groupCreationModalIsVisible}
        multipleSelect
        notes="Enter an email to search"
        noUsersFoundText="No friends found with this email"
        onContinue={tempGroupCreated ? addMembersToGroup : createGroup}
        onHide={hideGroupCreationModal}
        requireTitle
        successText={tempGroupCreated ? 'Group created successfully' : 'Successfully added users to group'}
        titleEditable
        titlePlaceholder="Enter group name"
      />
    </>
  );
};
