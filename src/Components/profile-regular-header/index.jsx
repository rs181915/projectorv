import React from 'react';
import { Link } from 'react-router-dom';
import { Pathname } from 'Routes';
import { ReactComponent as HomeIcon } from 'Assets/home-header.svg';
import { ReactComponent as SearchIcon } from 'Assets/search.svg';
import { ReactComponent as ProfileIcon } from 'Assets/profile.svg';
import styles from './index.module.css';

const HeaderIconButton = ({ icon: Icon, text, link }) => (
  <Link className={styles.headerIconButton} to={link}>
    {Icon && <Icon className={styles.headerIconButtonIcon} />}
    <div className={styles.headerIconButtonText}>{text}</div>
  </Link>
);

export const ProfileRegularHeader = ({ userName, userProfileLink }) => (
  <header className={styles.header}>
    <div className={styles.headerContent}>
      <div className={styles.leftContent}>
        <HeaderIconButton icon={HomeIcon} text="Home" link={Pathname.home} />
        <HeaderIconButton icon={SearchIcon} text="Search" link="" />
        {(userName && userProfileLink) && (
          <HeaderIconButton
            icon={ProfileIcon}
            text={`${(userName?.length > 16) ? `${userName?.substring(0, 16)}...` : userName}'s Page`}
            link={userProfileLink}
          />
        )}
      </div>
      <div className={styles.rightContent}>
        <HeaderIconButton text="UPLOAD" link={`${Pathname.uploadVideo}`} />
        <HeaderIconButton text="ACCOUNT" link="#" />
      </div>
    </div>
  </header>
);
