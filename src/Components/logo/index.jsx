import React from 'react';
import cn from 'classnames';
import logoWBG from 'Assets/logo-white-bg.png';
import logoBBG from 'Assets/logo-black-bg.png';
import logoImage from 'Assets/logo.png';
import styles from './index.module.css';

export const Logo = ({ className, isLight, isLarge }) => (
  <div className={cn({ [styles.logo]: true, [styles.large]: isLarge, [className]: className })}>
    {isLight
      ? <img className={styles.logoImage} src={logoImage} alt="projector logo" />
      : <img className={styles.logoImage} src={logoImage} alt="projector logo" />}
  </div>
);
