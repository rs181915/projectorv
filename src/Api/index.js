import AXIOS from 'axios';
import endpoints from './endpoints.json';

const getToken = () => localStorage.getItem('token');
const AxiosCancelToken = AXIOS.CancelToken;

const axios = AXIOS.create({ baseURL: endpoints.baseURL, method: 'POST' });

axios.interceptors.request.use((req) => {
  if (req?.data instanceof FormData) {
    const formData = req?.data;

    if (formData.has('token')) { formData.delete('token'); }

    formData.append('token', getToken());

    return ({ ...req, data: formData });
  }

  return ({ ...req, data: { ...req.data, token: getToken() } });
});

const dataFetcher = (url, data, cancelToken) => axios({
  url,
  ...data && { data },
  ...cancelToken && { cancelToken },
}).then((res) => res.data);

export {
  axios,
  AxiosCancelToken,
  dataFetcher,
  endpoints,
  getToken,
};
