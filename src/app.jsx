import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Routes, useLocation } from 'react-router-dom';
import useSWR from 'swr';
import { CircularProgress as MuiCircularProgress, withStyles } from '@material-ui/core';
import { AppContext, AuthContext } from 'Context';
import { Snackbar } from 'Components';
import { dataFetcher, endpoints } from 'Api';
import {
  Access,
  AccountSettings,
  BillingSettings,
  ConnectedAccountSettings,
  ContentLayout,
  ContentLayoutDashboard,
  Dashboard,
  ForgotPassword,
  GettingStarted,
  MyProfile,
  NotificationSettings,
  PrivacySettings,
  Profile,
  PublicHome,
  PublicPlans,
  SearchPage,
  SignIn,
  SignUp,
  VideoDetails,
  VideoPlayer,
  Watchlist,
  YourVideos,
} from 'Pages';
import { Pathname } from 'Routes';
import styles from './app.module.css';
import 'Styles/site-global.css';

const CircularProgress = withStyles({
  circle: {
    color: '#fff !important'
  }
})(MuiCircularProgress);

const defaultAppSnackbar = { isVisible: false, type: undefined, message: undefined };
const appSnackbarAutoHideDuration = 1500;

const getToken = () => localStorage.getItem('token');
const setToken = (token) => { localStorage.setItem('token', token); };
const removeToken = () => { localStorage.removeItem('token'); };

const RouteSwitch = ({ isNewUser, userIsSignedIn }) => {
  const currentLocation = useLocation();

  useEffect(() => { window.scrollTo(0, 0); }, [currentLocation.pathname]);

  return (
    <Routes>
      {userIsSignedIn
        ? (
          <>
            <Route path={Pathname.home} element={isNewUser ? <GettingStarted /> : <Dashboard />} />
            <Route path={Pathname.gettingStarted} element={<GettingStarted />} />
            <Route path={Pathname.access} element={<Access />} />
            <Route path={Pathname.dashboard} element={<Dashboard />} />
            <Route path={Pathname.yourVideos} element={<YourVideos />} />
            <Route path={Pathname.contentLayout} element={<ContentLayout />} />
            <Route path={Pathname.contentLayoutDashboard} element={<ContentLayoutDashboard />} />
            <Route path={Pathname.settings.basePath}>
              <Route path="/" element={<AccountSettings />} />
              <Route path={Pathname.settings.accounts} element={<AccountSettings />} />
              <Route path={Pathname.settings.notifications} element={<NotificationSettings />} />
              <Route path={Pathname.settings.privacy} element={<PrivacySettings />} />
              <Route path={Pathname.settings.manageUsers} element={<ConnectedAccountSettings />} />
              <Route path={Pathname.settings.billings} element={<BillingSettings />} />
            </Route>
            <Route path={Pathname.profile.basePath}>
              <Route path="/" element={<MyProfile />} />
              <Route path={Pathname.profile.friendProfile} element={<Profile />} />
            </Route>
            <Route path={Pathname.watchlist} element={<Watchlist />} />
            <Route path={Pathname.search} element={<SearchPage />} />
            <Route path={Pathname.video} element={<VideoDetails />} />
            <Route path={Pathname.player} element={<VideoPlayer />} />
            <Route path="*" element={isNewUser ? <GettingStarted /> : <Dashboard />} />
          </>
        )
        : (
          <>
            <Route path={Pathname.home} element={<PublicHome />} />
            <Route path={Pathname.plans} element={<PublicPlans />} />
            <Route path={Pathname.authentication.signUp} element={<SignUp />} />
            <Route path={Pathname.authentication.signIn} element={<SignIn />} />
            <Route path={Pathname.authentication.forgotPassword} element={<ForgotPassword />} />
            <Route path="*" element={<PublicHome />} />
          </>
        )}
    </Routes>
  );
};

export const App = () => {
  const [userIsSignedIn, setUserSignInStatus] = useState(false);
  const [autoSigningIn, setAutoSignIn] = useState(true);
  const [userDetails, setUserDetails] = useState({
    firstname: '',
    lastname: '',
    dpURL: '',
    email: '',
    mobile: '',
    isNewUser: undefined,
    emailNS: undefined,
    subscriptionNS: undefined,
  });
  const [userConnections, setUserConnections] = useState([]);
  const [preferredEmail, setPreferredEmail] = useState('');

  const [appSnackbar, setAppSnackbar] = useState(defaultAppSnackbar);

  const { isValidating: gettingUserConnections, mutate: getUserConnections } = useSWR([endpoints.getAllViewRequestSent, 1], {
    fetcher: (url, status) => dataFetcher(url, { status }),
    onSuccess: ({ success, data }) => {
      if (success) {
        setUserConnections(data);
      } else {
        setUserConnections([]);
      }
    },
    onError: () => { setUserConnections([]); },
  });

  const { mutate: autoSignIn } = useSWR([endpoints.getMyProfile], {
    onSuccess: ({ success, data }) => {
      if (success) {
        setUserDetails({
          ...userDetails,
          firstname: data?.firstname,
          lastname: data?.lastname,
          dpURL: data?.image,
          email: data?.email,
          mobile: data?.mobile,
          isNewUser: data?.isNewUser,
          emailNS: data?.email_notification === '1',
          subscriptionNS: data?.sub_notification === '1',
        });
        setAutoSignIn(false);
        setUserSignInStatus(true);
      } else {
        removeToken();
        setAutoSignIn(false);
      }
    },
    onError: () => {
      removeToken();
      setAutoSignIn(false);
    },
  });

  const forceAutoSignin = () => {
    setAutoSignIn(true);
    autoSignIn();
  };

  useEffect(() => {
    const htmlClass = document.getElementsByTagName('html')[0].classList;

    window.onresize = () => {
      htmlClass.add(styles.changingTheme);
      setTimeout(() => htmlClass.remove(styles.changingTheme), 500);
    };
  }, []);

  useEffect(() => {
    const token = getToken();

    if (token) {
      setAutoSignIn(true);
      autoSignIn();
    } else {
      setAutoSignIn(false);
    }
  }, [autoSignIn]);

  useEffect(() => {
    if (userIsSignedIn) {
      getUserConnections();
    }
  }, [userIsSignedIn, getUserConnections]);

  return (
    <AuthContext.Provider
      value={{
        forceAutoSignin,
        getToken,
        preferredEmail,
        removeToken,
        setPreferredEmail,
        setToken,
        setUserSignInStatus,
      }}
    >
      <AppContext.Provider
        value={{
          getUserConnections,
          gettingUserConnections,
          setAppSnackbar,
          setUserConnections,
          setUserDetails,
          userConnections,
          userDetails,
        }}
      >
        <div className={styles.app}>
          {autoSigningIn
            ? (
              <div className={styles.autoSignInSpinnerContainer}>
                <CircularProgress />
              </div>
            )
            : (
              <Router>
                <RouteSwitch
                  {...{
                    setUserSignInStatus,
                    isNewUser: userDetails?.isNewUser,
                    userIsSignedIn,
                  }}
                />
              </Router>
            )}
        </div>

        <Snackbar
          autoHideDuration={appSnackbarAutoHideDuration}
          isVisible={appSnackbar?.isVisible}
          message={appSnackbar?.message || ''}
          onClose={() => { setAppSnackbar(defaultAppSnackbar); }}
          type={appSnackbar?.type}
        />
      </AppContext.Provider>
    </AuthContext.Provider>
  );
};
