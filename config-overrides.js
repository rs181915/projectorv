const path = require('path');

const { getLoader } = require('react-app-rewired');

const rewire = (config, env) => {
  const cssRules = getLoader(
    config.module.rules,
    (rule) => rule.test && String(rule.test) === String(/\.css$/),
  );

  const cssLoader = getLoader(
    env === 'production' ? cssRules.loader : config.module.rules,
    (rule) => rule.loader
      && rule.loader.indexOf(`${path.sep}css-loader${path.sep}`) !== -1,
  );
  cssLoader.options = {
    ...cssLoader.options,
    modules: true,
    localIdentName: `${env === 'production' ? '' : '[local]__'}[hash:base64:8]`,
  };

  return config;
};

module.exports = rewire;
